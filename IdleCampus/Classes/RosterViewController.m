//
//  NewMessageViewController.m
//  IdleCampus
//
//  Created by Ankur Kothari on 5/7/13.
//
//

#import "RosterViewController.h"
#import "ComposeMessageViewController.h"
#import "XMPPNative.h"
#import "BingoViewController.h"
@interface RosterViewController ()

@end

@implementation RosterViewController
@synthesize groupHeader,topBar,tableData,group,tableView,friendToAdd;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)addFriendToRoster:(id)sender{
    
}

- (void)viewDidLoad
{
    groupHeader.text = group;
    [super viewDidLoad];
    // Set the background image for PortraitMode
    
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [leftButton setUserInteractionEnabled:NO];
    [leftButton setImage:[UIImage imageNamed:@"backButtonWithText.png"] forState:UIControlStateNormal];
    leftButton.frame = CGRectMake(0, 0, 44, 30);
    [leftButton addTarget:self action:@selector(goback:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    [self.navigationItem setLeftBarButtonItem:item animated:NO];
    [leftButton release];
    
     tableData = [[NSMutableArray alloc]initWithArray:[[NSUserDefaults standardUserDefaults]objectForKey:@"roster"]]; 
    
}

- (void)goback:(id) sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)newMessage{
    ComposeMessageViewController *composeViewController = [[ComposeMessageViewController alloc]initWithNibName:@"ComposeMessageViewController" bundle:nil];
    composeViewController.group = group;
    composeViewController.sentController = self;
    [self presentModalViewController:composeViewController animated:YES];
    //    [self.navigationController presentModalViewController:composeViewController animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"FriendWithStatus";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        //        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        // Load the top-level objects from the custom cell XIB.
        
        
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"FriendWithStatus" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    NSString *text = [tableData objectAtIndex:indexPath.row];
    
    
    //    cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
    UILabel *friendName = (UILabel *)[cell viewWithTag:2];
    friendName.text =  text;
   
//    UILabel *friendRequestStatus = (UILabel *)[cell viewWithTag:3];
//    friendRequestStatus.text =  text;
    
    
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:1];
//    if([friendRequestStatus.text isEqualToString:@"pending"]){
//        UIImage *image = [UIImage imageNamed: @"red.png"];
//        [imageView setImage:image];
//    }
//    else if([friendRequestStatus.text isEqualToString:@"accepted"]){
//        UIImage *image = [UIImage imageNamed: @"green.png"];
//        [imageView setImage:image];
//    }
   
    
    
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    
    NSString *string = [tableData objectAtIndex:indexPath.row];
    CGSize stringSize = [string sizeWithFont:[UIFont boldSystemFontOfSize:15]
                           constrainedToSize:CGSizeMake(320, 9999)
                               lineBreakMode:UILineBreakModeWordWrap];
    
    return stringSize.height+25;
    
}

-(IBAction)sendFriendshipRequest:(id)sender{
  XMPPNative*   xmpp = [XMPPNative sharedManager];
    
    [xmpp subscribeTo:friendToAdd.text];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *textSelected = [tableData objectAtIndex:indexPath.row];
    
        BingoViewController *bingoViewController = [[BingoViewController alloc]
                                                          initWithNibName:@"BingoViewController"
                                                          bundle:nil];
    
    bingoViewController.userPlayingBingoWith = textSelected;
        
        [self.navigationController pushViewController:bingoViewController animated:YES];
        [bingoViewController release];
        
       
   
    
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
