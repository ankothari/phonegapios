//
//  BingoKey.h
//  IdleCampus
//
//  Created by Ankur Kothari on 15/7/13.
//
//

#import <Foundation/Foundation.h>

@interface BingoKey : NSObject<NSCoding>{
    int key,xPos,yPos,x,y,myScore;
    BOOL pressed;
}
@property (nonatomic)int key,xPos,yPos,x,y,myScore;
@property (nonatomic, assign, getter=isPressed) BOOL pressed;
@end
