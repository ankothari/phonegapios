//
//  MyClass.cpp
//  IdleCampus
//
//  Created by Ankur Kothari on 12/4/12.
//
//

#include "MyClass.h"
#include "headers/pubsubevent.h"
#import <UIKit/UIKit.h>
using namespace std;



void MyClass::doSomething(string username, string password) {

    jid = new gloox::JID(username);
//    jid->setResource("idlecampus");
    client = new gloox::Client(*jid, password);
    ConnListener *connListener = new ConnListener();
    client->registerConnectionListener(this);
    client->registerPresenceHandler(this);
    client->registerMessageHandler(this);
    this->user = username;
    this->password = password;


    client->registerStanzaExtension(new gloox::PubSub::Event(NULL));
    rostermgr = client->rosterManager();
    rostermgr->registerRosterListener(this);

    pubmgr = new gloox::PubSub::Manager(client);
    //client->setServer("talk.google.com");
    groups = [[NSMutableDictionary alloc] initWithCapacity:0];
    client->setTls(gloox::TLSDisabled);
    
    try
    {
        bool b = client->connect();
    }
    catch (int e)
    {
        cout << "An exception occurred. Exception Nr. " << e << endl;
    }
    
    
   

}

bool MyClass::onTLSConnect(const gloox::CertInfo& info) {
    // decide whether you trust the certificate, examine the CertInfo structure
    return true; // if you trust it, otherwise return false
}

void MyClass::handleReceivedData(const gloox::ConnectionBase * /*connection*/, const std::string& data) {
    cout << "L";
}

void MyClass::handleTag(gloox::Tag *tag) {
    cout << tag->xml();
}

void MyClass::handleConnect(const gloox::ConnectionBase *connection) {
    cout << "l";
}

void MyClass::registerUser(string luser, string lpassword,string lgroup,string lemail) {

    client = new gloox::Client("idlecampus.com");
    client->disableRoster();
    client->registerConnectionListener(this);

    m_reg = new gloox::Registration(client);
    m_reg->registerRegistrationHandler(this);
  
    
    toRegister = true;
    firstRegister = true;
    user = luser;
    password = lpassword;
    email = lemail;
    group = lgroup;
    client->connect();
}

void MyClass::subscribe(string user) {
    gloox::JID *jid = new gloox::JID(user);
    rostermgr->subscribe(*jid);
}

void MyClass::onConnect() {
    if (toRegister) {
        m_reg->fetchRegistrationFields();
    }
    else if(!toRegister && firstRegister){
        this->subscribeNode(group);
         this->sendUserLoginDetailsToIdlecampus();
        
        [bridge sendRegistered:1];
    }
    else if(!toRegister && !firstRegister){
         this->sendUserLoginDetailsToIdlecampus();
        [bridge sendLogin1:2];
    }
    else {
        this->createMainNode(jid->full() + "/groups");
        client->registerIqHandler(this, gloox::ExtPubSubOwner);
        client->setPresence();
        
        
        
        

        
    }
    cout << "connected";

   
    //
    // connection established, auth done (see API docs for exceptions)
}

void MyClass::createMainNode(string node) {
    gloox::JID *jid1 = new gloox::JID("pubsub.192.168.0.11");

    pubmgr->createNode(*jid1, node, NULL, this);

    this->getSubscribersToGroup("test");


}

void MyClass::createNode(string node) {
    gloox::JID *jid1 = new gloox::JID("pubsub.192.168.0.11");

    pubmgr->createNode(*jid1, node, NULL, this);

    this->publishNode(node, jid->full() + "/groups");


}

void MyClass::publishNode(string node, string message) {

    gloox::Tag *tag = new gloox::Tag("item", "");
    gloox::Tag *messagechild = new gloox::Tag("value", "");
    messagechild->setCData(message);

    tag->addChild(messagechild);

    gloox::Tag *fromchild = new gloox::Tag("from", "");
    fromchild->setCData(node);

    // tag->addChild(fromchild);

    gloox::PubSub::Item *item = new gloox::PubSub::Item(tag);

    typedef std::list<gloox::PubSub::Item *> ItemList;
    ItemList list;


    ItemList::iterator it;
    it = list.begin();
    ++it;       // it points now to number 2           ^

    list.insert(it, item);
    gloox::JID *jid = new gloox::JID("pubsub.idlecampus.com");

    cout << item->tag()->xml();
    cout << item->tag()->name();
    cout << item->id();
    pubmgr->publishItem(*jid, node, list, NULL, this);

}

void MyClass::unsubscribeNode(std::string node) {
    gloox::JID *jid = new gloox::JID("idlecampus.com");
    pubmgr->unsubscribe(*jid, node, NULL, this);
}

void MyClass::subscribeNode(std::string node) {
    gloox::JID *jid = new gloox::JID("pubsub.idlecampus.com");
    pubmgr->subscribe(*jid, node, this);
    

}

void MyClass::deleteNode(string node) {
    gloox::JID *jid = new gloox::JID("pubsub.192.168.0.11");
    pubmgr->deleteNode(*jid, node, this);
}

void MyClass::getGroupsJoined() {
    gloox::JID *jid = new gloox::JID("pubsub.idlecampus.com");


    pubmgr->getSubscriptions(*jid, this);
}


void MyClass::handlePresence(const gloox::Presence& presence) {
    // presence info
    cout << "presence";
}

void MyClass::onDisconnect(gloox::ConnectionError e) {
    // presence info
    cout << e;
    cout << client->streamError();
    cout << "disconnected";

    if(e==16){
        cout<<"auth failed";
        
        cout << disconnected;
        
        [bridge sendLogin1:1];
        
        cout << "hidcdsdsfs";
    }
       //client->connect();
}

void MyClass::handleMessage(const gloox::Message& msg, gloox::MessageSession *session) {
    cout << msg.tag()->xml();
     cout << msg.to().resource();
    
    
    const gloox::PubSub::Event *pse = msg.findExtension<gloox::PubSub::Event>(gloox::ExtPubSubEvent);
    if (pse) {

        cout << pse->tag()->xml();
        cout << pse->node();
        const gloox::PubSub::Event::ItemOperationList o = pse->items();
        if (o.size() != 0) {
            const gloox::Tag *tag = (*(o.begin()))->payload;
//            const gloox::Tag *from = (*(tag->findChildren("from").begin()));
            cout << tag->hasChild("value");
            cout << tag->hasChild("retract");
            if (tag->hasChild("value")) {
                const gloox::Tag *message = (*(tag->findChildren("value").begin()));

                cout << message->cdata();
                string other = pse->node();

                string s = message->cdata() + "#" + other;
                cout << s;
                if(msg.to().resource() == "Bingo"){
                    cout << "Got Message from Bingo";
                    [bridge callNotify:[[NSString alloc] initWithUTF8String:message->cdata().c_str()] fromUser:[[NSString alloc] initWithUTF8String:msg.from().full().c_str()] fromApp:[[NSString alloc] initWithUTF8String:msg.to().resource().c_str()]];
                }
                else{
                    cout << "Got Message from Group";
                    [bridge callNotify:[[NSString alloc] initWithUTF8String:message->cdata().c_str()] fromUser:[[NSString alloc] initWithUTF8String:msg.from().full().c_str()] fromApp:@"idlecampus"];
                }
               
            }

        }


    }
    else {
        string other = msg.from().bare();
        string tag = msg.tag()->xml();


        string s = msg.body();
        cout << tag;
         [bridge callNotify:[[NSString alloc] initWithUTF8String:s.c_str()] fromUser:[[NSString alloc] initWithUTF8String:other.c_str()] fromApp:[[NSString alloc] initWithUTF8String:msg.to().resource().c_str()]];

    }


}

void MyClass::startChatWith(string message, string to) {

    gloox::JID *jid = new gloox::JID(to);
    jid->setResource("idlecampus");
    gloox::MessageSession *session = new gloox::MessageSession(client, *jid);
    session->registerMessageHandler(this);
    session->send(message, "No Subject");
//    return session;


}

void MyClass::playBingo(string message, string to) {
    
    gloox::JID *jid = new gloox::JID(to);
    jid->setResource("Bingo");
    gloox::MessageSession *session = new gloox::MessageSession(client, *jid);
    session->registerMessageHandler(this);
    session->send(message, "No Subject");
    //    return session;
    
    
}

void MyClass::auth_user(string to, bool ack) {
    gloox::JID *jid = new gloox::JID(to);
//    rostermgr->subscribe(*jid);
  rostermgr->ackSubscriptionRequest(*jid, ack);


}

void MyClass::handleItemAdded(const gloox::JID& jid) {

}

void MyClass::handleItemSubscribed(const gloox::JID& jid) {
//called when the item subscribes.
    
    //refresh the roster or add the contact to the roster.main thodi poori roster wapas maangunga.
}

void MyClass::handleItemUpdated(const gloox::JID& jid) {

}

void MyClass::handleItemUnsubscribed(const gloox::JID& jid) {

}

void MyClass::handleRoster(const gloox::Roster& roster) {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSMutableDictionary *rosterDict = [[NSMutableDictionary alloc] init];
        NSMutableArray *rostArray = [[NSMutableArray alloc]init];
        map<const string, gloox::RosterItem *> h = roster;
        map<const string, gloox::RosterItem *>::iterator iter;
        for (iter = h.begin(); iter != h.end(); ++iter) {
<<<<<<< HEAD
            [rosterarray addObject:[[NSString alloc] initWithUTF8String:iter->first.c_str()]];
            cout << iter->second->name();
=======
//            [rosterDict setObject:[[NSString alloc] initWithUTF8String:iter->first.c_str()] forKey:[[NSString alloc] initWithUTF8String:iter->second->online()]];
            [rostArray addObject:[[NSString alloc] initWithUTF8String:iter->first.c_str()] ];
            cout << iter->first.c_str();
            cout << iter->second->online();
>>>>>>> testing
        }
        [rosterDict setObject:rostArray forKey:@"roster"];
        [bridge sendRoster:rosterDict];
       
        NSLog(@"%@", rosterDict);

    });

}

void MyClass::handleRosterPresence(const gloox::RosterItem& item, const string& resource,
        gloox::Presence::PresenceType presence, const string& msg) {

}

void MyClass::handleSelfPresence(const gloox::RosterItem& item, const string& resource,
        gloox::Presence::PresenceType presence, const string& msg) {

}

bool MyClass::handleSubscriptionRequest(const gloox::JID& jid, const string& msg) {
    cout << "subscription";
    this->auth_user(jid.full(), true);
    [bridge gotSubscriptionRequest:[[NSString alloc] initWithUTF8String:jid.bare().c_str()]];
    return true;
}

bool MyClass::handleUnsubscriptionRequest(const gloox::JID& jid, const string& msg) {

}

void MyClass::handleNonrosterPresence(const gloox::Presence& presence) {

}

void MyClass::handleRosterError(const gloox::IQ& iq) {

}

void MyClass::handleItemRemoved(const gloox::JID& jid) {

}

void MyClass::handleRegistrationFields(const gloox::JID& from, int fields,
        std::string instructions) {
    struct gloox::RegistrationFields r;
    r.username = user;
    r.password = password;
    bool b = m_reg->createAccount(1 | 4, r);
    
    this->sendUserDetailsToIdlecampus();
    cout << b;
    
    NSString *cuser = [NSString stringWithCString:user.c_str()
                                         encoding:[NSString defaultCStringEncoding]];
    
    cuser = [cuser stringByAppendingString:@"@idlecampus.com"];
    
    toRegister = false;
    
    firstRegister = true;
    
    this->doSomething([cuser UTF8String], password);
    

    
    
    
    
    
}


void MyClass::sendUserDetailsToIdlecampus(){
    NSString *cuser = [NSString stringWithCString:user.c_str()
                                         encoding:[NSString defaultCStringEncoding]];
    NSString *cemail =  [NSString stringWithCString:email.c_str()
                                           encoding:[NSString defaultCStringEncoding]];
    NSURL *aUrl = [NSURL URLWithString:@"http://idlecampus.com/users"];  //http://10.124.7.63:3000/users
    cuser = [cuser stringByAppendingString:@"@idlecampus.com"];
    
    
    //The user has been registered here. and his details are now sent to the server for storage.
    HTTPDelegate *dd = [[HTTPDelegate alloc] init];
    
  
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *uuidString = [defaults objectForKey:@"device_identifier"];
    cout << uuidString;
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:aUrl
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    
    [request setHTTPMethod:@"POST"];
    NSString *postString = [NSString stringWithFormat:@"email=%@&jabber_id=%@&device_identifier=%@", cemail,cuser, uuidString];
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request
                                                                  delegate:dd];
    
    if (connection) {
        // Create the NSMutableData to hold the received data.
        // receivedData is an instance variable declared elsewhere.
        dd.receivedData = [[NSMutableData data] retain];
    } else {
        // Inform the user that the connection failed.
    }

    
    
    
  
//    [bridge sendRegistered];
    
    
    
    
}


void MyClass::sendUserLoginDetailsToIdlecampus(){
    NSString *cuser = [NSString stringWithCString:user.c_str()
                                         encoding:[NSString defaultCStringEncoding]];
    NSString *cemail =  [NSString stringWithCString:email.c_str()
                                           encoding:[NSString defaultCStringEncoding]];
    NSURL *aUrl = [NSURL URLWithString:@"http://idlecampus.com/users/login"];  //http://10.124.7.63:3000/users
   
    
    
    //The user has been registered here. and his details are now sent to the server for storage.
    HTTPDelegate *dd = [[HTTPDelegate alloc] init];
    
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *uuidString = [defaults objectForKey:@"device_identifier"];
    cout << uuidString;
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:aUrl
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    
    [request setHTTPMethod:@"POST"];
    NSString *postString = [NSString stringWithFormat:@"email=%@&jabber_id=%@&device_identifier=%@", cemail,cuser, uuidString];
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request
                                                                  delegate:dd];
    
    if (connection) {
        // Create the NSMutableData to hold the received data.
        // receivedData is an instance variable declared elsewhere.
        dd.receivedData = [[NSMutableData data] retain];
    } else {
        // Inform the user that the connection failed.
    }
    
    
    
    
    
    //    [bridge sendRegistered];
    
    
    
    
}


void MyClass::disconnect(){
    
    disconnected = true;
    
    try
    
    {
        client->disconnect();
    }
    catch (int e)
    {
        cout << "An exception occurred. Exception Nr. " << e << endl;
    }
    
    
    
    
    
}

void MyClass::handleAlreadyRegistered(const gloox::JID& from) {
}

void MyClass::handleRegistrationResult(const gloox::JID& from, gloox::RegistrationResult regResult) {
    cout << regResult;
}

void MyClass::handleDataForm(const gloox::JID& from, const gloox::DataForm& form) {
}

void MyClass::handleOOB(const gloox::JID& from, const gloox::OOB& oob) {
}

void MyClass::handleItem(const gloox::JID& service,
        const std::string& node,
        const gloox::Tag *entry) {
}

/**
 * Receives the list of Items for a node.
 *
 * @param id The reply IQ's id.
 * @param service Service hosting the queried node.
 * @param node ID of the queried node (empty for the root node).
 * @param itemList List of contained items.
 * @param error Describes the error case if the request failed.
 *
 * @see Manager::requestItems()
 */
void MyClass::handleItems(const std::string& id,
        const gloox::JID& service,
        const std::string& node,
        const ::gloox::PubSub::ItemList& itemList,
        const gloox::Error *error) {


    NSMutableArray *itemsArray = [[NSMutableArray alloc] init];


    for (_List_const_iterator<gloox::PubSub::Item *> it = itemList.begin(); it != itemList.end(); ++it) {


        const gloox::Tag *tag = (*it)->tag();
        const gloox::Tag *message = (*(tag->findChildren("value").begin()));
        cout << message->cdata() << endl;

        [itemsArray addObject:[[NSString alloc] initWithUTF8String:message->cdata().c_str()]];


    }

    [groups setObject:itemsArray forKey:@"mygroups"];

    this->getGroupsJoined();


}


void MyClass::getSubscribersToGroup(string node) {
    gloox::JID *jid = new gloox::JID("pubsub.192.168.0.11");
    pubmgr->getSubscribers(*jid, node, this);
}

/**
 * Receives the result for an item publication.
 *
 * @param id The reply IQ's id.
 * @param service Service hosting the queried node.
 * @param node ID of the queried node. If empty, the root node has been queried.
 * @param itemList List of contained items.
 * @param error Describes the error case if the request failed.
 *
 * @see Manager::publishItem
 */
void MyClass::handleItemPublication(const std::string& id,
        const gloox::JID& service,
        const std::string& node,
        const gloox::PubSub::ItemList& itemList,
        const gloox::Error *error) {

}

/**
 * Receives the result of an item removal.
 *
 * @param id The reply IQ's id.
 * @param service Service hosting the queried node.
 * @param node ID of the queried node. If empty, the root node has been queried.
 * @param itemList List of contained items.
 * @param error Describes the error case if the request failed.
 *
 * @see Manager::deleteItem
 */
void MyClass::handleItemDeletion(const std::string& id,
        const gloox::JID& service,
        const std::string& node,
        const gloox::PubSub::ItemList& itemList,
        const gloox::Error *error) {
}

/**
 * Receives the subscription results. In case a problem occured, the
 * Subscription ID and SubscriptionType becomes irrelevant.
 *
 * @param id The reply IQ's id.
 * @param service PubSub service asked for subscription.
 * @param node Node asked for subscription.
 * @param sid Subscription ID.
 * @param jid Subscribed entity.
 * @param subType Type of the subscription.
 * @param error Subscription Error.
 *
 * @see Manager::subscribe
 */
void MyClass::handleSubscriptionResult(const std::string& id,
        const gloox::JID& service,
        const std::string& node,
        const std::string& sid,
        const gloox::JID& jid,
        const gloox::PubSub::SubscriptionType subType,
        const gloox::Error *error = 0) {
    cout << "a";
}

/**
 * Receives the unsubscription results. In case a problem occured, the
 * subscription ID becomes irrelevant.
 *
 * @param id The reply IQ's id.
 * @param service PubSub service.
 * @param error Unsubscription Error.
 *
 * @see Manager::unsubscribe
 */
void MyClass::handleUnsubscriptionResult(const std::string& id,
        const gloox::JID& service,
        const gloox::Error *error) {
}

/**
 * Receives the subscription options for a node.
 *
 * @param id The reply IQ's id.
 * @param service Service hosting the queried node.
 * @param jid Subscribed entity.
 * @param node ID of the node.
 * @param options Options DataForm.
 * @param error Subscription options retrieval Error.
 *
 * @see Manager::getSubscriptionOptions
 */
void MyClass::handleSubscriptionOptions(const std::string& id,
        const gloox::JID& service,
        const gloox::JID& jid,
        const std::string& node,
        const gloox::DataForm *options,
        const gloox::Error *error) {
}

/**
 * Receives the result for a subscription options modification.
 *
 * @param id The reply IQ's id.
 * @param service Service hosting the queried node.
 * @param jid Subscribed entity.
 * @param node ID of the queried node.
 * @param error Subscription options modification Error.
 *
 * @see Manager::setSubscriptionOptions
 */
void MyClass::handleSubscriptionOptionsResult(const std::string& id,
        const gloox::JID& service,
        const gloox::JID& jid,
        const std::string& node,
        const gloox::Error *error) {
}


/**
 * Receives the list of subscribers to a node.
 *
 * @param id The reply IQ's id.
 * @param service Service hosting the node.
 * @param node ID of the queried node.
 * @param list Subscriber list.
 * @param error Subscription options modification Error.
 *
 * @see Manager::getSubscribers
 */
void MyClass::handleSubscribers(const std::string& id,
        const gloox::JID& service,
        const std::string& node,
        const gloox::PubSub::SubscriberList *list,
        const gloox::Error *error) {


    for (_List_const_iterator<gloox::PubSub::Subscriber> it = list->begin(); it != list->end(); ++it) {


        cout << it->jid;


    }

}

/**
 * Receives the result of a subscriber list modification.
 *
 * @param id The reply IQ's id.
 * @param service Service hosting the node.
 * @param node ID of the queried node.
 * @param list Subscriber list.
 * @param error Subscriber list modification Error.
 *
 * @see Manager::setSubscribers
 */
void MyClass::handleSubscribersResult(const std::string& id,
        const gloox::JID& service,
        const std::string& node,
        const gloox::PubSub::SubscriberList *list,
        const gloox::Error *error) {
}

/**
 * Receives the affiliate list for a node.
 *
 * @param id The reply IQ's id.
 * @param service Service hosting the node.
 * @param node ID of the queried node.
 * @param list Affiliation list.
 * @param error Affiliation list retrieval Error.
 *
 * @see Manager::getAffiliations
 */
void MyClass::handleAffiliates(const std::string& id,
        const gloox::JID& service,
        const std::string& node,
        const gloox::PubSub::AffiliateList *list,
        const gloox::Error *error) {
}

/**
 * Handle the affiliate list for a specific node.
 *
 * @param id The reply IQ's id.
 * @param service Service hosting the node.
 * @param node ID of the node.
 * @param list The Affiliate list.
 * @param error Affiliation list modification Error.
 *
 * @see Manager::setAffiliations
 */
void MyClass::handleAffiliatesResult(const std::string& id,
        const gloox::JID& service,
        const std::string& node,
        const gloox::PubSub::AffiliateList *list,
        const gloox::Error *error) {
}


/**
 * Receives the configuration for a specific node.
 *
 * @param id The reply IQ's id.
 * @param service Service hosting the node.
 * @param node ID of the node.
 * @param config Configuration DataForm.
 * @param error Configuration retrieval Error.
 *
 * @see Manager::getNodeConfig
 */
void MyClass::handleNodeConfig(const std::string& id,
        const gloox::JID& service,
        const std::string& node,
        const gloox::DataForm *config,
        const gloox::Error *error) {
}

/**
 * Receives the result of a node's configuration modification.
 *
 * @param id The reply IQ's id.
 * @param service Service hosting the node.
 * @param node ID of the node.
 * @param error Configuration modification Error.
 *
 * @see Manager::setNodeConfig
 */
void MyClass::handleNodeConfigResult(const std::string& id,
        const gloox::JID& service,
        const std::string& node,
        const gloox::Error *error) {
}

/**
 * Receives the result of a node creation.
 *
 * @param id The reply IQ's id.
 * @param service Service hosting the node.
 * @param node ID of the node.
 * @param error Node creation Error.
 *
 * @see Manager::setNodeConfig
 */
void MyClass::handleNodeCreation(const std::string& id,
        const gloox::JID& service,
        const std::string& node,
        const gloox::Error *error) {
}

/**
 * Receives the result for a node removal.
 *
 * @param id The reply IQ's id.
 * @param service Service hosting the node.
 * @param node ID of the node.
 * @param error Node removal Error.
 *
 * @see Manager::deleteNode
 */
void MyClass::handleNodeDeletion(const std::string& id,
        const gloox::JID& service,
        const std::string& node,
        const gloox::Error *error) {
}


/**
 * Receives the result of a node purge request.
 *
 * @param id The reply IQ's id.
 * @param service Service hosting the node.
 * @param node ID of the node.
 * @param error Node purge Error.
 *
 * @see Manager::purgeNode
 */
void MyClass::handleNodePurge(const std::string& id,
        const gloox::JID& service,
        const std::string& node,
        const gloox::Error *error) {
}

/**
 * Receives the Subscription list for a specific service.
 *
 * @param id The reply IQ's id.
 * @param service The queried service.
 * @param subMap The map of node's subscription.
 * @param error Subscription list retrieval Error.
 *
 * @see Manager::getSubscriptions
 */
void MyClass::handleSubscriptions(const std::string& id,
        const gloox::JID& service,
        const gloox::PubSub::SubscriptionMap& subMap,
        const gloox::Error *error) {


    NSMutableArray *itemsArray = [[NSMutableArray alloc] init];


    typedef std::map<std::string, gloox::PubSub::SubscriptionList> SubscriptionMap;

    // show content:
    for (SubscriptionMap::const_iterator it = subMap.begin(); it != subMap.end(); ++it) {
        std::cout << it->first;
        [itemsArray addObject:[[NSString alloc] initWithUTF8String:it->first.c_str()]];
    }


    [groups setObject:itemsArray forKey:@"groupsfollowing"];


    [bridge sendNodeItems:groups];


}


/**
 * Receives the Affiliation map for a specific service.
 *
 * @param id The reply IQ's id.
 * @param service The queried service.
 * @param affMap The map of node's affiliation.
 * @param error Affiliation list retrieval Error.
 *
 * @see Manager::getAffiliations
 */
void MyClass::handleAffiliations(const std::string& id,
        const gloox::JID& service,
        const gloox::PubSub::AffiliationMap& affMap,
        const gloox::Error *error) {
}

/**
 * Receives the default configuration for a specific node type.
 *
 * @param id The reply IQ's id.
 * @param service The queried service.
 * @param config Configuration form for the node type.
 * @param error Default node config retrieval Error.
 *
 * @see Manager::getDefaultNodeConfig
 */
void MyClass::handleDefaultNodeConfig(const std::string& id,
        const gloox::JID& service,
        const gloox::DataForm *config,
        const gloox::Error *error) {
}

void MyClass::handleMUCParticipantPresence(gloox::MUCRoom *room, const gloox::MUCRoomParticipant participant,
        const gloox::Presence& presence) {
}

void MyClass::handleMUCMessage(gloox::MUCRoom *room, const gloox::Message& msg, bool priv) {
}

bool MyClass::handleMUCRoomCreation(gloox::MUCRoom *room) {
}

void MyClass::handleMUCSubject(gloox::MUCRoom *room, const std::string& nick,
        const std::string& subject) {
}

void MyClass::handleMUCInviteDecline(gloox::MUCRoom *room, const gloox::JID& invitee,
        const std::string& reason) {
}

void MyClass::handleMUCError(gloox::MUCRoom *room, gloox::StanzaError error) {
}

void MyClass::handleMUCInfo(gloox::MUCRoom *room, int features, const std::string& name,
        const gloox::DataForm *infoForm) {
}

void MyClass::handleMUCItems(gloox::MUCRoom *room, const gloox::Disco::ItemList& items) {
}

gloox::StringList MyClass::handleDiscoNodeFeatures(const gloox::JID& from, const std::string& node) {

}

/**
 * In addition to @c handleDiscoNodeFeatures, this function is used to gather
 * more information on a specific node. It is called when a disco#info query
 * arrives with a node attribute that matches the one registered for this handler.
 * @param from The sender of the request.
 * @param node The node this handler is supposed to handle.
 * @return A list of identities for the given node. The caller will own the identities.
 */
gloox::Disco::IdentityList MyClass::handleDiscoNodeIdentities(const gloox::JID& from,
        const std::string& node) {

}

/**
 * This function is used to gather more information on a specific node.
 * It is csalled when a disco#items query arrives with a node attribute that
 * matches the one registered for this handler. If node is empty, items for the
 * root node (no node) shall be returned.
 * @param from The sender of the request.
 * @param to The receiving JID (useful for transports).
 * @param node The node this handler is supposed to handle.
 * @return A list of items supported by this node.
 */
gloox::Disco::ItemList MyClass::handleDiscoNodeItems(const gloox::JID& from, const gloox::JID& to,
        const std::string& node) {

}


void MyClass::handleDiscoInfo(const gloox::JID& from, const gloox::Disco::Info& info, int context) {

}

/**
 * Reimplement this function if you want to be notified about the result
 * of a disco#items query.
 * @param from The sender of the disco#items result.
 * @param items The Items.
 * @param context A context identifier.
 * @since 1.0
 */
void MyClass::handleDiscoItems(const gloox::JID& from, const gloox::Disco::Items& items, int context) {

//    gloox::JID *jid = new gloox::JID("pubsub.192.168.0.11");
//    pubmgr->getSubscribers(*jid, "TEST", this);


    NSMutableArray *itemsArray = [[NSMutableArray alloc] init];


    typedef std::list<gloox::Disco::Item *> ItemList;
    const ItemList list = items.items();


    for (_List_const_iterator<gloox::Disco::Item *> it = list.begin(); it != list.end(); ++it) {

        [itemsArray addObject:[[NSString alloc] initWithUTF8String:(*it)->node().c_str()]];

        cout << (*it)->tag()->xml();

        cout << items.tag()->xml();

    }

    [groups setObject:itemsArray forKey:@"mygroups"];

    this->getGroupsJoined();


}

void MyClass::getGroups() {
    gloox::JID *jid1 = new gloox::JID("pubsub.idlecampus.com");

//    gloox::Disco *disco = client->disco();
//    disco->getDiscoItems(*jid1,jid->full()+"/groups" , this, 0);
    cout << jid->full() + "/groups";
    pubmgr->requestItems(*jid1, jid->full() + "/groups", "", 10, this);
}

/**
 * Reimplement this function to receive disco error notifications.
 * @param from The sender of the error result.
 * @param error The Error. May be 0.
 * @param context A context identifier.
 * @since 1.0
 */
void MyClass::handleDiscoError(const gloox::JID& from, const gloox::Error *error, int context) {

}

/**
 * Reimplement this function to receive notifications about incoming IQ
 * stanzas of type 'set' in the disco namespace.
 * @param iq The full IQ.
 * @return Returns @b true if the stanza was handled and answered, @b false otherwise.
 */
bool MyClass::handleIq(const gloox::IQ& iq) {
    cout << iq.tag()->xml();

}

/**
 * Reimplement this function if you want to be notified about
 * incoming IQs with a specific value of the @c id attribute. You
 * have to enable tracking of those IDs using Client::trackID().
 * This is usually useful for IDs that generate a positive reply, i.e.
 * &lt;iq type='result' id='reg'/&gt; where a namespace filter wouldn't
 * work.
 * @param iq The complete IQ stanza.
 * @param context A value to restore context, stored with ClientBase::trackID().
 * @note Only sIQ stanzas of type 'result' or 'error' can arrive here.
 * @since 1.0
 */
void MyClass::handleIqID(const gloox::IQ& iq, int context) {
    cout << iq.tag()->xml();
    cout << context;
}



