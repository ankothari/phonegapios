#import <Cordova/CDV.h>
#import "XMPPBridge.h"
@interface XMPP : CDVPlugin{
    NSMutableDictionary *commands;
    XMPPBridge *bridge;
    NSString *rosterid;
    NSMutableData *receivedData;
    NSURLConnection *conn;
}

- (void)echo:(CDVInvokedUrlCommand*)command;
- (void)send:(CDVInvokedUrlCommand*)command;
- (void)login:(CDVInvokedUrlCommand*)command;
- (void)login1:(CDVInvokedUrlCommand*)command;
- (void)registerUser:(CDVInvokedUrlCommand*)command;
-(void)receiveMessage:(CDVInvokedUrlCommand*)command;
-(void)subscribeTo:(CDVInvokedUrlCommand*)command;
-(void)on_presence:(CDVInvokedUrlCommand*)command;
-(void)auth_user:(CDVInvokedUrlCommand*)command;
-(void)create_group:(CDVInvokedUrlCommand*)command;
-(void)join_group:(CDVInvokedUrlCommand*)command;
-(void)publish_message:(CDVInvokedUrlCommand*)command;
- (void)get_group_name:(CDVInvokedUrlCommand*)command;
@end

