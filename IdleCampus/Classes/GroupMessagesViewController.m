//
//  GroupMessagesViewController.m
//  IdleCampus
//
//  Created by Ankur Kothari on 5/7/13.
//
//

#import "GroupMessagesViewController.h"
#import <AudioToolbox/AudioServices.h>
@interface GroupMessagesViewController ()

@end

@implementation GroupMessagesViewController

@synthesize tableData,tableView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)gotMessage:(NSNotification *) notification{
//    NSString *soundPath = [[NSBundle mainBundle] pathForResource:@"sms-received" ofType:@"wav"];
//    SystemSoundID soundID;
//    AudioServicesCreateSystemSoundID((CFURLRef)[NSURL fileURLWithPath: soundPath], &soundID);
//    AudioServicesPlaySystemSound (soundID);
//    //[soundPath release];
//    NSLog(@"soundpath retain count: %d", [soundPath retainCount]);
    int so = 0;
    
    NSString *snew = [NSString stringWithFormat:@"%d",so];
    [[NSUserDefaults standardUserDefaults] setObject:snew  forKey:@"numberOfUnreadMails"];
    
    
    NSDictionary *msgDict = [notification userInfo];
    
    NSString* msg = [msgDict objectForKey:@"mesage"];
    
    [tableData insertObject:msg atIndex:0];
   
    [self.tableView performSelectorOnMainThread:@selector(reloadData)
                                     withObject:nil
                                  waitUntilDone:NO];
    
    
    
    
    
}

-(void)viewWillAppear:(BOOL)animated{
 



     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotMessage:) name:@"receivedMessage" object:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
     [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)viewDidLoad
{
    
    
    
    [super viewDidLoad];
     [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed: @"topBlueBarWithText1.png"] forBarMetrics:UIBarMetricsDefault];
    
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [leftButton setUserInteractionEnabled:NO];
    [leftButton setImage:[UIImage imageNamed:@"backButtonWithText.png"] forState:UIControlStateNormal];
    leftButton.frame = CGRectMake(0, 0, 44, 30);
    [leftButton addTarget:self action:@selector(goback:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    [leftButton release];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [leftButton setUserInteractionEnabled:NO];
    [rightButton setImage:[UIImage imageNamed:@"ClearButtonWithText.png"] forState:UIControlStateNormal];
    rightButton.frame = CGRectMake(0, 0, 44, 30);
    [rightButton addTarget:self action:@selector(clearMessages:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    [rightButton release];
    
     tableData = [[NSMutableArray alloc]initWithArray:[[NSUserDefaults standardUserDefaults]objectForKey:@"receivedMessages"]];
}

-(void)clearMessages:(id)sender{
    [self.tableData removeAllObjects];
    [self.tableView reloadData];
    [[NSUserDefaults standardUserDefaults]setObject:self.tableData forKey:@"receivedMessages"];
}


- (void)goback:(id) sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    //    cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
    
    NSString *string = [tableData objectAtIndex:indexPath.row];
    CGSize stringSize = [string sizeWithFont:[UIFont boldSystemFontOfSize:15] constrainedToSize:CGSizeMake(320, 9999) lineBreakMode:UILineBreakModeWordWrap];
    
    UITextView *textV=[[UITextView alloc] initWithFrame:CGRectMake(5, 5, 290, stringSize.height+10)];
    textV.font = [UIFont systemFontOfSize:15.0];
    textV.text=string;
    textV.textColor=[UIColor blackColor];
    textV.editable=NO;
    textV.scrollEnabled = NO;
    [cell.contentView addSubview:textV];
    [textV release];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *textSelected = [tableData objectAtIndex:indexPath.row];
   
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    
    NSString *string = [tableData objectAtIndex:indexPath.row];
    CGSize stringSize = [string sizeWithFont:[UIFont boldSystemFontOfSize:15]
                           constrainedToSize:CGSizeMake(320, 9999)
                               lineBreakMode:UILineBreakModeWordWrap];
    
    return stringSize.height+25;
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
