//
//  MyGroupViewController.h
//  IdleCampus
//
//  Created by Ankur Kothari on 5/7/13.
//
//

#import <UIKit/UIKit.h>

@interface MyGroupViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>{
     NSArray *tableData;
}
-(UITableViewCell *) getCellAt:(NSInteger)index;
@property(retain,nonatomic) IBOutlet UIToolbar *topBar;
@property(retain,nonatomic) NSArray *tableData;
@property (nonatomic, retain) IBOutlet UILabel *groupHeader;
@property (nonatomic, retain) NSString *group;
@property(retain,nonatomic) IBOutlet UITableView *tableView;
@end
