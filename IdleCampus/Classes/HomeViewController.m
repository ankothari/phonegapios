//
//  HomeViewController.m
//  IdleCampus
//
//  Created by Ankur Kothari on 5/7/13.
//
//

#import "HomeViewController.h"

#import "RegisterViewController.h"
#import "MyGroupViewController.h"
#import "ForgotPasswordViewController.h"
#import "Reachability.h"
@interface HomeViewController ()

@end


NSString *const kXMPPmyJID = @"kXMPPmyJID";
NSString *const kXMPPmyPassword = @"kXMPPmyPassword";

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) //1
@implementation HomeViewController
@synthesize username,password,signinButton;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
    
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
    username.text = [[NSUserDefaults standardUserDefaults] stringForKey:kXMPPmyJID];
    password.text = [[NSUserDefaults standardUserDefaults] stringForKey:kXMPPmyPassword];
    
   
    
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
  
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)loginUser:(id)sender{
    
     [username resignFirstResponder];
    [password resignFirstResponder];
    NSString *susername = username.text;
    NSString *spassword = password.text;
    int s = [susername length];
    if(susername == NULL || [susername length]==0){
        
        [[[[UIAlertView alloc]
           initWithTitle:@"Error"
           message:@"Please enter a username"
           delegate:self
           cancelButtonTitle:@"Ok"
           otherButtonTitles: nil] autorelease] show];
    }
    else if(spassword == NULL || [spassword length] == 0){
        
        [[[[UIAlertView alloc]
           initWithTitle:@"Error"
           message:@"Please enter a password"
           delegate:self
           cancelButtonTitle:@"Ok"
           otherButtonTitles: nil] autorelease] show];
    }
  
    
    
    else{

    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //       hud.mode = MBProgressHUDModeAnnularDeterminate;
    hud.labelText = @"Loading";
        if(xmpp == nil){
            xmpp = [XMPPNative sharedManager];
    xmpp.home = self;
   [xmpp gotRoster];
        }
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        
        [defaults setObject:username.text forKey:kXMPPmyJID];
        [defaults setObject:password.text forKey:kXMPPmyPassword];
        [defaults synchronize];
   
    [xmpp login:username.text password:password.text ];
//
   
    
    }
}

-(void)loggedin{
    [xmpp gotGroups];
}

-(void)gotGroups{
    
}


-(IBAction)registerUser:(id)sender{
    RegisterViewController *registerViewController = [[RegisterViewController alloc]
                                                      initWithNibName:@"RegisterViewController"
                                                      bundle:nil];
    
    [self.navigationController pushViewController:registerViewController animated:YES];
    
//    [self.view addSubview:registerViewController.view];
  
}

-(IBAction)userForgotPassword:(id)sender{
    ForgotPasswordViewController *forgotPasswordViewController = [[ForgotPasswordViewController alloc]
                                                      initWithNibName:@"ForgotPasswordViewController"
                                                      bundle:nil];
    
    [self.navigationController pushViewController:forgotPasswordViewController animated:YES];
//    [self.view addSubview:forgotPasswordViewController.view];
}

-(void)getTimetable:(NSString *)group{
    Timetable *timetable = [[Timetable alloc]init];
    NSString *surl = [NSString stringWithFormat:@"http://idlecampus.com/timetable/get_timetable_for_group?group=%@",group];
    [timetable notification:surl];
    
    
}

-(void)showGroup:(NSString *)group{
    MyGroupViewController *myGroupViewController = [[MyGroupViewController alloc]init];
    
    [self.view addSubview:myGroupViewController.view];

}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Private
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)setField:(UITextField *)field forKey:(NSString *)key
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (field.text != nil)
    {
        [defaults setObject:field.text forKey:key];
    } else {
        [defaults removeObjectForKey:key];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Actions
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return [super shouldAutorotateToInterfaceOrientation:interfaceOrientation];
}

- (IBAction)hideKeyboard:(id)sender {
    [sender resignFirstResponder];
//    [self done:sender];
}


@end
