
#import "XMPP.h"
#import "XMPPBridge.h"
#import <Cordova/CDV.h>


#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) //1



#import "AppDelegate.h"



@implementation XMPP

- (void)echo:(CDVInvokedUrlCommand*)command
{
    //[commands setObject:command forKey:@"echo"];
    CDVPluginResult* pluginResult = nil;
    NSString* echo = [command.arguments objectAtIndex:0];
    
    if (echo != nil && [echo length] > 0) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:echo];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}
- (void)login:(CDVInvokedUrlCommand*)command
{
       bridge = [[XMPPBridge alloc]init];
    [commands setObject:command forKey:@"login"];
    NSLog(@"login");
    CDVPluginResult* pluginResult = nil;
    NSString* username = [command.arguments objectAtIndex:0];
    NSString* password = [command.arguments objectAtIndex:1];
    NSArray *a = [[NSArray alloc]initWithObjects:username,password,nil ];
    if (username != nil && [username length] > 0) {
       
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"true"];
        [pluginResult setKeepCallbackAsBool:YES];
        
        bool b = [bridge connectWithUser:username password:password];
        
//        dispatch_async( dispatch_get_main_queue(), ^{
//        [self openapp:@"a"];
//        });
        
        
        
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)login1:(CDVInvokedUrlCommand*)command
{
    bridge = [[XMPPBridge alloc]init];
    [commands setObject:command forKey:@"login1"];
    NSLog(@"login");
    CDVPluginResult* pluginResult = nil;
    NSString* username = [command.arguments objectAtIndex:0];
    NSString* password = [command.arguments objectAtIndex:1];
    NSArray *a = [[NSArray alloc]initWithObjects:username,password,nil ];
    if (username != nil && [username length] > 0) {
        
       
        
        bool b = [bridge connectWithUser:username password:password];
        
        //        dispatch_async( dispatch_get_main_queue(), ^{
        //        [self openapp:@"a"];
        //        });
        
        
        
    }
}

//-(void)openapp:(NSString *)l{
//    NSString *tr = [NSString stringWithFormat:@"xmpp.open_app()"];;
////[[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://10.124.4.129:8888/idlecampus-client/apps/bingo/index.html"]];
//    
//    NSString *urlAddress =  @"http://10.124.4.129:8888/idlecampus-client/apps/bingo/index.html";
//    
//    //Create a URL object.
//    NSURL *url = [NSURL URLWithString:urlAddress];
//    //URL Requst Object
//    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
//    
//    //Load the request in the UIWebView.
//   // [detailWebView loadRequest:requestObj];
//AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
////    
//[appDelegate.viewController.webView loadRequest:requestObj];
//}
- (void)registerUser:(CDVInvokedUrlCommand*)command
{
    bridge = [[XMPPBridge alloc]init];
    [commands setObject:command forKey:@"registered"];
    CDVPluginResult* pluginResult = nil;
    NSString* username = [command.arguments objectAtIndex:0];
    NSString* password = [command.arguments objectAtIndex:1];
    
     NSString* email = [command.arguments objectAtIndex:3];
    NSString* group = [command.arguments objectAtIndex:2];
    
    
    NSArray *a = [[NSArray alloc]initWithObjects:username,password,nil ];
    if (username != nil && [username length] > 0) {
//        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"true"];
//        [pluginResult setKeepCallbackAsBool:YES];

        bool b = [bridge registerUser:username password:password group:group email:email];

//    } else {
//        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
//    }
//    
//    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        
    }
}


-(void)receiveMessage:(CDVInvokedUrlCommand*)command{
   
        [commands setObject:command forKey:@"receive"];
    
     
    
     
}
-(void)create_group:(CDVInvokedUrlCommand*)command{
    
    CDVPluginResult* pluginResult = nil;
    NSString* message = [command.arguments objectAtIndex:0];
    
    if (message != nil && message > 0) {
        [bridge create_group:message];
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:message];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    
    
    
    
}

-(void)join_group:(CDVInvokedUrlCommand*)command{
    
    CDVPluginResult* pluginResult = nil;
    NSString* message = [command.arguments objectAtIndex:0];
    
    if (message != nil && message > 0) {
        [bridge join_group:message];
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:message];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}
-(void)receiveTestNotification:(NSNotification *) notification{
    
           NSDictionary *userInfo = notification.userInfo;
        NSString *s = [userInfo objectForKey:@"s"];
    NSString *presence = [userInfo objectForKey:@"presence"];
        NSMutableArray *roster = [userInfo objectForKey:@"roster"];
    NSMutableDictionary *groups = [userInfo objectForKey:@"groups"];
     NSMutableDictionary *registered = [userInfo objectForKey:@"registered"];
    int registered_int = [[userInfo objectForKey:@"registered"] intValue];
    int login1_int = [[userInfo objectForKey:@"login1"] intValue];
   
  
       
        CDVPluginResult* pluginResult = nil;
        if ([roster count]>0) {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsArray:roster];
            [pluginResult setKeepCallbackAsBool:YES];
            CDVInvokedUrlCommand *rcommand = [commands objectForKey:@"roster"];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:rcommand.callbackId];
        }

    if ([groups count]>0) {
//        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:groups];
        
        
        if([[groups objectForKey:@"mygroups"] count]>0){
            NSString* urlString = [NSString stringWithFormat:@"http://idlecampus.com/groups/get_group_name?group_code=%@",[[groups objectForKey:@"mygroups"] objectAtIndex:0]];
            
            NSURL *group_name_url = [NSURL URLWithString:urlString];        // Create the request.
            
            dispatch_async(kBgQueue, ^{
                
                
                
                NSData* data = [NSData dataWithContentsOfURL:
                                group_name_url];
                
                NSLog(@"%@", [[groups objectForKey:@"mygroups"] objectAtIndex:0]);
                [self performSelectorOnMainThread:@selector(getMyGroup:)
                                       withObject:data waitUntilDone:YES];
            });
        }
        else{
        NSString* urlString = [NSString stringWithFormat:@"http://idlecampus.com/groups/get_group_name?group_code=%@",[[groups objectForKey:@"groupsfollowing"] objectAtIndex:0]];
    
        NSURL *group_name_url = [NSURL URLWithString:urlString];        // Create the request.
        
        dispatch_async(kBgQueue, ^{
            
            
            
            NSData* data = [NSData dataWithContentsOfURL:
                            group_name_url];
            
            NSLog(@"%@", [[groups objectForKey:@"groupsfollowing"] objectAtIndex:0]);
            [self performSelectorOnMainThread:@selector(getGroupFollowing:)
                                   withObject:data waitUntilDone:YES];
        });
        }
        
               
//        [pluginResult setKeepCallbackAsBool:YES];
//        CDVInvokedUrlCommand *rcommand = [commands objectForKey:@"groups"];
//        [self.commandDelegate sendPluginResult:pluginResult callbackId:rcommand.callbackId];
    }
       if([s length]>0){
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:s];
            [pluginResult setKeepCallbackAsBool:YES];
            CDVInvokedUrlCommand *command = [commands objectForKey:@"receive"];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }
    
    if([presence length]>0){
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:presence];
        [pluginResult setKeepCallbackAsBool:YES];
        CDVInvokedUrlCommand *command = [commands objectForKey:@"presence"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }



    
    if(registered_int){
        
        
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsInt:registered_int];
        [pluginResult setKeepCallbackAsBool:YES];
        CDVInvokedUrlCommand *command = [commands objectForKey:@"registered"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
    
    
    if(login1_int){
        
        
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsInt:login1_int];
        [pluginResult setKeepCallbackAsBool:YES];
        CDVInvokedUrlCommand *command = [commands objectForKey:@"login1"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }






    
}

- (void)getGroupFollowing:(NSData *)responseData {
    //parse out the json data
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData //1
                          
                          options:kNilOptions
                          error:&error];
    
    NSString* group_name = [json objectForKey:@"group_name"]; //2
    NSString* group_code = [json objectForKey:@"group_code"]; //2
    CDVPluginResult* pluginResult = nil;
    
    //NSMutableDictionary myDictionary = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *myDictionary = [[NSMutableDictionary alloc] init];
    
    
    NSNumber *code = [NSNumber numberWithInt:2];
    [myDictionary setObject:code  forKey:@"code"];
    [myDictionary setObject:group_code  forKey:@"group_code"];
    [myDictionary setObject:group_name  forKey:@"name"];
    
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:myDictionary];
    
    
//    [pluginResult setKeepCallbackAsBool:YES];
    CDVInvokedUrlCommand *rcommand = [commands objectForKey:@"groups"];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:rcommand.callbackId];

    
    NSLog(@"group_name: %@", group_name); //3
}
- (void)getMyGroup:(NSData *)responseData {
    //parse out the json data
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData //1
                          
                          options:kNilOptions
                          error:&error];
    
    NSString* group_name = [json objectForKey:@"group_name"]; //2
    NSString* group_code = [json objectForKey:@"group_code"]; //2
    CDVPluginResult* pluginResult = nil;
    
    //NSMutableDictionary myDictionary = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *myDictionary = [[NSMutableDictionary alloc] init];
    
   
        NSNumber *code = [NSNumber numberWithInt:1];
        [myDictionary setObject:code  forKey:@"code"];
    [myDictionary setObject:group_code  forKey:@"group_code"];
        [myDictionary setObject:group_name  forKey:@"name"];
       
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:myDictionary];
    
    
    //    [pluginResult setKeepCallbackAsBool:YES];
    CDVInvokedUrlCommand *rcommand = [commands objectForKey:@"groups"];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:rcommand.callbackId];
    
    
    NSLog(@"group_name: %@", group_name); //3
}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // This method is called when the server has determined that it
    // has enough information to create the NSURLResponse.
    
    // It can be called multiple times, for example in the case of a
    // redirect, so each time we reset the data.
    
    // receivedData is an instance variable declared elsewhere.
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the new data to receivedData.
    // receivedData is an instance variable declared elsewhere.
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
    // release the connection, and the data object
    [connection release];
    // receivedData is declared as a method instance elsewhere
    [receivedData release];
    
    // inform the user
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // do something with the data
    // receivedData is declared as a method instance elsewhere
    NSLog(@"Succeeded! Received %d bytes of data",[receivedData length]);
    
    // release the connection, and the data object
    [connection release];
    [receivedData release];
}

- (void)disconnect:(CDVInvokedUrlCommand*)command{
    CDVPluginResult* pluginResult = nil;
    [commands removeAllObjects];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
        [bridge disconnect];
           
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)removeAllObjects:(CDVInvokedUrlCommand*)command{
    CDVPluginResult* pluginResult = nil;
    [commands removeAllObjects];
     [[NSNotificationCenter defaultCenter] removeObserver:self];
    
  
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)send:(CDVInvokedUrlCommand*)command{
    CDVPluginResult* pluginResult = nil;
    NSString* message = [command.arguments objectAtIndex:0];
    NSString* to = [command.arguments objectAtIndex:1];
    if (message != nil && message > 0) {
        [bridge send:message to:to];
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:message];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

}




-(void)publish_message:(CDVInvokedUrlCommand*)command{
    CDVPluginResult* pluginResult = nil;
    NSString* to = [command.arguments objectAtIndex:0];
    NSString* message = [command.arguments objectAtIndex:1];
    if (message != nil && message > 0) {
        [bridge publishMessage:message to:to];
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:message];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
   
}
-(void)gotRoster:(CDVInvokedUrlCommand*)command{
        
           [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(receiveTestNotification:)
                                                     name:@"TestNotification"
                                                   object:nil];
        commands = [[NSMutableDictionary alloc]initWithObjects:nil forKeys:nil];
        [commands setObject:command forKey:@"roster"];
    

}

-(void)gotGroups:(CDVInvokedUrlCommand*)command{
    
    
    [commands setObject:command forKey:@"groups"];
   
     [bridge getGroups];
    
    
    
}
-(void)subscribeTo:(CDVInvokedUrlCommand*)command{
    CDVPluginResult* pluginResult = nil;
    NSString* user = [command.arguments objectAtIndex:0];
    
    if (user != nil && user > 0) {
        [bridge subscribe:user];
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:user];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

-(void)on_presence:(CDVInvokedUrlCommand*)command{
    [commands setObject:command forKey:@"presence"];
}
-(void)auth_user:(CDVInvokedUrlCommand*)command{
    CDVPluginResult* pluginResult = nil;
    NSString* message = [command.arguments objectAtIndex:0];
    NSString* to = [command.arguments objectAtIndex:1];
    if (message != nil && message > 0) {
        [bridge auth_user:message to:to];
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:message];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}
@end

