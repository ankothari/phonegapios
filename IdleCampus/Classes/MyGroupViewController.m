//
//  MyGroupViewController.m
//  IdleCampus
//
//  Created by Ankur Kothari on 5/7/13.
//
//

#import "MyGroupViewController.h"
#import "WeekdaysViewController.h"
#import "GroupMessagesViewController.h"
#import "GroupTableCellViewController.h"
#import "Reachability.h"
#import <AudioToolbox/AudioServices.h>
#import "BingoViewController.h"
#import "RosterViewController.h"
#import "Bingo.h"
@interface MyGroupViewController ()

@end

@implementation MyGroupViewController
@synthesize tableData,topBar,groupHeader,group,tableView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
      
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotMessage:) name:@"receivedMessage" object:nil];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newBingoGame:) name:@"newBingoGame" object:nil];
    
    [self.tableView performSelectorOnMainThread:@selector(reloadData)
                                     withObject:nil
                                  waitUntilDone:NO];
    
   
    
    [super viewWillAppear:animated];
}

-(void)newBingoGame:(NSNotification *)notification{
    
    NSDictionary *msgDict = [notification userInfo];
    
    NSString* fromUser = [msgDict objectForKey:@"fromUser"];
    

    BingoViewController *bingoViewController = [[BingoViewController alloc]
                                                initWithNibName:@"BingoViewController"
                                                bundle:nil];
    
    bingoViewController.userPlayingBingoWith = [[NSString alloc]initWithString:fromUser];
    
    
    ((Bingo *)bingoViewController.view).turn.text = @"Your turn";
    
    
    
    
    

    [self.navigationController pushViewController:bingoViewController animated:YES];
    
    
    
    
    
    [bingoViewController release];
}

- (void)gotMessage:(NSNotification *) notification{
    
   NSString *s = [[NSUserDefaults standardUserDefaults]objectForKey:@"numberOfUnreadMails"];
    if([s length] == 0){
        int so = [s intValue];
        so++;
        NSString *snew = [NSString stringWithFormat:@"%d",so];
        [[NSUserDefaults standardUserDefaults] setObject:snew  forKey:@"numberOfUnreadMails"];

    }
    else{
        int so = [s intValue];
        so++;
        NSString *snew = [NSString stringWithFormat:@"%d",so];
        [[NSUserDefaults standardUserDefaults] setObject:snew  forKey:@"numberOfUnreadMails"];
    }
    [self.tableView performSelectorOnMainThread:@selector(reloadData)
                                     withObject:nil
                                  waitUntilDone:NO];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
     [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}


- (void)viewDidLoad
{
    
   
    
   
    
    
      groupHeader.text = group;
    [super viewDidLoad];
    // Set the background image for PortraitMode
    self.navigationItem.hidesBackButton = YES;
 [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed: @"topBlueBarWithText1.png"] forBarMetrics:UIBarMetricsDefault];
//    [self.topBar setBackgroundImage:[UIImage imageNamed:@"topBlueBarWithText1.png"] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
//    // create button
//    UIButton* backButton = [UIButton buttonWithType:101]; // left-pointing shape!
//    [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
//    [backButton setTitle:@"Back" forState:UIControlStateNormal];
//    [backButton setBackgroundImage:[UIImage imageNamed:@"backButton.png"] forState:UIControlStateSelected];
//    // create button item -- possible because UIButton subclasses UIView!
//    UIBarButtonItem* backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    // add to toolbar, or to a navbar (you should only have one of these!)
//    [self.topBar setItems:[NSArray arrayWithObject:backItem]];
    tableData =  [[NSArray alloc] initWithObjects:@"Timetable",nil];
    //@"Messages",@"Bingo",@"Friends",
}


-(void)viewDidAppear:(BOOL)animated{
    
}
-(void)viewDidUnload{
    
}




-(void)backAction{
    [self.view removeFromSuperview];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"GroupTableCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        // Load the top-level objects from the custom cell XIB.
        
        
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"GroupTableCell" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    NSString *text = [tableData objectAtIndex:indexPath.row];

    
//    cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
     UILabel *label = (UILabel *)[cell viewWithTag:1];
    label.text =  text;
    
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:2];
    if([text isEqualToString:@"Timetable"]){
      UIImage *image = [UIImage imageNamed: @"timetable.png"];
        [imageView setImage:image];
    }
//    else if([text isEqualToString:@"Messages"]){
//        UIImage *image = [UIImage imageNamed: @"MailIcon.png"];
//        [imageView setImage:image];
//        
//        UILabel *numberOfMails = ((GroupTableCellViewController *)cell).msgs;
//        NSString *str = [[NSUserDefaults standardUserDefaults]objectForKey:@"numberOfUnreadMails"];
//         numberOfMails.text = str ;
//
//    }
//    else if([text isEqualToString:@"Bingo"]){
//        
//        UILabel *numberOfMails = ((GroupTableCellViewController *)cell).msgs;
//        NSString *str = @"0 Friends playing";
//        numberOfMails.text = str ;
//        
//    }
    
    
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *textSelected = [tableData objectAtIndex:indexPath.row];
    if([textSelected isEqualToString:@"Timetable"]){
        WeekdaysViewController *weekdaysViewController = [[WeekdaysViewController alloc]
                                                        initWithNibName:@"WeekdaysViewController"
                                                        bundle:nil];
        
        [self.navigationController pushViewController:weekdaysViewController animated:YES];
        [weekdaysViewController release];
        
//        [self.view addSubview:weekdaysViewController.view];
    }
    if([textSelected isEqualToString:@"Messages"]){
        GroupMessagesViewController *groupMessagesViewController = [[GroupMessagesViewController alloc]
                                                        initWithNibName:@"GroupMessagesViewController"
                                                        bundle:nil];
        
        [self.navigationController pushViewController:groupMessagesViewController animated:YES];
        
        
        int so = 0;
        
        NSString *snew = [NSString stringWithFormat:@"%d",so];
        [[NSUserDefaults standardUserDefaults] setObject:snew  forKey:@"numberOfUnreadMails"];
        
        
        [groupMessagesViewController release];
//        [self.view addSubview:groupMessagesViewController.view];
    }

    if([textSelected isEqualToString:@"Bingo"]){
        BingoViewController *bingoViewController = [[BingoViewController alloc]
                                                                    initWithNibName:@"BingoViewController"
                                                                    bundle:nil];
        
        [self.navigationController pushViewController:bingoViewController animated:YES];
        
        
       
        
        
        [bingoViewController release];
           }
    if([textSelected isEqualToString:@"Friends"]){
        RosterViewController *rosterViewController = [[RosterViewController alloc]
                                                    initWithNibName:@"RosterViewController"
                                                    bundle:nil];
        
        [self.navigationController pushViewController:rosterViewController animated:YES];
        
        
        
        
        
        [rosterViewController release];
    }

}

-(UITableViewCell *) getCellAt:(NSInteger)index{
    NSUInteger indexArr[] = {0,index};  // First one is the section, second the row
    
    NSIndexPath *myPath = [NSIndexPath indexPathWithIndexes:indexArr length:2];
    
    return [self tableView:self.tableView cellForRowAtIndexPath:myPath];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
