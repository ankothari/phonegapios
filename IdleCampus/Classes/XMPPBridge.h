//
//  XMPPBridge.h
//  IdleCampus
//
//  Created by Ankur Kothari on 12/4/12.
//
//

#import <Foundation/Foundation.h>
#import "XMPPDelegate.h"
#ifdef __cplusplus
class MyClass;
#endif
@interface XMPPBridge : NSObject<XMPPDelegate>{
    NSMutableDictionary *commands;
#ifdef __cplusplus
    MyClass *handler;
#endif
}
-(bool)connectWithUser:(NSString *)user password:(NSString *)password;
-(bool)registerUser:(NSString *)user password:(NSString *)lpassword group:(NSString *)lgroup email:(NSString *)lemail;
-(void)send:(NSString *)message to:(NSString *)to;
-(void)sendBingo:(NSString *)message to:(NSString *)to;
-(void)callNotify:(NSString *)message fromUser:(NSString *)fromUser fromApp:(NSString *)fromApp;
-(void)sendRoster:(NSMutableDictionary *)dict;
-(void)subscribe:(NSString *)user;
-(void)gotSubscriptionRequest:(NSString *)user;
-(void)auth_user:(NSString *)ack to:(NSString *)to;
-(void)create_group:(NSString *)group;
-(void)join_group:(NSString *)group;
-(void)sendNodeItems:(NSMutableDictionary *)items;
-(void)sendJoinedNodeItems:(NSMutableArray *)items;
-(void)getGroups;
-(void)sendRegistered:(int)b;
-(void)sendLogin:(int)b;
-(void)sendLogin1:(int)b;
-(void)disconnect;
-(void)publishMessage:(NSString *)message to:(NSString *)to;
@property (nonatomic, assign) dispatch_queue_t queue;
@end

