//
//  SplashScreenViewController.h
//  IdleCampus
//
//  Created by Ankur Kothari on 6/7/13.
//
//

#import <UIKit/UIKit.h>
@class XMPPNative;
extern NSString *const kXMPPmyJID;
extern NSString *const kXMPPmyPassword;
@interface SplashScreenViewController : UIViewController
@property (retain,nonatomic) XMPPNative *xmpp;
@end
