//
//  JoinGroupViewController.h
//  IdleCampus
//
//  Created by Ankur Kothari on 6/7/13.
//
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "XMPPNative.h"
extern NSString *const kXMPPmyJID;
extern NSString *const kXMPPmyPassword;
@interface JoinGroupViewController : UIViewController{
    NSString *username;
    NSString *password;
    NSString *email;
    XMPPNative *xmpp;
    MBProgressHUD *hud;
}
@property (retain, nonatomic)MBProgressHUD *hud;

@property (retain,nonatomic) XMPPNative *xmpp;

@property (nonatomic,retain) IBOutlet UITextField *group;

@property(nonatomic, retain) NSString *username;
@property(nonatomic, retain) NSString *password;
@property(nonatomic, retain) NSString *email;


-(IBAction)joinGroup:(id)sender;
- (IBAction)hideKeyboard:(id)sender;
@end
