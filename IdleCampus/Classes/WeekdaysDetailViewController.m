//
//  WeekdaysDetailViewController.m
//  IdleCampus
//
//  Created by Ankur Kothari on 5/7/13.
//
//

#import "WeekdaysDetailViewController.h"
#import <AudioToolbox/AudioServices.h>
@interface WeekdaysDetailViewController ()

@end

@implementation WeekdaysDetailViewController
@synthesize weekday,weekdayHeader,tableData,topBar;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotMessage:) name:@"receivedMessage" object:nil];
    
    
    weekdayHeader.text = weekday;
    
    NSMutableDictionary *timetable = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:@"timetable"]];
    
     [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed: @"topBlueBarWithText1.png"] forBarMetrics:UIBarMetricsDefault];
    
    
    NSMutableArray *weekdayinfo = [timetable objectForKey:weekday];
    tableData =  [[NSArray alloc] initWithArray:weekdayinfo];
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [leftButton setUserInteractionEnabled:NO];
    [leftButton setImage:[UIImage imageNamed:@"backButtonWithText.png"] forState:UIControlStateNormal];
    leftButton.frame = CGRectMake(0, 0, 44, 30);
    [leftButton addTarget:self action:@selector(goback:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    [self.navigationItem setLeftBarButtonItem:item animated:NO];
    [leftButton release];

    [super viewWillAppear:animated];
}

       
    - (void)gotMessage:(NSNotification *) notification{
        
        
        NSDictionary *msgDict = [notification userInfo];
        
        NSString* msg = [msgDict objectForKey:@"mesage"];
        
        NSString *s = [[NSUserDefaults standardUserDefaults]objectForKey:@"numberOfUnreadMails"];
        if([s length] == 0){
            int so = [s intValue];
            so++;
            NSString *snew = [NSString stringWithFormat:@"%d",so];
            [[NSUserDefaults standardUserDefaults] setObject:snew  forKey:@"numberOfUnreadMails"];
            
        }
        else{
            int so = [s intValue];
            so++;
            NSString *snew = [NSString stringWithFormat:@"%d",so];
            [[NSUserDefaults standardUserDefaults] setObject:snew  forKey:@"numberOfUnreadMails"];
        }
        
    }

- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
  
}
- (void)goback:(id) sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.topBar setBackgroundImage:[UIImage imageNamed:@"topBlueBarWithText1.png"] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
    
    // Do any additional setup after loading the view from its nib.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
//    cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
    
    NSString *string = [tableData objectAtIndex:indexPath.row];
    CGSize stringSize = [string sizeWithFont:[UIFont boldSystemFontOfSize:15] constrainedToSize:CGSizeMake(320, 9999) lineBreakMode:UILineBreakModeWordWrap];
    
    UITextView *textV=[[UITextView alloc] initWithFrame:CGRectMake(5, 5, 290, stringSize.height+10)];
    textV.font = [UIFont systemFontOfSize:15.0];
    textV.text=string;
    textV.textColor=[UIColor blackColor];
    textV.editable=NO;
    textV.scrollEnabled = NO;
    [cell.contentView addSubview:textV];
    [textV release];

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    
    NSString *string = [tableData objectAtIndex:indexPath.row];
    CGSize stringSize = [string sizeWithFont:[UIFont boldSystemFontOfSize:15]
                          constrainedToSize:CGSizeMake(320, 9999)
                              lineBreakMode:UILineBreakModeWordWrap];
    
    return stringSize.height+25;
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

