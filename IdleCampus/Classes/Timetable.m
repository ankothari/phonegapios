//
//  Timetable.m
//  IdleCampus
//
//  Created by Ankur Kothari on 6/7/13.
//
//

#import "Timetable.h"

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) //1


@implementation Timetable
@synthesize timetableDictionary;

-(void)notification:(NSString *)url{
       NSURL *group_name_url = [NSURL URLWithString:url];        // Create the request.
    
    dispatch_async(kBgQueue, ^{
        
        
        
        NSData* data = [NSData dataWithContentsOfURL:
                        group_name_url];
        
       
        [self performSelectorOnMainThread:@selector(gotTimetable:)
                               withObject:data waitUntilDone:YES];
    });

}

- (void)gotTimetable:(NSData *)responseData {
    //parse out the json data
    NSError* error;
    
    timetableDictionary = [[NSMutableDictionary alloc]init];
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData //1
                          
                          options:kNilOptions
                          error:&error];
    
    
    
    
    //NSMutableDictionary myDictionary = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *myDictionary = [[NSMutableDictionary alloc] init];
    
    
    //    NSNumber *code = [NSNumber numberWithInt:2];
    //    [myDictionary setObject:code  forKey:@"code"];
    //    [myDictionary setObject:group_code  forKey:@"group_code"];
    //    [myDictionary setObject:group_name  forKey:@"name"];
    
    
    
    
//    [[NSUserDefaults standardDefaults] setObject:json forKey:@"timetable"];
    
    
    NSDictionary* timetable = [json objectForKey:@"timetable"]; //2
    NSArray* entries = [timetable objectForKey:@"entries"]; //2
    
    
    
    
    if ([entries count] == 0) {
        //                        $("#timetablestatus").text("Not Available");
    }
    NSArray * weekdays = [timetable objectForKey:@"weekdays"];
  [[NSUserDefaults standardUserDefaults] setObject:weekdays forKey:@"weekdays"];
    
    
    
    
    NSMutableArray *mondayList = [[NSMutableArray alloc]init];
    NSMutableArray * tuesdayList = [[NSMutableArray alloc]init];
    NSMutableArray * wednesdayList = [[NSMutableArray alloc]init];
    NSMutableArray * thursdayList = [[NSMutableArray alloc]init];
    NSMutableArray * fridayList = [[NSMutableArray alloc]init];
    NSMutableArray *saturdayList = [[NSMutableArray alloc]init];
    NSMutableArray * sundayList = [[NSMutableArray alloc]init];
    NSMutableArray * weekdaysList = [[NSMutableArray alloc]init];
    
    for(int len = 0;len < [weekdays count];len++){
        [weekdaysList addObject:weekdays[len]];
    }
    
    
    for (int a = 0; a < [entries count]; a++) {
        
        for (int b = 0; b < [entries[a] count]; b++) {
            
            for (int c = 0; c < [entries[a][b] count]; c++) {
                
                NSDictionary* obj = entries[a][b][c];
                NSLog(@"%@",obj);
                NSString *str = @"";
                //                                NSString *timeToDisplay = [self getDisplayTime(entries.getJSONArray(a))];
                
                //                                str = [str stringByAppendingString:timeToDisplay];
                str = [str  stringByAppendingString:@"\n"];
               NSArray* keys = [obj allKeys];
    
    for(NSString* key in keys) {
      
            NSString* value = [[obj objectForKey:key] description];
         
            if (value != nil && ![value isEqualToString:@"null"]) {
                if(![key isEqualToString:@"from_hours"] && ![key isEqualToString:@"from_minutes"] && ![key isEqualToString:@"to_minutes"] && ![key isEqualToString:@"to_hours"] && ![key isEqualToString:@"weekday"]){
                    
                    str = [str stringByAppendingFormat:@"%@:%@\n",key,value];
                     
                }
            }
    }
        
                NSString *weekday = [obj objectForKey:@"weekday"];
                if([weekday isEqualToString:@"Monday"]){
                    [mondayList addObject:str];
                }
                if([weekday isEqualToString:@"Tuesday"]){
                    [tuesdayList addObject:str];
                }
                if([weekday isEqualToString:@"Wednesday"]){
                    [wednesdayList addObject:str];
                }
                if([weekday isEqualToString:@"Thursday"]){
                    [thursdayList addObject:str];
                }
                if([weekday isEqualToString:@"Friday"]){
                    [fridayList addObject:str];
                }
                if([weekday isEqualToString:@"Saturday"]){
                    [saturdayList addObject:str];
                }
                if([weekday isEqualToString:@"Sunday"]){
                    [sundayList addObject:str];
                }
               
                            
       
    
                
            
                
                            
            }
        }
    }
    
    [timetableDictionary setObject:mondayList forKey:@"Monday"];
    [timetableDictionary setObject:tuesdayList forKey:@"Tuesday"];
    [timetableDictionary setObject:wednesdayList forKey:@"Wednesday"];
    [timetableDictionary setObject:thursdayList forKey:@"Thursday"];
    [timetableDictionary setObject:fridayList forKey:@"Friday"];
    [timetableDictionary setObject:saturdayList forKey:@"Saturday"];
    [timetableDictionary setObject:sundayList forKey:@"Sunday"];
    [timetableDictionary setObject:weekdaysList forKey:@"weekdays"];
    [[NSUserDefaults standardUserDefaults] setObject:timetableDictionary forKey:@"timetable"];
    
                             
                
                
            }
    

@end
