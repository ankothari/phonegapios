//
//  BingoViewController.h
//  IdleCampus
//
//  Created by Ankur Kothari on 15/7/13.
//
//

#import <UIKit/UIKit.h>
@class Bingo;
@interface BingoViewController : UIViewController{
    NSMutableArray *numbers;
    NSMutableArray *lines;
    NSMutableArray *points;
    BOOL myTurn;
    int countX,countY;
    IBOutlet Bingo *bingo;
    NSString *userPlayingBingoWith;
    IBOutlet UILabel *friendName;
    IBOutlet UILabel *whoseTurn;
}
@property (nonatomic,retain)IBOutlet UILabel *friendName;
@property (nonatomic,retain) IBOutlet UILabel *whoseTurn;
@property (nonatomic,retain)  NSString *userPlayingBingoWith;
@property (nonatomic,retain) IBOutlet UIView *bingo;
@property (nonatomic)int countX,countY;
@property (nonatomic, assign, getter=isMyTurn) BOOL myTurn;
@property (nonatomic,retain) NSMutableArray *numbers;
@property (nonatomic,retain) NSMutableArray *lines;
@property (nonatomic,retain) NSMutableArray *points;
@end
