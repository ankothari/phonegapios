//
//  RegisterViewController.m
//  IdleCampus
//
//  Created by Ankur Kothari on 5/7/13.
//
//

#import "RegisterViewController.h"
#import "JoinGroupViewController.h"

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) //1
@implementation RegisterViewController
@synthesize username,password,email,activityView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
   
    
    
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed: @"topBlueBarWithText1.png"] forBarMetrics:UIBarMetricsDefault];
    
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [leftButton setUserInteractionEnabled:NO];
    [leftButton setImage:[UIImage imageNamed:@"backButtonWithText.png"] forState:UIControlStateNormal];
    leftButton.frame = CGRectMake(0, 0, 44, 30);
    [leftButton addTarget:self action:@selector(goback:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    [self.navigationItem setLeftBarButtonItem:item animated:NO];
    [leftButton release];
     
}

- (void)goback:(id) sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)checkName:(NSData *)responseData {
    
    NSString *sno = [NSString stringWithUTF8String:[responseData bytes]];
   
    int noInt = [sno intValue];
    
    if(noInt > 0){
//        [activityView stopAnimating];
        [hud hide:YES];
        [[[[UIAlertView alloc]
           initWithTitle:@"Error"
           message:@"Username already in use"
           delegate:self
           cancelButtonTitle:@"Ok"
           otherButtonTitles: nil] autorelease] show];
    }
    else{
        NSString* urlString = [NSString stringWithFormat:@"http://idlecampus.com/users/checkEmail?email=%@",email.text];
        
        NSURL *check_name_url = [NSURL URLWithString:urlString];        // Create the request.
        
        dispatch_async(kBgQueue, ^{
            
            
            
            NSData* data = [NSData dataWithContentsOfURL:
                            check_name_url];
            
            
            [self performSelectorOnMainThread:@selector(checkEmail:)
                                   withObject:data waitUntilDone:YES];
        });
        
  
    }
   
    
       
    
}

- (void)checkEmail:(NSData *)responseData{
    NSString *sno = [NSString stringWithUTF8String:[responseData bytes]];
    
    int noInt = [sno intValue];
    
    if(noInt > 0){
//        [activityView stopAnimating];
        [hud hide:YES];
        [[[[UIAlertView alloc]
           initWithTitle:@"Error"
           message:@"Email already in use"
           delegate:self
           cancelButtonTitle:@"Ok"
           otherButtonTitles: nil] autorelease] show];
    }
    else{
        
//        [activityView stopAnimating];
        [hud hide:YES];
        JoinGroupViewController *joinGroupViewController = [[JoinGroupViewController alloc]init];
        joinGroupViewController.username = username.text;
        joinGroupViewController.password = password.text;
        joinGroupViewController.email = email.text;
        
        [self.navigationController pushViewController:joinGroupViewController animated:YES];
    }
    

    
    
    
}




-(IBAction)registerUser:(id)sender{
    
    if([username.text isEqualToString:@""]){
    
    [[[[UIAlertView alloc]
       initWithTitle:@"Error"
       message:@"Please enter a username"
       delegate:self
       cancelButtonTitle:@"Ok"
       otherButtonTitles: nil] autorelease] show];
}
   else if([password.text isEqualToString:@""]){
        
        [[[[UIAlertView alloc]
           initWithTitle:@"Error"
           message:@"Please enter a password"
           delegate:self
           cancelButtonTitle:@"Ok"
           otherButtonTitles: nil] autorelease] show];
    }
   else if([email.text isEqualToString:@""]){
        
        [[[[UIAlertView alloc]
           initWithTitle:@"Error"
           message:@"Please enter an email"
           delegate:self
           cancelButtonTitle:@"Ok"
           otherButtonTitles: nil] autorelease] show];
    }
    

   else{
    
       hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//       hud.mode = MBProgressHUDModeAnnularDeterminate;
       hud.labelText = @"Loading";
//      activityView=[[UIActivityIndicatorView alloc]     initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//       
//       activityView.center=self.view.center;
//       
//       [activityView startAnimating];
//       
//       [self.view addSubview:activityView];
   NSString* urlString = [NSString stringWithFormat:@"http://idlecampus.com/users/checkName?name=%@",username.text];
    
    NSURL *check_name_url = [NSURL URLWithString:urlString];        // Create the request.
    
    dispatch_async(kBgQueue, ^{
        
        
        
        NSData* data = [NSData dataWithContentsOfURL:
                        check_name_url];
        
        
        [self performSelectorOnMainThread:@selector(checkName:)
                               withObject:data waitUntilDone:YES];
    });

   }
  
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)hideKeyboard:(id)sender {
    [sender resignFirstResponder];
    //    [self done:sender];
}

@end
