/*
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 
 */

//
//  AppDelegate.m
//  IdleCampus
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright ___ORGANIZATIONNAME___ ___YEAR___. All rights reserved.
//

#import "AppDelegate.h"
#include "headers/jid.h"
#include "headers/client.h"
#include "headers/messagehandler.h"

#import "HomeViewController.h"
#import "SplashScreenViewController.h"
@implementation AppDelegate

@synthesize window, viewController;

- (id)init
{
    /** If you need to do any extra app-specific initialization, you can do it here
     *  -jm
     **/
    NSHTTPCookieStorage* cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];

    [cookieStorage setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];

    self = [super init];
    return self;
}

#pragma UIApplicationDelegate implementation

/**
 * This is main kick off after the app inits, the views and Settings are setup here. (preferred - iOS4 and up)
 */

- (void)applicationDidEnterBackground:(UIApplication *)application {
    XMPPNative *xmpp = [XMPPNative sharedManager];
    [xmpp disconnect];
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    
    XMPPNative *xmpp = [XMPPNative sharedManager];
    xmpp.status = @"resume";
    [xmpp gotRoster];
    NSString *sUsername = [[NSUserDefaults standardUserDefaults] stringForKey:kXMPPmyJID];
    
    NSString *sPassword = [[NSUserDefaults standardUserDefaults] stringForKey:kXMPPmyPassword];
    
    if([sUsername length]==0 && [sPassword length] == 0){
        
    }else{
        [xmpp login:sUsername password:sPassword ];
    }

}

- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
    
    [[UIApplication sharedApplication]
     registerForRemoteNotificationTypes: (UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge |
                                          UIRemoteNotificationTypeSound)];
    
    
    UILocalNotification *localNotification = [launchOptions objectForKey:
                                              UIApplicationLaunchOptionsLocalNotificationKey];
    if (localNotification) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Inside application:didFinishLaunchingWithOptions:" message:localNotification.alertBody
                              delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [alert release];
    
    }

   
    NSURL* url = [launchOptions objectForKey:UIApplicationLaunchOptionsURLKey];
    NSString* invokeString = nil;

   

    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    self.window = [[[UIWindow alloc] initWithFrame:screenBounds] autorelease];
    self.window.autoresizesSubviews = YES;

    self.viewController = [[[SplashScreenViewController alloc] init] autorelease];
//    self.viewController.useSplashScreen = YES;
//    self.viewController.wwwFolderName = @"www";
//    self.viewController.startPage = @"index.html";
//    self.viewController.invokeString = invokeString;

    // NOTE: To control the view's frame size, override [self.viewController viewWillAppear:] in your view controller.

   
    UIDeviceOrientation curDevOrientation = [[UIDevice currentDevice] orientation];

    if (UIDeviceOrientationUnknown == curDevOrientation) {
        // UIDevice isn't firing orientation notifications yet… go look at the status bar
        curDevOrientation = (UIDeviceOrientation)[[UIApplication sharedApplication] statusBarOrientation];
    }

    if (UIDeviceOrientationIsValidInterfaceOrientation(curDevOrientation)) {
//        if ([self.viewController supportsOrientation:curDevOrientation]) {
//            forceStartupRotation = NO;
//        }
    }

//    if (forceStartupRotation) {
//        UIInterfaceOrientation newOrient;
//        if ([self.viewController supportsOrientation:UIInterfaceOrientationPortrait]) {
//            newOrient = UIInterfaceOrientationPortrait;
//        } else if ([self.viewController supportsOrientation:UIInterfaceOrientationLandscapeLeft]) {
//            newOrient = UIInterfaceOrientationLandscapeLeft;
//        } else if ([self.viewController supportsOrientation:UIInterfaceOrientationLandscapeRight]) {
//            newOrient = UIInterfaceOrientationLandscapeRight;
//        } else {
//            newOrient = UIInterfaceOrientationPortraitUpsideDown;
//        }
//
//        NSLog(@"AppDelegate forcing status bar to: %d from: %d", newOrient, curDevOrientation);
//        [[UIApplication sharedApplication] setStatusBarOrientation:newOrient];
//    }
    
//    UIImage * backButtonImage = [UIImage imageNamed: @"backButtonWithText1"];
//    backButtonImage = [backButtonImage stretchableImageWithLeftCapWidth: 15.0 topCapHeight: 30.0];
//    [[UIBarButtonItem appearance] setBackButtonBackgroundImage: backButtonImage forState: UIControlStateNormal barMetrics: UIBarMetricsDefault];
//
    
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:self.viewController];
    
    [self.window setRootViewController:navigationController];
//    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];

    return YES;
}



// this happens while we are running ( in the background, or from within our own app )
// only valid if IdleCampus-Info.plist specifies a protocol to handle
//- (BOOL)application:(UIApplication*)application handleOpenURL:(NSURL*)url
//{
//    
//    if (!url) {
//        return NO;
//    }
//
//    // calls into javascript global function 'handleOpenURL'
//    NSString* jsString = [NSString stringWithFormat:@"handleOpenURL(\"%@\");", url];
//    [self.viewController.webView stringByEvaluatingJavaScriptFromString:jsString];
//
//    // all plugins will get the notification, and their handlers will be called
//    [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:CDVPluginHandleOpenURLNotification object:url]];
//
//    return YES;
//}

//- (NSUInteger)application:(UIApplication*)application supportedInterfaceOrientationsForWindow:(UIWindow*)window
//{
//    // iPhone doesn't support upside down by default, while the iPad does.  Override to allow all orientations always, and let the root view controller decide what's allowed (the supported orientations mask gets intersected).
//    NSUInteger supportedInterfaceOrientations = (1 << UIInterfaceOrientationPortrait) | (1 << UIInterfaceOrientationLandscapeLeft) | (1 << UIInterfaceOrientationLandscapeRight) | (1 << UIInterfaceOrientationPortraitUpsideDown);
//
//    return supportedInterfaceOrientations;
//}

// Delegation methods
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)devToken {
    NSUserDefaults  *defaults = [NSUserDefaults standardUserDefaults];
    NSString        *uuidString    = [defaults objectForKey: @"device_identifier"];
    
    if (!uuidString)
    {
        uuidString = (NSString *) CFUUIDCreateString (NULL, CFUUIDCreate(NULL));
        [defaults setObject: uuidString forKey: @"device_identifier"];
        [defaults synchronize]; // handle error
    }
    
    NSString *deviceToken = [[devToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    deviceToken = [deviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
   // self.registered = YES;
   
    
    [self sendProviderDeviceToken:deviceToken device_identifier:uuidString]; // custom method
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    NSLog(@"Error in registration. Error: %@", err);
}

//registration_Id,device_identifier,device_type

-(void)sendProviderDeviceToken:(NSString *)registration_Id device_identifier:(NSString *)device_identifier {
    NSURL *aUrl = [NSURL URLWithString:@"http://developer.idlecampus.com/devices"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:aUrl
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    
    
    [request setHTTPMethod:@"POST"];
    NSString *postString = [NSString stringWithFormat:@"registration_id=%@&device_identifier=%@&device_type=IOS",registration_Id,device_identifier];
   [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLConnection *connection= [[NSURLConnection alloc] initWithRequest:request
                                                                 delegate:self];
    
}
- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo
{
    application.applicationIconBadgeNumber = 0;
    NSLog(@"%@",userInfo);
//    NSString *url = [userInfo objectForKey:@"url"];
//    NSString *app = [userInfo objectForKey:@"app"];

    
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait) ;
}

@end
