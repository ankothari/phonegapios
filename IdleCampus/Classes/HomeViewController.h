//
//  HomeViewController.h
//  IdleCampus
//
//  Created by Ankur Kothari on 5/7/13.
//
//

#import <UIKit/UIKit.h>
#import "XMPPNative.h"
#import "Timetable.h"
#import "MBProgressHUD.h"
extern NSString *const kXMPPmyJID;
extern NSString *const kXMPPmyPassword;
@interface HomeViewController : UIViewController{
    XMPPNative *xmpp;
    MBProgressHUD *hud;
}
@property (retain, nonatomic)MBProgressHUD *hud;

@property (retain,nonatomic) XMPPNative *xmpp;
@property (retain, nonatomic) IBOutlet UIButton *signinButton;
@property (retain, nonatomic) IBOutlet UIButton *registerButton;
@property (retain, nonatomic) IBOutlet UIButton *forgotPassordButton;
@property (retain, nonatomic) IBOutlet UITextField *username;
@property (retain, nonatomic) IBOutlet UITextField *password;
- (IBAction)hideKeyboard:(id)sender;
-(IBAction)loginUser:(id)sender;

-(IBAction)registerUser:(id)sender;

-(IBAction)userForgotPassword:(id)sender;

@end
