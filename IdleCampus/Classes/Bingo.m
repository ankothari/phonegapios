//
//  Bingo.m
//  IdleCampus
//
//  Created by Ankur Kothari on 15/7/13.
//
//

#import "Bingo.h"
#import "BingoKey.h"
#import "XMPPNative.h"  

#define DegreesToRadians(x) ((x) * M_PI / 180.0)

@implementation Bingo
@synthesize myTurn, turn, userPlayingBingoWith, status, bingoLines,newGamebutton,myScore,yourScore;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {

    //find if there is any game saved corresponding to the user. If yes then load that else create a new game.

//    and when u click on a friend, if any old game is saved then it opens or else there is a new game and u r asked to start a new game. else the old game is restored with ur markings, score and status.
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(context, 2.0);
    
    CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);
    
    CGContextSetTextMatrix(context, CGAffineTransformMake(1.0, 0.0, 0.0, -1.0, 0.0, 0.0));
    CGContextSelectFont(context, "Arial", 15, kCGEncodingMacRoman);


    NSData *gameStatusForUser = [[NSUserDefaults standardUserDefaults] dataForKey:userPlayingBingoWith];
    NSDictionary *myDictionary = (NSDictionary *) [NSKeyedUnarchiver unarchiveObjectWithData:gameStatusForUser];
   
    

    if (gameStatusForUser) {
        myScore.text = [myDictionary objectForKey:@"myScore"];
        bingoLines = [[NSMutableArray alloc] initWithArray:[myDictionary objectForKey:@"lines"]];
        NSNumber *myTurnInt = [myDictionary objectForKey:@"turn"];
        myTurn = [myTurnInt boolValue];
        NSRange range = [userPlayingBingoWith rangeOfString:@"@"];
        int indexOfAt = 0;
        if (range.length > 0) {
            indexOfAt = range.location;
        } else {
            indexOfAt = -1;
        }
        NSString *singleUser = [userPlayingBingoWith substringWithRange:NSMakeRange(0, indexOfAt)];
        
        turn.text = [NSString stringWithFormat:@"%@'s turn", singleUser];

        if (myTurn) {
             turn.text = [NSString stringWithFormat:@"%@ turn", @"Your"];
        }
        else{
             turn.text = [NSString stringWithFormat:@"%@'s turn", singleUser];
        }
       
        for (int i = 0; i < 25; i++) {
            BingoKey *key = [bingoLines objectAtIndex:i];
            lines[key.xPos][key.yPos] = key;
            int x = key.x;
            int y = key.y;
            points[x - 20][y - 20] = key.key;
            NSLog(@"%d",key.key);
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotMessage:) name:@"receivedBingo" object:nil];


            numbers = [[NSMutableArray alloc] init];


            countX = 0, countY = 0;
           

            int z = key.key;


            NSNumber *zWrapped = [NSNumber numberWithInt:z];
            [numbers addObject:zWrapped];


            NSString *str;


            str = [NSString stringWithFormat:@"%d", z]; //%d or %i both is ok.
            const char *text1 = [str UTF8String];
            CGContextShowTextAtPoint(context, x, y, (const char *) text1, [str length]);
            if(key.pressed){
                [self draw:x-20 y:y-20];
            }
            

            countY++;

            if (countY == 5) {
                countX++;
                countY = 0;


            }

        }
        for (int s = 0; s < 5; s++) {
            for (int g = 0; g < 5; g++) {
                NSLog(@"%d %d %d %d", s, g, lines[s][g].key, lines[s][g].pressed);
            }
        }


    }
    else {
        myScore.text = @"0";
        yourScore.text = @"0";
        bingoLines = [[NSMutableArray alloc] init];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotMessage:) name:@"receivedBingo" object:nil];


        numbers = [[NSMutableArray alloc] init];


        countX = 0, countY = 0;
        myTurn = true;
        for (int x = 60; x <= 220; x += 40) {


            for (int y = 60; y <= 220; y += 40) {
                int z = (arc4random() % 25) + 1;
                NSNumber *num = [NSNumber numberWithInteger:z];
                NSInteger Aindex = [numbers indexOfObject:num];

                if (z == 0 || NSNotFound != Aindex) {

                    
                    y -= 40;
                } else {
                    NSNumber *zWrapped = [NSNumber numberWithInt:z];
                    [numbers addObject:zWrapped];


                    NSString *str;


                    str = [NSString stringWithFormat:@"%d", z]; //%d or %i both is ok.
                    const char *text1 = [str UTF8String];
                    CGContextShowTextAtPoint(context, x, y, (const char *) text1, [str length]);
                    BingoKey *bingoKey = [[BingoKey alloc] init];
                    bingoKey.key = z;
                    bingoKey.pressed = false;
                    bingoKey.xPos = countX;
                    bingoKey.yPos = countY;
                    bingoKey.x = x;
                    bingoKey.y = y;
                    [bingoLines addObject:bingoKey];
                    lines[countX][countY] = bingoKey;

                    countY++;

                    if (countY == 5) {
                        countX++;
                        countY = 0;


                    }

                    points[x - 20][y - 20] = z;
                }
            }
        }
        for (int s = 0; s < 5; s++) {
            for (int g = 0; g < 5; g++) {
                NSLog(@"%d %d %d %d", s, g, lines[s][g].key, lines[s][g].pressed);
            }
        }


        NSMutableDictionary *gameStateForUser = [[NSMutableDictionary alloc] init];
        [gameStateForUser setObject:bingoLines forKey:@"lines"];
        [gameStateForUser setObject:[[NSNumber alloc]initWithBool:myTurn] forKey:@"turn"];

//    for (int a = 5; a < 10; a++) {
//       
//
//        for (int b = 0; b < 5; b++) {
//
//            lines[a][b] = lines[b][a - 5];
//        }
//    }
//   
//    for (int a = 0; a < 5; a++) {
//        lines[10][a] = lines[a][a];
//
//
//    }
//   
//
//    int l = 4;
//    for (int a = 0; a < 5; a++) {
//
//        lines[11][a] = lines[a][l--];
//
//
//    }
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        NSData *yourArrayAsData = [NSKeyedArchiver archivedDataWithRootObject:gameStateForUser];
        [ud setObject:yourArrayAsData forKey:userPlayingBingoWith];
    }


        CGContextMoveToPoint(context, 40, 40);
        CGContextAddLineToPoint(context, 240, 40);
        CGContextMoveToPoint(context, 40, 80);
        CGContextAddLineToPoint(context, 240, 80);
        CGContextMoveToPoint(context, 40, 120);
        CGContextAddLineToPoint(context, 240, 120);
        CGContextMoveToPoint(context, 40, 160);
        CGContextAddLineToPoint(context, 240, 160);
        CGContextMoveToPoint(context, 40, 200);
        CGContextAddLineToPoint(context, 240, 200);
        CGContextMoveToPoint(context, 40, 240);
        CGContextAddLineToPoint(context, 240, 240);

        CGContextMoveToPoint(context, 40, 40);
        CGContextAddLineToPoint(context, 40, 240);
        CGContextMoveToPoint(context, 80, 40);
        CGContextAddLineToPoint(context, 80, 240);
        CGContextMoveToPoint(context, 120, 40);
        CGContextAddLineToPoint(context, 120, 240);
        CGContextMoveToPoint(context, 160, 40);
        CGContextAddLineToPoint(context, 160, 240);
        CGContextMoveToPoint(context, 200, 40);
        CGContextAddLineToPoint(context, 200, 240);
        CGContextMoveToPoint(context, 240, 40);
        CGContextAddLineToPoint(context, 240, 240);


        CGContextShowTextAtPoint(context, 60, 280, "B", 1);
        CGContextShowTextAtPoint(context, 100, 280, "I", 1);
        CGContextShowTextAtPoint(context, 140, 280, "N", 1);
        CGContextShowTextAtPoint(context, 180, 280, "G", 1);
        CGContextShowTextAtPoint(context, 220, 280, "O", 1);


        CGContextStrokePath(context);


}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];

    // gets the coordinats of the touch with respect to the specified view.
    CGPoint touchPoint = [touch locationInView:self];


    if (myTurn) {

//
        float offsetX = touchPoint.x;
        float offsetY = touchPoint.y;
//
        if (offsetX > 240 || offsetY > 240)
            return;
        for (int x = floor(offsetX); x >= 40; x--) {
            bool found = false;
//
            for (int y = floor(offsetY); y >= 40; y--) {
                //int i;

                if (points[x][y] != 0) {
                    NSString *str;
                    NSMutableString *myString = [NSMutableString string];

                    str = [NSString stringWithFormat:@"%d", points[x][y]]; //%d or %i both is ok.
                    NSLog(@"%d %d %d", x, y, points[x][y]);
//
                    myTurn = false;
                    //game status true means game continue false then stop game
                    bool gameStatus = [self draw:x y:y];
                    NSRange range = [userPlayingBingoWith rangeOfString:@"@"];
                    int indexOfAt = 0;
                    if (range.length > 0) {
                        indexOfAt = range.location;
                    } else {
                        indexOfAt = -1;
                    }
                    NSString *singleUser = [userPlayingBingoWith substringWithRange:NSMakeRange(0, indexOfAt)];

                    turn.text = [NSString stringWithFormat:@"%@'s turn", singleUser];
                    if (gameStatus) {
                        [self sendData:str from_user:@"a@a.com" data:[NSString stringWithFormat:@"%d", points[x][y]]];
                    }

                    found = true;

//  
                    break;

                }

            }
            if (found)
                break;


        }
    }


}

- (void)sendData:(NSString *)toUser from_user:(NSString *)from_user data:(NSString *)data_to_send {
    NSLog(@"sending data...");


    XMPPNative *xmpp = [XMPPNative sharedManager];
    [xmpp sendBingo:data_to_send :userPlayingBingoWith];


}

- (void)draw1:(int)number {

    BOOL isfirst = true;
    first:
    for (int a1 = 0; a1 < 12; a1++) {
//
        for (int b1 = 0; b1 < 5; b1++) {
            BingoKey *v = lines[a1][b1];
//
//            //here i have to find the coordinate corresponing the value.
            if (v.key == number) {
                v.pressed = true;
                dispatch_sync(dispatch_get_main_queue(), ^{

                    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(40 + 40 * a1 - 8, 40 + 20 + 40 * b1, 40 * 1.414, 2)];
                    lineView.transform = CGAffineTransformMakeRotation(DegreesToRadians(45));
                    lineView.backgroundColor = [UIColor blackColor];
                    [self addSubview:lineView];
                    [lineView release];
                });




//                context.lineTo(80+40*a1, 80+40*b1);
//

//                // for every button clicked set the value of it to false...and then check how many lines have been cut total. if number of liens cut is 5 then BINGO.
                int lines_true = 0;
                for (int a = 0; a < 12; a++) {
                    int count_of_true = 0;
                    for (int b = 0; b < 5; b++) {
                        BingoKey *v1 = lines[b][a];
                        if (v1.key == v.key) {
                            v1.pressed = true;
                            int index = [bingoLines indexOfObject:v1];
                            [bingoLines replaceObjectAtIndex:index withObject:v1];
                        }
                        if (v1.pressed == true) {
                            count_of_true++;

                            if (count_of_true == 5) {
                                lines_true++;
                                NSLog(@"%d lines cut ho gayi", lines_true);
                            }

                        }

                        if (lines_true == 5 && isfirst) {
                            isfirst = false;

                            int myscore = 0;
                            myscore++;
                           
                        }
                    }
                }
                int count_of_true = 0;
                for (int equalPoints = 0; equalPoints < 5; equalPoints++) {


                    BingoKey *v1 = lines[equalPoints][equalPoints];
                    if (v1.key == v.key) {
                        v1.pressed = true;
                        int index = [bingoLines indexOfObject:v1];
                        [bingoLines replaceObjectAtIndex:index withObject:v1];
                    }
                    if (v1.pressed == true) {
                        count_of_true++;
                        //
                        if (count_of_true == 5) {
                            lines_true++;
                            NSLog(@"%d lines cut ho gayi", lines_true);
                        }

                    }
                    if (lines_true == 5 && isfirst) {
                        isfirst = false;
                        int myscore = 0;
                        myscore++;
                        [self youWin];
                        
                      

                    }
                }


                count_of_true = 0;
                for (int equalPoints = 0; equalPoints < 5; equalPoints++) {


                    BingoKey *v1 = lines[equalPoints][4 - equalPoints];
                    if (v1.key == v.key) {
                        v1.pressed = true;
                        int index = [bingoLines indexOfObject:v1];
                        [bingoLines replaceObjectAtIndex:index withObject:v1];
                    }
                    if (v1.pressed == true) {
                        count_of_true++;
                        //
                        if (count_of_true == 5) {
                            lines_true++;
                            NSLog(@"%d lines cut ho gayi", lines_true);
                        }

                    }
                    if (lines_true == 5 && isfirst) {
                        isfirst = false;
                        int myscore = 0;
                        myscore++;
                       [self youWin];

                    }
                }


                for (int a = 0; a < 12; a++) {
                    int count_of_true = 0;
                    for (int b = 0; b < 5; b++) {
                        BingoKey *v1 = lines[a][b];
                        if (v1.key == v.key) {
                            v1.pressed = true;
                            int index = [bingoLines indexOfObject:v1];
                            [bingoLines replaceObjectAtIndex:index withObject:v1];
                        }
                        if (v1.pressed == true) {
                            count_of_true++;
                            //
                            if (count_of_true == 5) {
                                lines_true++;
                                NSLog(@"%d lines cut ho gayi", lines_true);
                            }
                            //
                            //
                            //
                        }
                        //
                        if (lines_true == 5 && isfirst) {
                            isfirst = false;
                            //                            var myscore =  $('#myscore').html();
                            int myscore = 0;
                            myscore++;
                          [self youWin];
                        }
                    }
                }
                
                NSMutableDictionary *gameStateForUser = [[NSMutableDictionary alloc] init];
                [gameStateForUser setObject:bingoLines forKey:@"lines"];
                [gameStateForUser setObject:[[NSNumber alloc]initWithBool:myTurn] forKey:@"turn"];
                
                NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                NSData *yourArrayAsData = [NSKeyedArchiver archivedDataWithRootObject:gameStateForUser];
                [ud setObject:yourArrayAsData forKey:userPlayingBingoWith];

                int a = 0;
                int b = 0;


                if (lines_true == 1 && !one) {

                    one = true;
                    a = 50;
                    b = 260;
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(a - 15, b + 15, 40 * 1.414, 2)];
                        lineView.transform = CGAffineTransformMakeRotation(DegreesToRadians(45));
                        lineView.backgroundColor = [UIColor blackColor];
                        [self addSubview:lineView];
                        [lineView release];
                    });

                }
                if (lines_true == 2 && !two) {

                    two = true;
                    a = 90;
                    b = 260;
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(a - 15, b + 15, 40 * 1.414, 2)];
                        lineView.transform = CGAffineTransformMakeRotation(DegreesToRadians(45));
                        lineView.backgroundColor = [UIColor blackColor];
                        [self addSubview:lineView];
                        [lineView release];
                    });
                }
                if (lines_true == 3 && !three) {

                    a = 130;
                    b = 260;
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(a - 15, b + 15, 40 * 1.414, 2)];
                        lineView.transform = CGAffineTransformMakeRotation(DegreesToRadians(45));
                        lineView.backgroundColor = [UIColor blackColor];
                        [self addSubview:lineView];
                        [lineView release];
                    });
                }
                if (lines_true == 4 && !four) {
                    four = true;

                    a = 170;
                    b = 260;
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(a - 15, b + 15, 40 * 1.414, 2)];
                        lineView.transform = CGAffineTransformMakeRotation(DegreesToRadians(45));
                        lineView.backgroundColor = [UIColor blackColor];
                        [self addSubview:lineView];
                        [lineView release];
                    });
                }
                if (lines_true == 5 && !five) {
                    five = true;
                    a = 210;
                    b = 260;
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(a - 15, b + 15, 40 * 1.414, 2)];
                        lineView.transform = CGAffineTransformMakeRotation(DegreesToRadians(45));
                        lineView.backgroundColor = [UIColor blackColor];
                        [self addSubview:lineView];
                        [lineView release];
                    });
                }
                //
//
            }
        }
    }
}


- (BOOL)draw:(int)x y:(int)y {


    BOOL isfirst = true;

//    [self setNeedsDisplay];

    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(x - 8, y + 20, 40 * 1.414, 2)];
    lineView.backgroundColor = [UIColor blackColor];
    lineView.transform = CGAffineTransformMakeRotation(DegreesToRadians(45));
    [self addSubview:lineView];
    [lineView release];

//
//    // for every button clicked set the value of it to false...and then check how many lines have been cut total. if number of liens cut is 5 then BINGO.
    NSData *gameStatusForUser = [[NSUserDefaults standardUserDefaults] dataForKey:userPlayingBingoWith];

    int lines_true = 0;
    for (int a = 0; a < 12; a++) {
        int count_of_true = 0;
        for (int b = 0; b < 5; b++) {
            BingoKey *v = lines[b][a];
            if (v.key == points[x][y]&& v.key!=0) {
                if ( v.pressed == false ) {
                      v.pressed = true;
                }
                else if(v.pressed && gameStatusForUser){
                    int index = [bingoLines indexOfObject:v];
                    [bingoLines replaceObjectAtIndex:index withObject:v];
                }
              
              
                
            }
            if (v.pressed == true) {
                count_of_true++;
//
                if (count_of_true == 5) {
                    lines_true++;
                    NSLog(@"%d lines cut ho gayi", lines_true);
                }

            }
            if (lines_true == 5 && isfirst) {
                isfirst = false;
                int myscore = 0;
                myscore++;
[self youWin];

                return false;

            }
        }
//
    }
    int count_of_true = 0;
    for (int equalPoints = 0; equalPoints < 5; equalPoints++) {


        BingoKey *v = lines[equalPoints][equalPoints];
        if (v.key == points[x][y]&& v.key!=0) {
            if ( v.pressed == false ) {
                v.pressed = true;
            }
            else if(v.pressed && gameStatusForUser){
                int index = [bingoLines indexOfObject:v];
                [bingoLines replaceObjectAtIndex:index withObject:v];
            }
            
            
            
        }

        if (v.pressed == true) {
            count_of_true++;
            //
            if (count_of_true == 5) {
                lines_true++;
                NSLog(@"%d lines cut ho gayi", lines_true);
            }

        }
        if (lines_true == 5 && isfirst) {
            isfirst = false;
            int myscore = 0;
            myscore++;
           [self youWin];
            return false;


        }
    }

    for (int a = 0; a < 12; a++) {
        int count_of_true = 0;
        for (int b = 0; b < 5; b++) {
            BingoKey *v = lines[a][b];
            if (v.key == points[x][y]&& v.key!=0) {
                if ( v.pressed == false ) {
                    v.pressed = true;
                }
                else if(v.pressed && gameStatusForUser){
                    int index = [bingoLines indexOfObject:v];
                    [bingoLines replaceObjectAtIndex:index withObject:v];
                }
                
                
                
            }

            if (v.pressed == true) {
                count_of_true++;
                //
                if (count_of_true == 5) {
                    lines_true++;
                    NSLog(@"%d lines cut ho gayi", lines_true);
                }

            }
            if (lines_true == 5 && isfirst) {
                isfirst = false;
                int myscore = 0;
                myscore++;
                [self youWin];

                return false;
            }
        }
        //
    }

    count_of_true = 0;
    for (int equalPoints = 0; equalPoints < 5; equalPoints++) {


        BingoKey *v = lines[equalPoints][4 - equalPoints];
        if (v.key == points[x][y]&& v.key!=0) {
            if ( v.pressed == false ) {
                v.pressed = true;
            }
            else if(v.pressed && gameStatusForUser){
                int index = [bingoLines indexOfObject:v];
                [bingoLines replaceObjectAtIndex:index withObject:v];
            }
            
            
            
        }

        if (v.pressed == true) {
            count_of_true++;
            //
            if (count_of_true == 5) {
                lines_true++;
                NSLog(@"%d lines cut ho gayi", lines_true);
            }

        }
        if (lines_true == 5 && isfirst) {
            isfirst = false;
            int myscore = 0;
            myscore++;
           [self youWin];
            return false;


        }
    }
    
    NSMutableDictionary *gameStateForUser = [[NSMutableDictionary alloc] init];
    [gameStateForUser setObject:bingoLines forKey:@"lines"];
    [gameStateForUser setObject:[[NSNumber alloc]initWithBool:myTurn] forKey:@"turn"];
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSData *yourArrayAsData = [NSKeyedArchiver archivedDataWithRootObject:gameStateForUser];
    [ud setObject:yourArrayAsData forKey:userPlayingBingoWith];

    int a = 0;
    int b = 0;


    if (lines_true == 1 && !one) {

        one = true;
        a = 50;
        b = 260;
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(a - 15, b + 15, 40 * 1.414, 2)];
        lineView.transform = CGAffineTransformMakeRotation(DegreesToRadians(45));
        lineView.backgroundColor = [UIColor blackColor];
        [self addSubview:lineView];
        [lineView release];

    }
    if (lines_true == 2 && !two) {

        two = true;
        a = 90;
        b = 260;
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(a - 15, b + 15, 40 * 1.414, 2)];
        lineView.transform = CGAffineTransformMakeRotation(DegreesToRadians(45));
        lineView.backgroundColor = [UIColor blackColor];
        [self addSubview:lineView];
        [lineView release];
    }
    if (lines_true == 3 && !three) {

        a = 130;
        b = 260;
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(a - 15, b + 15, 40 * 1.414, 2)];
        lineView.transform = CGAffineTransformMakeRotation(DegreesToRadians(45));
        lineView.backgroundColor = [UIColor blackColor];
        [self addSubview:lineView];
        [lineView release];
    }
    if (lines_true == 4 && !four) {
        four = true;

        a = 170;
        b = 260;
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(a - 15, b + 15, 40 * 1.414, 2)];
        lineView.transform = CGAffineTransformMakeRotation(DegreesToRadians(45));
        lineView.backgroundColor = [UIColor blackColor];
        [self addSubview:lineView];
        [lineView release];
    }
    if (lines_true == 5 && !five) {
        five = true;
        a = 210;
        b = 260;
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(a - 15, b + 15, 40 * 1.414, 2)];
        lineView.transform = CGAffineTransformMakeRotation(DegreesToRadians(45));
        lineView.backgroundColor = [UIColor blackColor];
        [self addSubview:lineView];
        [lineView release];
    }
//
//
    return true;
}
//


- (void)refresh {
    [self setNeedsDisplay];
}

- (IBAction)refresh:(id)sender {
    [self refresh];
}

- (IBAction)newTournament:(id)sender {
    [self refresh];
    XMPPNative *xmpp = [XMPPNative sharedManager];
    [xmpp send:@"newBingoTournament" :userPlayingBingoWith];
}

- (IBAction)new:(id)sender {
    XMPPNative *xmpp = [XMPPNative sharedManager];

    myTurn = false;

    NSRange range = [userPlayingBingoWith rangeOfString:@"@"];
    int indexOfAt = 0;
    if (range.length > 0) {
        indexOfAt = range.location;
    } else {
        indexOfAt = -1;
    }
    NSString *singleUser = [userPlayingBingoWith substringWithRange:NSMakeRange(0, indexOfAt)];

    turn.text = [NSString stringWithFormat:@"%@'s turn", singleUser];
    

    [xmpp send:@"newBingogame" :userPlayingBingoWith];
    
    newGamebutton.hidden = true;
    
    [newGamebutton setHidden:YES];
    
    status.text = @"Waiting...";
}




- (void)gotMessage:(NSNotification *)notification {

//    get the message

    //if the message is lose then print u lose and put game status as u lose

    //if the message is new then start a new game or the user wants the game to be refreshed

// and also the user has finsihed his turn and now it is my turn to turn the tables or to play

//    call draw()

    NSDictionary *msgDict = [notification userInfo];

    NSString *msg = [msgDict objectForKey:@"mesage"];

    if ([msg isEqualToString:@"lose"]) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            status.text = @"You Lose";

            turn.text = @"";
            
            newGamebutton.hidden = true;
            
            [newGamebutton setHidden:YES];
            
            int yourscore = [yourScore.text intValue];
            yourscore++;
            yourScore.text = [NSString stringWithFormat:@"%d",yourscore];
            
           
        });
    }
    else if ([msg isEqualToString:@"new"]) {
        newGamebutton.hidden = true;
        
        [newGamebutton setHidden:YES];
    }
    else {
        myTurn = true;
        dispatch_sync(dispatch_get_main_queue(), ^{

            turn.text = [NSString stringWithFormat:@"%@ turn", @"Your"];
        });

        [self draw1:[msg intValue]];
    }


}



-(void)youWin{
    dispatch_sync(dispatch_get_main_queue(), ^{
        XMPPNative *xmpp = [XMPPNative sharedManager];
        status.text = @"You Win";
        int myscore = [myScore.text intValue];
        myscore++;
        myScore.text = [NSString stringWithFormat:@"%d",myscore];
       
        turn.text = @"";
        [xmpp sendBingo:@"lose" :userPlayingBingoWith];
    });
}
- (void)viewWillDisappear:(BOOL)animated {
    NSMutableDictionary *gameStateForUser = [[NSMutableDictionary alloc] init];
    [gameStateForUser setObject:bingoLines forKey:@"lines"];
    [gameStateForUser setObject:[[NSNumber alloc]initWithBool:myTurn] forKey:@"turn"];
    [gameStateForUser setObject:myScore.text forKey:@"myScore"];
    [gameStateForUser setObject:yourScore.text forKey:@"yourScore"];
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSData *yourArrayAsData = [NSKeyedArchiver archivedDataWithRootObject:gameStateForUser];
    [ud setObject:yourArrayAsData forKey:userPlayingBingoWith];
}

@end
