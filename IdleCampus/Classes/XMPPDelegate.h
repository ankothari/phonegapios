//
//  XMPPDelegate.h
//  IdleCampus
//
//  Created by Ankur Kothari on 6/7/13.
//
//

#import <Foundation/Foundation.h>

@protocol XMPPDelegate <NSObject>


-(void)sendRegistered:(int)b;
-(void)sendLogin1:(int)b;

@end
