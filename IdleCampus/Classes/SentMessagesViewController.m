//
//  NewMessageViewController.m
//  IdleCampus
//
//  Created by Ankur Kothari on 5/7/13.
//
//

#import "SentMessagesViewController.h"
#import "ComposeMessageViewController.h"
@interface SentMessagesViewController ()

@end

@implementation SentMessagesViewController
@synthesize groupHeader,topBar,tableData,group,tableView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    groupHeader.text = group;
    [super viewDidLoad];
    // Set the background image for PortraitMode
    
    self.navigationItem.hidesBackButton = YES;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed: @"topBlueBarWithText1.png"] forBarMetrics:UIBarMetricsDefault];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(newMessage)];;
    [item setTintColor:[UIColor blackColor]];
    self.navigationItem.rightBarButtonItem = item;
//initWithTitle:@"+ Contact"
//style:UIBarButtonItemStylePlain
   
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc]initWithTitle:@"Clear" style:UIBarButtonItemStyleBordered target:self action:@selector(clearMessages:)];;
    
    [item1 setTintColor:[UIColor blackColor]];
    self.navigationItem.leftBarButtonItem = item1;
  
   
    tableData = [[NSMutableArray alloc]initWithArray:[[NSUserDefaults standardUserDefaults]objectForKey:@"sentMessages"]];

}

-(void)backButtonItemToDismissModal{
    
    [self dismissModalViewControllerAnimated:YES];
    
}

-(void)newMessage{
    ComposeMessageViewController *composeViewController = [[ComposeMessageViewController alloc]initWithNibName:@"ComposeMessageViewController" bundle:nil];
    composeViewController.group = group;
    composeViewController.sentController = self;
    [self presentModalViewController:composeViewController animated:YES];
//    [self.navigationController presentModalViewController:composeViewController animated:YES];
     }

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    //    cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
    
    NSString *string = [tableData objectAtIndex:indexPath.row];
    CGSize stringSize = [string sizeWithFont:[UIFont boldSystemFontOfSize:15] constrainedToSize:CGSizeMake(320, 9999) lineBreakMode:UILineBreakModeWordWrap];
    
    UITextView *textV=[[UITextView alloc] initWithFrame:CGRectMake(5, 5, 290, stringSize.height+10)];
    textV.font = [UIFont systemFontOfSize:15.0];
    textV.text=string;
    textV.textColor=[UIColor blackColor];
    textV.editable=NO;
    textV.scrollEnabled = NO;
    [cell.contentView addSubview:textV];
    [textV release];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    
    NSString *string = [tableData objectAtIndex:indexPath.row];
    CGSize stringSize = [string sizeWithFont:[UIFont boldSystemFontOfSize:15]
                           constrainedToSize:CGSizeMake(320, 9999)
                               lineBreakMode:UILineBreakModeWordWrap];
    
    return stringSize.height+25;
    
}

-(IBAction)clearMessages:(id)sender{
    [self.tableData removeAllObjects];
    [self.tableView reloadData];
     [[NSUserDefaults standardUserDefaults]setObject:self.tableData forKey:@"sentMessages"];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
