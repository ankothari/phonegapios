//
//  BingoKey.m
//  IdleCampus
//
//  Created by Ankur Kothari on 15/7/13.
//
//

#import "BingoKey.h"

@implementation BingoKey
@synthesize pressed,key,myScore;

- (void)encodeWithCoder:(NSCoder *)enCoder
{
//    [super encodeWithCoder:enCoder];
    [enCoder encodeInt:self.xPos forKey:@"xPos"];
    [enCoder encodeInt:self.xPos forKey:@"myScore"];
    [enCoder encodeInt:self.yPos forKey:@"yPos"];
    [enCoder encodeInt:self.key forKey:@"key"];
    [enCoder encodeInt:self.x forKey:@"x"];
    [enCoder encodeInt:self.y forKey:@"y"];
    [enCoder encodeBool:pressed forKey:@"pressed"];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if(self=[super init])
    {
        self.xPos = [aDecoder decodeIntForKey:@"xPos"];
        self.myScore = [aDecoder decodeIntForKey:@"myScore"];
        self.x = [aDecoder decodeIntForKey:@"x"];
        self.y = [aDecoder decodeIntForKey:@"y"];
        self.xPos = [aDecoder decodeIntForKey:@"xPos"];
        self.yPos = [aDecoder decodeIntForKey:@"yPos"];
        self.key = [aDecoder decodeIntForKey:@"key"];
       self.pressed = [aDecoder decodeBoolForKey:@"pressed"];
    }
    return self;
}

@end
