//
//  FriendWithStatusViewController.m
//  IdleCampus
//
//  Created by Ankur Kothari on 18/7/13.
//
//

#import "FriendWithStatusViewController.h"

@interface FriendWithStatusViewController ()

@end

@implementation FriendWithStatusViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
