//
//  RegisterViewController.h
//  IdleCampus
//
//  Created by Ankur Kothari on 5/7/13.
//
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
@interface RegisterViewController : UIViewController{
    UIActivityIndicatorView *activityView;
    MBProgressHUD *hud;
}
@property (retain, nonatomic)MBProgressHUD *hud;
@property (retain, nonatomic) UIActivityIndicatorView *activityView;
@property (retain, nonatomic) IBOutlet UIButton *registerButton;
@property (retain, nonatomic) IBOutlet UITextField *username;
@property (retain, nonatomic) IBOutlet UITextField *password;
@property (retain, nonatomic) IBOutlet UITextField *email;
@property (retain, nonatomic) IBOutlet UIToolbar *topBar;
- (IBAction)hideKeyboard:(id)sender;
-(IBAction)registerUser:(id)sender;

@end
