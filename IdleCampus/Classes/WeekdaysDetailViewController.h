//
//  WeekdaysDetailViewController.h
//  IdleCampus
//
//  Created by Ankur Kothari on 5/7/13.
//
//

#import <UIKit/UIKit.h>

@interface WeekdaysDetailViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>{
    NSString *weekday;
    
    NSArray *tableData;
}
@property (retain, nonatomic) IBOutlet UIToolbar *topBar;
@property(retain,nonatomic) NSArray *tableData;
@property (nonatomic, retain) IBOutlet UILabel *weekdayHeader;
@property (nonatomic, retain) NSString *weekday;

@end
