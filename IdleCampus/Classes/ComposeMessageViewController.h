//
//  ComposeMessageViewController.h
//  IdleCampus
//
//  Created by Ankur Kothari on 8/7/13.
//
//

#import <UIKit/UIKit.h>
#import "SentMessagesViewController.h"
@interface ComposeMessageViewController : UIViewController<UITextViewDelegate>
@property (retain,nonatomic) IBOutlet UIToolbar *topBar;
@property (retain,nonatomic) IBOutlet UITextView *textView;
@property (retain,nonatomic) NSString *group;
@property (retain,nonatomic) SentMessagesViewController *sentController;
@end
