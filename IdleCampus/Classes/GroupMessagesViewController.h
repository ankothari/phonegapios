//
//  GroupMessagesViewController.h
//  IdleCampus
//
//  Created by Ankur Kothari on 5/7/13.
//
//

#import <UIKit/UIKit.h>

@interface GroupMessagesViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>{
    NSMutableArray *tableData;
    
}
@property (retain, nonatomic) IBOutlet UIToolbar *topBar;
@property(retain,nonatomic) NSMutableArray *tableData;
@property (retain, nonatomic) IBOutlet UITableView *tableView;
@end
