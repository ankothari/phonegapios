//
//  Timetable.h
//  IdleCampus
//
//  Created by Ankur Kothari on 6/7/13.
//
//

#import <Foundation/Foundation.h>

@interface Timetable : NSObject{
    NSMutableDictionary *timetableDictionary;
}

@property (nonatomic,retain) NSMutableDictionary *timetableDictionary;

-(void)notification:(NSString *)url;

@end
