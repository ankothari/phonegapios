//
//  WeekdaysViewController.m
//  IdleCampus
//
//  Created by Ankur Kothari on 5/7/13.
//
//

#import "WeekdaysViewController.h"
#import <AudioToolbox/AudioServices.h>
#import "WeekdaysDetailViewController.h"
@interface WeekdaysViewController ()

@end

@implementation WeekdaysViewController

@synthesize tableData;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotMessage:) name:@"receivedMessage" object:nil];
}

- (void)gotMessage:(NSNotification *) notification{
    
   
    
    
   
    NSString *s = [[NSUserDefaults standardUserDefaults]objectForKey:@"numberOfUnreadMails"];
    if([s length] == 0){
        int so = [s intValue];
        so++;
        NSString *snew = [NSString stringWithFormat:@"%d",so];
        [[NSUserDefaults standardUserDefaults] setObject:snew  forKey:@"numberOfUnreadMails"];
        
    }
    else{
        int so = [s intValue];
        so++;
        NSString *snew = [NSString stringWithFormat:@"%d",so];
        [[NSUserDefaults standardUserDefaults] setObject:snew  forKey:@"numberOfUnreadMails"];
    }
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [leftButton setUserInteractionEnabled:NO];
    [leftButton setImage:[UIImage imageNamed:@"backButtonWithText.png"] forState:UIControlStateNormal];
    leftButton.frame = CGRectMake(0, 0, 44, 30);
    [leftButton addTarget:self action:@selector(goback:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:leftButton];

    [self.navigationItem setLeftBarButtonItem:item animated:NO];
    [leftButton release];
    
    
    NSMutableArray *array = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"weekdays"]];
    
     [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed: @"topBlueBarWithText1.png"] forBarMetrics:UIBarMetricsDefault];
    
       
    tableData =  [[NSArray alloc] initWithArray:array];
}

- (void)goback:(id) sender 
{
    [self.navigationController popViewControllerAnimated:YES];
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *textSelected = [tableData objectAtIndex:indexPath.row];
   
        WeekdaysDetailViewController *weekdaysDetailViewController = [[WeekdaysDetailViewController alloc]
                                                                    initWithNibName:@"WeekdaysDetailViewController"
                                                                    bundle:nil];
    
         weekdaysDetailViewController.weekday = textSelected;
    
       
    [self.navigationController pushViewController:weekdaysDetailViewController animated:YES];
    
    [weekdaysDetailViewController release];
   }


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
