//
//  XMPPBridge.m
//  IdleCampus
//
//  Created by Ankur Kothari on 12/4/12.
//
//

#import "XMPPBridge.h"
#include "headers/jid.h"
#include "headers/client.h"
#include "headers/messagehandler.h"
#include "headers/clientbase.h"
#include "headers/connectiontcpclient.h"
#include <iostream.h>
#include "headers/gloox.h"
#include "headers/presence.h"
#include "MyClass.h"

@implementation XMPPBridge
@synthesize queue;
-(id)init{
    self = [super init];
    if (self) {
        commands = [[NSMutableDictionary alloc]initWithObjects:nil forKeys:nil];
    }
    return self;
}
-(bool)connectWithUser:(NSString *)user password:(NSString *)lpassword{
        
<<<<<<< HEAD
     user = [user stringByAppendingString:@"@chat.facebook.com"];//localhost//@chat.facebook.com
=======
     user = [user stringByAppendingString:@"@idlecampus.com"];//@ip addresstalk.google.comchat.facebook.com
>>>>>>> testing
   
    queue = dispatch_queue_create("idla", NULL);
    
    dispatch_async(queue, ^{
    
        handler =  new MyClass();
        
        handler->bridge = self;
        handler->doSomething([user UTF8String],[lpassword UTF8String]);
    });
    
    
    
  
}
-(void)sendRegistered:(int)b{
    NSNumber* b_int = [NSNumber numberWithInt:b];
    
    NSMutableArray *a = [[NSMutableArray alloc]initWithObjects:b_int, nil];
    
    NSArray *key =[[NSArray alloc]initWithObjects:@"registered", nil];
    NSDictionary *dic = [[NSDictionary alloc]initWithObjects:a forKeys:key];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"TestNotification"
     object:self userInfo:dic];
}

-(void)sendLogin1:(int)b{
    NSNumber* b_int = [NSNumber numberWithInt:b];
    
    NSMutableArray *a = [[NSMutableArray alloc]initWithObjects:b_int, nil];
    
    NSArray *key =[[NSArray alloc]initWithObjects:@"login1", nil];
    NSDictionary *dic = [[NSDictionary alloc]initWithObjects:a forKeys:key];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"TestNotification"
     object:self userInfo:dic];
}

-(void)sendLogin:(int)b{
    
    
    NSNumber* b_int = [NSNumber numberWithInt:b];
    
    NSMutableArray *a = [[NSMutableArray alloc]initWithObjects:b_int, nil];
    
    NSArray *key =[[NSArray alloc]initWithObjects:@"login", nil];
    NSDictionary *dic = [[NSDictionary alloc]initWithObjects:a forKeys:key];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"TestNotification"
     object:self userInfo:dic];
    
}



-(bool)registerUser:(NSString *)user password:(NSString *)lpassword group:(NSString *)lgroup email:(NSString *)lemail{
    
        
    dispatch_queue_t queue = dispatch_queue_create("idla1", NULL);
    
    dispatch_async(queue, ^{
        
        handler =  new MyClass();
        
        handler->bridge = self;
        handler->registerUser([user UTF8String],[lpassword UTF8String],[lgroup UTF8String],[lemail UTF8String]);
    });
    
    
    
        
}

-(void)disconnect{
     handler->disconnect();
}

-(void)callNotify:(NSString *)message fromUser:(NSString *)fromUser fromApp:(NSString *)fromApp{
    NSArray *a = [[NSArray alloc]initWithObjects:message,fromUser,fromApp, nil];
    NSArray *key =[[NSArray alloc]initWithObjects:@"message",@"fromUser",@"fromApp", nil];
    NSDictionary *dic = [[NSDictionary alloc]initWithObjects:a forKeys:key];
   
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"TestNotification"
     object:self userInfo:dic];
}
-(void)send:(NSString *)message to:(NSString *)to{
    handler->startChatWith([message UTF8String],[to  UTF8String]);
}
-(void)sendBingo:(NSString *)message to:(NSString *)to{
    handler->playBingo([message UTF8String],[to  UTF8String]);
}
-(void)auth_user:(NSString *)ack to:(NSString *)to{
    handler->auth_user([ack UTF8String], [to UTF8String]);
}
-(void)create_group:(NSString *)group{
    handler->createNode([group UTF8String]);
}
-(void)join_group:(NSString *)group{
    handler->subscribeNode([group UTF8String]);
}
-(void)sendRoster:(NSMutableDictionary *)dict{
    
    NSArray *a = [[NSArray alloc]initWithObjects:dict, nil];
    NSArray *key =[[NSArray alloc]initWithObjects:@"roster", nil];
    NSDictionary *dic = [[NSDictionary alloc]initWithObjects:a forKeys:key];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"TestNotification"
     object:self userInfo:dict];
}

-(void)sendNodeItems:(NSMutableDictionary *)items{
    
   
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setObject:items forKey:@"groups"];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"TestNotification"
     object:self userInfo:dic];
}
-(void)sendJoinedNodeItems:(NSMutableArray *)items{
    
    NSArray *a = [[NSArray alloc]initWithObjects:items, nil];
    NSArray *key =[[NSArray alloc]initWithObjects:@"joinednodes", nil];
    NSDictionary *dic = [[NSDictionary alloc]initWithObjects:a forKeys:key];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"TestNotification"
     object:self userInfo:dic];
}
-(void)subscribe:(NSString *)user{
    handler->subscribe([user UTF8String]);
}
-(void)getGroups{
    handler->getGroups();
}
-(void)getGroupsJoined{
    handler->getGroupsJoined();
}
-(void)publishMessage:(NSString *)message to:(NSString *)to{
    handler->publishNode([to UTF8String], [message UTF8String]);
    
}
-(void)gotSubscriptionRequest:(NSString *)user{
    NSArray *a = [[NSArray alloc]initWithObjects:user, nil];
    NSArray *key =[[NSArray alloc]initWithObjects:@"presence", nil];
    NSDictionary *dic = [[NSDictionary alloc]initWithObjects:a forKeys:key];
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"TestNotification"
     object:self userInfo:dic];
}


@end
