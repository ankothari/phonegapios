//
//  Bingo.h
//  IdleCampus
//
//  Created by Ankur Kothari on 15/7/13.
//
//

#import <UIKit/UIKit.h>
@class BingoKey;
@interface Bingo : UIView{
    NSMutableArray *numbers;
  
    BOOL myTurn;
    int countX,countY;
    IBOutlet UILabel *turn;
    IBOutlet UILabel *status;
    int points[240][240];
    BingoKey *lines[240][240];
     NSString *userPlayingBingoWith;
     BOOL one , two , three , four , five ;
    NSMutableArray *bingoLines;
    IBOutlet UIButton *newGamebutton;
    IBOutlet UILabel *myScore;
    IBOutlet UILabel *yourScore;
}
@property(nonatomic,retain)IBOutlet UILabel *myScore;
@property(nonatomic,retain)IBOutlet UILabel *yourScore;
@property(nonatomic,retain)  IBOutlet UIButton *newGamebutton;
@property(nonatomic,retain)  NSMutableArray *bingoLines;
@property(nonatomic,retain) NSString *userPlayingBingoWith;
@property(nonatomic,retain)  IBOutlet UILabel *turn,*status;
@property (nonatomic)int countX,countY;
@property (nonatomic, assign, getter=isMyTurn) BOOL myTurn;
@property (nonatomic,retain) NSMutableArray *numbers;


-(void)sendData:(NSString *)toUser from_user:(NSString *)from_user data:(NSString *)data_to_send;
-(void)sendBingo:(NSString *)message to:(NSString *)to;
-(void)refresh;
- (BOOL)draw:(int)x y:(int)y;
@end
