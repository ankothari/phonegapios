//
//  ForgotPasswordViewController.h
//  IdleCampus
//
//  Created by Ankur Kothari on 5/7/13.
//
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *forgotPasswordButton;
@property (strong, nonatomic) IBOutlet UITextField *email;
@property (retain, nonatomic) IBOutlet UIToolbar *topBar;
-(IBAction)forgotPassword:(id)sender;
- (IBAction)hideKeyboard:(id)sender;
@end
