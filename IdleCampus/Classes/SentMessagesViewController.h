//
//  NewMessageViewController.h
//  IdleCampus
//
//  Created by Ankur Kothari on 5/7/13.
//
//

#import <UIKit/UIKit.h>

@interface SentMessagesViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>{
    NSMutableArray *tableData;
    IBOutlet UITableView *tableView;
}
@property(retain,nonatomic) IBOutlet UIToolbar *topBar;
@property(retain,nonatomic) NSMutableArray *tableData;
@property (nonatomic, retain) IBOutlet UILabel *groupHeader;
@property (nonatomic, retain) NSString *group;
@property (nonatomic,retain) IBOutlet UITableView *tableView;
@end
