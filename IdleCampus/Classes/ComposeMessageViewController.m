//
//  ComposeMessageViewController.m
//  IdleCampus
//
//  Created by Ankur Kothari on 8/7/13.
//
//

#import "ComposeMessageViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "XMPPNative.h"
@interface ComposeMessageViewController ()

@end

@implementation ComposeMessageViewController
@synthesize topBar,textView,group,sentController;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
       [self.topBar setBackgroundImage:[UIImage imageNamed:@"topBlueBarWithText1.png"] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
      
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed: @"topBlueBarWithText1.png"] forBarMetrics:UIBarMetricsDefault];
    
    [textView.layer setBackgroundColor: [[UIColor whiteColor] CGColor]];
    [textView.layer setBorderColor: [[UIColor grayColor] CGColor]];
    [textView.layer setBorderWidth: 1.0];
    [textView.layer setCornerRadius:8.0f];
    [textView.layer setMasksToBounds:YES];
}



-(IBAction)backButtonItemToDismissModal:(id)sender{
    
    [self dismissModalViewControllerAnimated:YES];
    
}

- (void)textViewDidBeginEditing:(UITextView *)ltextView
{
    if ([ltextView.text isEqualToString:@"Enter your message here:"]) {
        ltextView.text = @"";
        ltextView.textColor = [UIColor blackColor]; //optional
    }
    [ltextView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)ltextView
{
    if ([ltextView.text isEqualToString:@""]) {
        ltextView.text = @"Enter your message here:";
        ltextView.textColor = [UIColor lightTextColor]; //optional
    }
    [ltextView resignFirstResponder];
}

-(IBAction)publishMessage:(id)sender{
    
   
    
    NSMutableArray* tableData  = [[NSUserDefaults standardUserDefaults]objectForKey:@"sentMessages"];
    NSMutableArray *sentMessages = [[NSMutableArray alloc] initWithArray:tableData];
    if(tableData == nil){
        tableData = [[NSMutableArray alloc]init];
    }
    
    NSMutableDictionary *groupDict = [[NSUserDefaults standardUserDefaults]objectForKey:@"group"];
    
    NSString *group_code = [groupDict objectForKey:@"group_code"];
    NSString *message = textView.text;
    
    [sentMessages insertObject:message atIndex:0];
    
    [[NSUserDefaults standardUserDefaults]setObject:sentMessages forKey:@"sentMessages"];
   XMPPNative *xmpp = [XMPPNative sharedManager];
    xmpp.home = self;
    [xmpp gotRoster];
    
    [xmpp publish_message:message :group_code];
    
    sentController.tableData = sentMessages;
    
    [sentController.tableView reloadData];
    
    [self dismissModalViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
