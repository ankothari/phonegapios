//
//  JoinGroupViewController.m
//  IdleCampus
//
//  Created by Ankur Kothari on 6/7/13.
//
//

#import "JoinGroupViewController.h"
#import "XMPPNative.h"
@interface JoinGroupViewController ()

@end

@implementation JoinGroupViewController
@synthesize group,username,password,email;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
    
    
    
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed: @"topBlueBarWithText1.png"] forBarMetrics:UIBarMetricsDefault];
    // Do any additional setup after loading the view from its nib.
    
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [leftButton setUserInteractionEnabled:NO];
    [leftButton setImage:[UIImage imageNamed:@"backButtonWithText.png"] forState:UIControlStateNormal];
    leftButton.frame = CGRectMake(0, 0, 44, 30);
    [leftButton addTarget:self action:@selector(goback:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    [self.navigationItem setLeftBarButtonItem:item animated:NO];
    [leftButton release];
}

- (void)goback:(id) sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)joinGroup:(id)sender{
    [group resignFirstResponder];
    NSString *sgroup = group.text;
   
    
    if(sgroup == NULL || [sgroup length]==0){
        
        [[[[UIAlertView alloc]
           initWithTitle:@"Error"
           message:@"Please enter a group code"
           delegate:self
           cancelButtonTitle:@"Ok"
           otherButtonTitles: nil] autorelease] show];
    }
    
    else{
    
//     first see if the group is present
    NSString* urlString = [NSString stringWithFormat:@"http://idlecampus.com/groups/get_group_name?group_code=%@",group.text];
    
    NSURL *group_name_url = [NSURL URLWithString:urlString];        // Create the request.
    
    NSData* data = [NSData dataWithContentsOfURL:
                    group_name_url];
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //       hud.mode = MBProgressHUDModeAnnularDeterminate;
    hud.labelText = @"Searching Group";
    
   
        
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];//6GT3XJ
        
        
        
        [self registerUserWithGroup:data];
        
    

    }
    
   }

-(void)registerUserWithGroup:(NSData *)data{
    
   NSString* newStr = [NSString stringWithUTF8String:[data bytes]];
    
    if([newStr isEqualToString:@"group not found"]){
        [[[[UIAlertView alloc]
           initWithTitle:@"Error"
           message:@"Group not found"
           delegate:self
           cancelButtonTitle:@"Ok"
           otherButtonTitles: nil] autorelease] show];
    }
    else{
        
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //       hud.mode = MBProgressHUDModeAnnularDeterminate;
    hud.labelText = @"Loading";
    
    xmpp = [XMPPNative sharedManager];
    xmpp.home = self;
    [xmpp gotRoster];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    
    [defaults setObject:username forKey:kXMPPmyJID];
    [defaults setObject:password forKey:kXMPPmyPassword];
    [defaults synchronize];
    
    
    [xmpp registerUser:username password:password email:email group:group.text];
    }
}
- (IBAction)hideKeyboard:(id)sender {
    [sender resignFirstResponder];
    //    [self done:sender];
}

- (void)dealloc {
    [username release];
    [super dealloc];
}

@end
