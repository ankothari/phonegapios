//
//  BingoViewController.m
//  IdleCampus
//
//  Created by Ankur Kothari on 15/7/13.
//
//

#import "BingoViewController.h"
#include <stdlib.h>
#import "Bingo.h"
@interface BingoViewController ()

@end

@implementation BingoViewController
@synthesize numbers,points,lines,countX,countY,myTurn,bingo,userPlayingBingoWith,friendName,whoseTurn;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotMessage:) name:@"receivedBingo" object:nil];
    
   
    
    
    
    [super viewWillAppear:animated];
}
- (void)viewWillDisappear:(BOOL)animated
{
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:(Bingo *)self.view];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}

- (void)gotMessage:(NSNotification *) notification{
    
    NSString *s = [[NSUserDefaults standardUserDefaults]objectForKey:@"numberOfUnreadMails"];
    if([s length] == 0){
        int so = [s intValue];
        so++;
        NSString *snew = [NSString stringWithFormat:@"%d",so];
        [[NSUserDefaults standardUserDefaults] setObject:snew  forKey:@"numberOfUnreadMails"];
        
    }
    else{
        int so = [s intValue];
        so++;
        NSString *snew = [NSString stringWithFormat:@"%d",so];
        [[NSUserDefaults standardUserDefaults] setObject:snew  forKey:@"numberOfUnreadMails"];
    }
 
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    ((Bingo *)(self.view)).userPlayingBingoWith = self.userPlayingBingoWith;
    
    

    friendName.text = userPlayingBingoWith;

    
    ((Bingo *)self.view).userPlayingBingoWith = userPlayingBingoWith;

    
    
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed: @"topBlueBarWithText1.png"] forBarMetrics:UIBarMetricsDefault];
    // Do any additional setup after loading the view from its nib.
    
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [leftButton setUserInteractionEnabled:NO];
    [leftButton setImage:[UIImage imageNamed:@"backButtonWithText.png"] forState:UIControlStateNormal];
    leftButton.frame = CGRectMake(0, 0, 44, 30);
    [leftButton addTarget:self action:@selector(goback:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    [self.navigationItem setLeftBarButtonItem:item animated:NO];
    [leftButton release];
}
-(IBAction)newGame:(id)sender{
    
}

-(IBAction)newTournament:(id)sender{
    
}

- (void)goback:(id) sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
