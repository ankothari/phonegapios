
#import "XMPPBridge.h"

@class Reachability;
@interface XMPPNative:NSObject<UIAlertViewDelegate>{
    NSMutableDictionary *commands;
    XMPPBridge *bridge;
    NSString *rosterid;
    NSMutableData *receivedData;
    NSURLConnection *conn;
    UIViewController *home;
    NSString *groupToRegisterWith;
    Reachability* hostReach;
    Reachability* internetReach;
    Reachability* wifiReach;
    NSString *fromUser;
    NSString *status;

}
@property (retain,nonatomic) NSString *fromUser;
@property (retain,nonatomic) NSString *status;
@property (retain,nonatomic) NSString *groupToRegisterWith;
@property (retain,nonatomic) UIViewController *home;
@property (nonatomic, assign) dispatch_queue_t queue;
+ (id)sharedManager;
- (void)echo;
- (void)send;
- (void)login:(NSString *)username password:(NSString *)password;
- (void)login1:(NSString *)username :(NSString *)password;
-(void)gotRoster;
-(void)gotGroups;
-(void)receiveMessage;
-(void)subscribeTo:(NSString *)user;
-(void)on_presence;
-(void)auth_user;
-(void)create_group;
-(void)join_group;
-(void)publish_message:(NSString *)message :(NSString *)to;
- (void)get_group_name;
- (void)disconnect;
- (void)registerUser:(NSString *)username password:(NSString *)password email:(NSString *)email group:(NSString *)group;
- (void)send:(NSString *)message :(NSString *)to;
- (void)sendBingo:(NSString *)message :(NSString *)to;
@end

