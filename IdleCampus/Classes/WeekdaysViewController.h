//
//  WeekdaysViewController.h
//  IdleCampus
//
//  Created by Ankur Kothari on 5/7/13.
//
//

#import <UIKit/UIKit.h>

@interface WeekdaysViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>{
    NSArray *tableData;
}

@property(retain,nonatomic) NSArray *tableData;
@property (retain, nonatomic) IBOutlet UIToolbar *topBar;
- (void)goback;
@end
