

#include <iostream>
#include <string>
#include <list>
#include <map>
#include "headers/jid.h"
#include "headers/client.h"
#include "headers/messagehandler.h"
#include "headers/clientbase.h"
#include "headers/connectiontcpclient.h"
#include <iostream.h>
#include "headers/gloox.h"
#include "headers/presence.h"
#include "headers/connectionlistener.h"
#include "headers/presencehandler.h"
#include "headers/messagehandler.h"
#include "headers/message.h"
#include "XMPPBridge.h"
#include "headers/rostermanager.h"
#include "headers/rosterlistener.h"
#include "headers/messagesession.h"
#include "headers/registrationhandler.h"
#include "headers/registration.h"
#include "headers/pubsubmanager.h"
#include "headers/pubsubresulthandler.h"
#include "headers/mucroomhandler.h"
#include "headers/pubsubitem.h"
#include "headers/disco.h"
#include "Client.h"
#include "headers/discohandler.h"
#include "headers/disconodehandler.h"
#import "HTTPDelegate.h"

class ConnListener : public gloox::ConnectionListener {
public:
    virtual void onConnect() {
        cout << "ConnListener::onConnect()" << endl;
    }
    virtual void onDisconnect(gloox::ConnectionError e) {
        cout << "ConnListener::onDisconnect() " << e << endl;
    }
    virtual bool onTLSConnect(const gloox::CertInfo& info) {
        cout << "ConnListener::onTLSConnect()" << endl;
        return false;
    }
};
class MyClass:public gloox::ConnectionListener,gloox::PresenceHandler,gloox::MessageHandler,gloox::RosterListener,gloox::RegistrationHandler,gloox::PubSub::ResultHandler,gloox::MUCRoomHandler,gloox::DiscoNodeHandler,gloox::DiscoHandler,gloox::IqHandler{
public:
    HTTPDelegate *delegate;
    bool toRegister;
    bool firstRegister;
    bool disconnected;
     struct gloox::RegistrationFields  rr;
    std::string user;
    NSMutableDictionary *groups;
    std::string password;
    std::string email;
    std::string group;
    gloox::PubSub::Manager *pubmgr;
    gloox::JID *jid ;
    gloox::Client *client;
    gloox::Client *client1;
    XMPPBridge *bridge;
    gloox::Registration * m_reg;
    gloox::RosterManager *rostermgr;
    void doSomething(std::string username,std::string password);
    void startChatWith(std::string message, std::string to);
     void playBingo(std::string message, std::string to);
    void subscribe(std::string user);
    void registerUser(std::string user,std::string password,std::string group,std::string email);
    void createNode(std::string node);
    void getGroups();
    void disconnect();
     void handlePresence( const gloox::Presence& presence );
    void publishNode(std::string node,std::string message);
    void unsubscribeNode(std::string node);
    void subscribeNode(std::string node);
    void deleteNode(std::string node);
     void onConnect();
    void sendUserDetailsToIdlecampus();
     void sendUserLoginDetailsToIdlecampus();
    void getGroupsJoined();
     void onDisconnect( gloox::ConnectionError e ) ;
     bool onTLSConnect( const gloox::CertInfo& info );
     void handleMessage( const gloox::Message& msg, gloox::MessageSession* session  );
     void handleItemAdded( const gloox::JID& jid ) ;
    void auth_user(std::string to,bool ack);
    void createMainNode(std::string node);
    void getSubscribersToGroup(std::string message);
            void handleItemSubscribed( const gloox::JID& jid ) ;
    
            void handleItemUpdated( const gloox::JID& jid ) ;
    
            void handleItemUnsubscribed( const gloox::JID& jid ) ;
    
            void handleRoster( const gloox::Roster& roster ) ;
    
            void handleRosterPresence( const gloox::RosterItem& item, const std::string& resource,
                                                                                            gloox::Presence::PresenceType presence, const std::string& msg ) ;
    
            void handleSelfPresence( const gloox::RosterItem& item, const std::string& resource,
                                                                                        gloox::Presence::PresenceType presence, const std::string& msg ) ;
    
            bool handleSubscriptionRequest( const gloox::JID& jid, const std::string& msg ) ;
            bool handleUnsubscriptionRequest( const gloox::JID& jid, const std::string& msg ) ;
     
            void handleNonrosterPresence( const gloox::Presence& presence ) ;
     void handleRosterError( const gloox::IQ& iq );
     void handleItemRemoved( const gloox::JID& jid );
    
     void handleRegistrationFields( const gloox::JID& from, int fields,
                                                                                        std::string instructions ) ;
    
            void handleAlreadyRegistered( const gloox::JID& from ) ;
    
            void handleRegistrationResult( const gloox::JID& from, gloox::RegistrationResult regResult ) ;
    
            void handleDataForm( const gloox::JID& from, const gloox::DataForm& form ) ;
    
            void handleOOB( const gloox::JID& from, const gloox::OOB& oob ) ;
    
     void handleItem( const gloox::JID& service,
                            const std::string& node,
                            const gloox::Tag* entry ) ;
    
    /**
     * Receives the list of Items for a node.
     *
     * @param id The reply IQ's id.
     * @param service Service hosting the queried node.
     * @param node ID of the queried node (empty for the root node).
     * @param itemList List of contained items.
     * @param error Describes the error case if the request failed.
     *
     * @see Manager::requestItems()
     */
     void handleItems( const std::string& id,
                             const gloox::JID& service,
                             const std::string& node,
                             const ::gloox::PubSub::ItemList& itemList,
                             const gloox::Error* error  ) ;
    
    /**
     * Receives the result for an item publication.
     *
     * @param id The reply IQ's id.
     * @param service Service hosting the queried node.
     * @param node ID of the queried node. If empty, the root node has been queried.
     * @param itemList List of contained items.
     * @param error Describes the error case if the request failed.
     *
     * @see Manager::publishItem
     */
     void handleItemPublication( const std::string& id,
                                       const gloox::JID& service,
                                       const std::string& node,
                                       const gloox::PubSub::ItemList& itemList,
                                       const gloox::Error* error  ) ;
    
    /**
     * Receives the result of an item removal.
     *
     * @param id The reply IQ's id.
     * @param service Service hosting the queried node.
     * @param node ID of the queried node. If empty, the root node has been queried.
     * @param itemList List of contained items.
     * @param error Describes the error case if the request failed.
     *
     * @see Manager::deleteItem
     */
     void handleItemDeletion( const std::string& id,
                             const gloox::JID& service,
                                    const std::string& node,
                                    const gloox::PubSub::ItemList& itemList,
                                    const gloox::Error* error  ) ;
    
    /**
     * Receives the subscription results. In case a problem occured, the
     * Subscription ID and SubscriptionType becomes irrelevant.
     *
     * @param id The reply IQ's id.
     * @param service PubSub service asked for subscription.
     * @param node Node asked for subscription.
     * @param sid Subscription ID.
     * @param jid Subscribed entity.
     * @param subType Type of the subscription.
     * @param error Subscription Error.
     *
     * @see Manager::subscribe
     */
     void handleSubscriptionResult( const std::string& id,
                                          const gloox::JID& service,
                                          const std::string& node,
                                          const std::string& sid,
                                          const gloox::JID& jid,
                                   const gloox::PubSub::SubscriptionType subType,
                                          const gloox::Error* error  ) ;
    
    /**
     * Receives the unsubscription results. In case a problem occured, the
     * subscription ID becomes irrelevant.
     *
     * @param id The reply IQ's id.
     * @param service PubSub service.
     * @param error Unsubscription Error.
     *
     * @see Manager::unsubscribe
     */
     void handleUnsubscriptionResult( const std::string& id,
                                            const gloox::JID& service,
                                            const gloox::Error* error  ) ;
    
    /**
     * Receives the subscription options for a node.
     *
     * @param id The reply IQ's id.
     * @param service Service hosting the queried node.
     * @param jid Subscribed entity.
     * @param node ID of the node.
     * @param options Options DataForm.
     * @param error Subscription options retrieval Error.
     *
     * @see Manager::getSubscriptionOptions
     */
     void handleSubscriptionOptions( const std::string& id,
                                           const gloox::JID& service,
                                           const gloox::JID& jid,
                                           const std::string& node,
                                           const gloox::DataForm* options,
                                           const gloox::Error* error  ) ;
    
    /**
     * Receives the result for a subscription options modification.
     *
     * @param id The reply IQ's id.
     * @param service Service hosting the queried node.
     * @param jid Subscribed entity.
     * @param node ID of the queried node.
     * @param error Subscription options modification Error.
     *
     * @see Manager::setSubscriptionOptions
     */
     void handleSubscriptionOptionsResult( const std::string& id,
                                                 const gloox::JID& service,
                                                 const gloox::JID& jid,
                                                 const std::string& node,
                                                 const gloox::Error* error  ) ;
    
    
    /**
     * Receives the list of subscribers to a node.
     *
     * @param id The reply IQ's id.
     * @param service Service hosting the node.
     * @param node ID of the queried node.
     * @param list Subscriber list.
     * @param error Subscription options modification Error.
     *
     * @see Manager::getSubscribers
     */
     void handleSubscribers( const std::string& id,
                                   const gloox::JID& service,
                                   const std::string& node,
                                   const gloox::PubSub::SubscriberList* list,
                                   const gloox::Error* error  ) ;
    
    /**
     * Receives the result of a subscriber list modification.
     *
     * @param id The reply IQ's id.
     * @param service Service hosting the node.
     * @param node ID of the queried node.
     * @param list Subscriber list.
     * @param error Subscriber list modification Error.
     *
     * @see Manager::setSubscribers
     */
     void handleSubscribersResult( const std::string& id,
                                         const gloox::JID& service,
                                         const std::string& node,
                                         const gloox::PubSub::SubscriberList* list,
                                         const gloox::Error* error  ) ;
    
    /**
     * Receives the affiliate list for a node.
     *
     * @param id The reply IQ's id.
     * @param service Service hosting the node.
     * @param node ID of the queried node.
     * @param list Affiliation list.
     * @param error Affiliation list retrieval Error.
     *
     * @see Manager::getAffiliations
     */
     void handleAffiliates( const std::string& id,
                                  const gloox::JID& service,
                                  const std::string& node,
                                  const gloox::PubSub::AffiliateList* list,
                                  const gloox::Error* error  ) ;
    
    /**
     * Handle the affiliate list for a specific node.
     *
     * @param id The reply IQ's id.
     * @param service Service hosting the node.
     * @param node ID of the node.
     * @param list The Affiliate list.
     * @param error Affiliation list modification Error.
     *
     * @see Manager::setAffiliations
     */
     void handleAffiliatesResult( const std::string& id,
                                        const gloox::JID& service,
                                        const std::string& node,
                                        const gloox::PubSub::AffiliateList* list,
                                        const gloox::Error* error  ) ;
    
    
    /**
     * Receives the configuration for a specific node.
     *
     * @param id The reply IQ's id.
     * @param service Service hosting the node.
     * @param node ID of the node.
     * @param config Configuration DataForm.
     * @param error Configuration retrieval Error.
     *
     * @see Manager::getNodeConfig
     */
     void handleNodeConfig( const std::string& id,
                                  const gloox::JID& service,
                                  const std::string& node,
                                  const gloox::DataForm* config,
                                  const gloox::Error* error  ) ;
    
    /**
     * Receives the result of a node's configuration modification.
     *
     * @param id The reply IQ's id.
     * @param service Service hosting the node.
     * @param node ID of the node.
     * @param error Configuration modification Error.
     *
     * @see Manager::setNodeConfig
     */
     void handleNodeConfigResult( const std::string& id,
                                        const gloox::JID& service,
                                        const std::string& node,
                                        const gloox::Error* error  ) ;
    
    /**
     * Receives the result of a node creation.
     *
     * @param id The reply IQ's id.
     * @param service Service hosting the node.
     * @param node ID of the node.
     * @param error Node creation Error.
     *
     * @see Manager::setNodeConfig
     */
     void handleNodeCreation( const std::string& id,
                                    const gloox::JID& service,
                                    const std::string& node,
                                    const gloox::Error* error  ) ;
    
    /**
     * Receives the result for a node removal.
     *
     * @param id The reply IQ's id.
     * @param service Service hosting the node.
     * @param node ID of the node.
     * @param error Node removal Error.
     *
     * @see Manager::deleteNode
     */
     void handleNodeDeletion( const std::string& id,
                                    const gloox::JID& service,
                                    const std::string& node,
                                    const gloox::Error* error  ) ;
    
    
    /**
     * Receives the result of a node purge request.
     *
     * @param id The reply IQ's id.
     * @param service Service hosting the node.
     * @param node ID of the node.
     * @param error Node purge Error.
     *
     * @see Manager::purgeNode
     */
     void handleNodePurge( const std::string& id,
                                 const gloox::JID& service,
                                 const std::string& node,
                                 const gloox::Error* error  ) ;
    
    /**
     * Receives the Subscription list for a specific service.
     *
     * @param id The reply IQ's id.
     * @param service The queried service.
     * @param subMap The map of node's subscription.
     * @param error Subscription list retrieval Error.
     *
     * @see Manager::getSubscriptions
     */
     void handleSubscriptions( const std::string& id,
                                     const gloox::JID& service,
                                     const gloox::PubSub::SubscriptionMap& subMap,
                                     const gloox::Error* error ) ;
    
    /**
     * Receives the Affiliation map for a specific service.
     *
     * @param id The reply IQ's id.
     * @param service The queried service.
     * @param affMap The map of node's affiliation.
     * @param error Affiliation list retrieval Error.
     *
     * @see Manager::getAffiliations
     */
     void handleAffiliations( const std::string& id,
                                    const gloox::JID& service,
                                    const gloox::PubSub::AffiliationMap& affMap,
                                    const gloox::Error* error  ) ;
    
    /**
     * Receives the default configuration for a specific node type.
     *
     * @param id The reply IQ's id.
     * @param service The queried service.
     * @param config Configuration form for the node type.
     * @param error Default node config retrieval Error.
     *
     * @see Manager::getDefaultNodeConfig
     */
     void handleDefaultNodeConfig( const std::string& id,
                                         const gloox::JID& service,
                                         const gloox::DataForm* config,
                                         const gloox::Error* error  ) ;
    
     void handleMUCParticipantPresence( gloox::MUCRoom* room, const gloox::MUCRoomParticipant participant,
                                                         const gloox::Presence& presence ) ;
    
            void handleMUCMessage( gloox::MUCRoom* room, const gloox::Message& msg, bool priv ) ;
    
            bool handleMUCRoomCreation( gloox::MUCRoom* room ) ;
    
            void handleMUCSubject( gloox::MUCRoom* room, const std::string& nick,
                                                                                    const std::string& subject ) ;
    
            void handleMUCInviteDecline( gloox::MUCRoom* room, const gloox::JID& invitee,
                                                                                                const std::string& reason ) ;
    
            void handleMUCError( gloox::MUCRoom* room, gloox::StanzaError error ) ;
    
            void handleMUCInfo( gloox::MUCRoom* room, int features, const std::string& name,
                                                                              const gloox::DataForm* infoForm ) ;
     
            void handleMUCItems( gloox::MUCRoom* room, const gloox::Disco::ItemList& items ) ;
    
    
     gloox::StringList handleDiscoNodeFeatures( const gloox::JID& from, const std::string& node ) ;
    
    /**
     * In addition to @c handleDiscoNodeFeatures, this function is used to gather
     * more information on a specific node. It is called when a disco#info query
     * arrives with a node attribute that matches the one registered for this handler.
     * @param from The sender of the request.
     * @param node The node this handler is supposed to handle.
     * @return A list of identities for the given node. The caller will own the identities.
     */
     gloox::Disco::IdentityList handleDiscoNodeIdentities( const gloox::JID& from,
                                                          const std::string& node ) ;
    
    /**
     * This function is used to gather more information on a specific node.
     * It is csalled when a disco#items query arrives with a node attribute that
     * matches the one registered for this handler. If node is empty, items for the
     * root node (no node) shall be returned.
     * @param from The sender of the request.
     * @param to The receiving JID (useful for transports).
     * @param node The node this handler is supposed to handle.
     * @return A list of items supported by this node.
     */
     gloox::Disco::ItemList handleDiscoNodeItems( const gloox::JID& from, const gloox::JID& to,
                                                 const std::string& node = gloox::EmptyString ) ;

    
     void handleDiscoInfo( const gloox::JID& from, const gloox::Disco::Info& info, int context ) ;
    
    /**
     * Reimplement this function if you want to be notified about the result
     * of a disco#items query.
     * @param from The sender of the disco#items result.
     * @param items The Items.
     * @param context A context identifier.
     * @since 1.0
     */
     void handleDiscoItems( const gloox::JID& from, const gloox::Disco::Items& items, int context ) ;
    
    /**
     * Reimplement this function to receive disco error notifications.
     * @param from The sender of the error result.
     * @param error The Error. May be 0.
     * @param context A context identifier.
     * @since 1.0
     */
     void handleDiscoError( const gloox::JID& from, const gloox::Error* error, int context ) ;
    
    /**
     * Reimplement this function to receive notifications about incoming IQ
     * stanzas of type 'set' in the disco namespace.
     * @param iq The full IQ.
     * @return Returns @b true if the stanza was handled and answered, @b false otherwise.
     */
    bool handleIq( const gloox::IQ& iq );
    void handleIqID( const gloox::IQ& iq, int context );
    void handleReceivedData (const gloox::ConnectionBase *connection, const std::string &data);
    void handleTag ( 	gloox::Tag * 	tag	 );
    void handleConnect( 	const gloox::ConnectionBase * 	connection	 );
};