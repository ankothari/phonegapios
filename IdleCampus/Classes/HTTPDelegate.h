//
//  HTTPDelegate.h
//  IdleCampus
//
//  Created by Ankur Kothari on 4/2/13.
//
//

#import <Foundation/Foundation.h>

@interface HTTPDelegate : NSObject<NSURLConnectionDelegate>{
    NSData *receivedData;
}
@property (nonatomic, retain) NSData *receivedData;
@end
