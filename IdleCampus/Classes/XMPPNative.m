
#import "XMPPNative.h"
#import "XMPPBridge.h"

#import "MyGroupViewController.h"
#import "HomeViewController.h"
#import "SentMessagesViewController.h"
#import "GroupMessagesViewController.h"
#import "Reachability.h"
#import <AudioToolbox/AudioServices.h>
#import "BingoViewController.h"
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) //1



#import "AppDelegate.h"

extern NSString *const kXMPPmyJID;
extern NSString *const kXMPPmyPassword;
@implementation XMPPNative

@synthesize queue,groupToRegisterWith,status,fromUser;
- (void)login:(NSString *)username password:(NSString *)password
{
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];
    
    
	hostReach = [[Reachability reachabilityWithHostName: @"www.apple.com"] retain];
	[hostReach startNotifier];
    	[self updateInterfaceWithReachability: hostReach];
	
    internetReach = [[Reachability reachabilityForInternetConnection] retain];
	[internetReach startNotifier];
    	[self updateInterfaceWithReachability: internetReach];
    
    wifiReach = [[Reachability reachabilityForLocalWiFi] retain];
	[wifiReach startNotifier];
    	[self updateInterfaceWithReachability: wifiReach];
    
    
       bridge = [[XMPPBridge alloc]init];
   
  
   
       
             
        bool b = [bridge connectWithUser:username password:password];
        

        
        
       
   
}
- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
    if(curReach == hostReach)
	{
//		[self configureTextField: remoteHostStatusField imageView: remoteHostIcon reachability: curReach];
        NetworkStatus netStatus = [curReach currentReachabilityStatus];
        BOOL connectionRequired= [curReach connectionRequired];
        
//        summaryLabel.hidden = (netStatus != ReachableViaWWAN);
        NSString* baseLabel=  @"";
        if(connectionRequired)
        {
            baseLabel=  @"Cellular data network is available.\n  Internet traffic will be routed through it after a connection is established.";
        }
        else
        {
            baseLabel=  @"Cellular data network is active.\n  Internet traffic will be routed through it.";
        }
//        summaryLabel.text= baseLabel;
    }
	if(curReach == internetReach)
	{
//		[self configureTextField: internetConnectionStatusField imageView: internetConnectionIcon reachability: curReach];
	}
	if(curReach == wifiReach)
	{
//		[self configureTextField: localWiFiConnectionStatusField imageView: localWiFiConnectionIcon reachability: curReach];
	}
	
}


- (void) configureTextField: (UITextField*) textField imageView: (UIImageView*) imageView reachability: (Reachability*) curReach
{
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    BOOL connectionRequired= [curReach connectionRequired];
    NSString* statusString= @"";
    switch (netStatus)
    {
        case NotReachable:
        {
            statusString = @"Access Not Available";
            imageView.image = [UIImage imageNamed: @"stop-32.png"] ;
            //Minor interface detail- connectionRequired may return yes, even when the host is unreachable.  We cover that up here...
            connectionRequired= NO;
            break;
        }
            
        case ReachableViaWWAN:
        {
            statusString = @"Reachable WWAN";
            imageView.image = [UIImage imageNamed: @"WWAN5.png"];
            break;
        }
        case ReachableViaWiFi:
        {
            statusString= @"Reachable WiFi";
            imageView.image = [UIImage imageNamed: @"Airport.png"];
            break;
        }
    }
    if(connectionRequired)
    {
        statusString= [NSString stringWithFormat: @"%@, Connection Required", statusString];
    }
    textField.text= statusString;
}



//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
	[self updateInterfaceWithReachability: curReach];
}




- (void)login1:(NSString *)username :(NSString *)password
{
    bridge = [[XMPPBridge alloc]init];
    
    
    
    
    
    bool b = [bridge connectWithUser:username password:password];
    
    
    
    
    
    
}

- (void)registerUser:(NSString *)username password:(NSString *)password email:(NSString *)email group:(NSString *)group {
    bridge = [[XMPPBridge alloc]init];
    groupToRegisterWith = group;
    
    status = @"register";
    NSArray *a = [[NSArray alloc]initWithObjects:username,password,nil ];
    if (username != nil && [username length] > 0) {
//        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"true"];
//        [pluginResult setKeepCallbackAsBool:YES];

        bool b = [bridge registerUser:username password:password group:group email:email];

//    } else {
//        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
//    }
//    
//    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        
    }
}


-(void)receiveMessage{
   
           
     
    
     
}
-(void)create_group:(NSString *)group {
    
   
   
    
   
        [bridge create_group:group];
       
   
    
    
    
    
}

-(void)join_group:(NSString *)group {
    
     
            [bridge join_group:group];
}
-(void)receiveTestNotification:(NSNotification *) notification{
    
           NSDictionary *userInfo = notification.userInfo;
        NSString *message = [userInfo objectForKey:@"message"];
    NSString *presence = [userInfo objectForKey:@"presence"];
        NSMutableArray *roster = [userInfo objectForKey:@"roster"];
    NSMutableDictionary *groups = [userInfo objectForKey:@"groups"];
     NSMutableDictionary *registered = [userInfo objectForKey:@"registered"];
    int registered_int = [[userInfo objectForKey:@"registered"] intValue];
    int login1_int = [[userInfo objectForKey:@"login1"] intValue];
   
  
       
    
        if ([roster count]>0) {
            //when u get the roster do soemthing here.
            [[NSUserDefaults standardUserDefaults] setObject:roster forKey:@"roster"];
            
            
        }

    if ([groups count]>0) {

        NSLog(@"got groups");
        
        if([[groups objectForKey:@"mygroups"] count]>0){
            
            [[NSUserDefaults standardUserDefaults] setObject:[[groups objectForKey:@"mygroups"] objectAtIndex:0] forKey:@"group_code"];
            NSString* urlString = [NSString stringWithFormat:@"http://idlecampus.com/groups/get_group_name?group_code=%@",[[groups objectForKey:@"mygroups"] objectAtIndex:0]];
            
            NSURL *group_name_url = [NSURL URLWithString:urlString];        // Create the request.
            
                            NSData* data = [NSData dataWithContentsOfURL:
                                group_name_url];
                
                NSLog(@"%@", [[groups objectForKey:@"mygroups"] objectAtIndex:0]);
                [self performSelectorOnMainThread:@selector(getMyGroup:)
                                       withObject:data waitUntilDone:YES];
          
            
            
           
        }
        else{
        NSString* urlString = [NSString stringWithFormat:@"http://idlecampus.com/groups/get_group_name?group_code=%@",[[groups objectForKey:@"groupsfollowing"] objectAtIndex:0]];
    
        NSURL *group_name_url = [NSURL URLWithString:urlString];
            
            queue = dispatch_queue_create("mygroupsfollowing", NULL);
            
           
                
                NSData* data = [NSData dataWithContentsOfURL:
                                group_name_url];
                
              
                [self performSelectorOnMainThread:@selector(getGroupFollowing:)
                                       withObject:data waitUntilDone:YES];
           

        
             }
        
               

    }
       if([message length]>0){
           
          fromUser = [userInfo objectForKey:@"fromUser"];
           NSRange range = [fromUser rangeOfString:@"@"];
           int indexOfAt = 0;
           if ( range.length > 0 ) {
               indexOfAt = range.location;
           } else {
               indexOfAt = -1;
           }
           NSString *singleUser ;
           if(indexOfAt != -1)
           singleUser  = [fromUser substringWithRange:NSMakeRange(0, indexOfAt)];
           NSLog(@"%@",singleUser);
           NSString *fromApp = [userInfo objectForKey:@"fromApp"];
           
           if ([fromApp isEqualToString:@"Bingo"]) {
               NSMutableArray *a = [[NSMutableArray alloc]initWithObjects:message, nil];
               
               NSArray *key =[[NSArray alloc]initWithObjects:@"mesage", nil];
               NSDictionary *dic = [[NSDictionary alloc]initWithObjects:a forKeys:key];
               [[NSNotificationCenter defaultCenter]
                postNotificationName:@"receivedBingo"
                object:self userInfo:dic];
           }
           if ([fromApp isEqualToString:@"idlecampus"]) {
               
               if ([message isEqualToString:@"newBingogame"]) {
                   
                   NSString *alertMessage = [NSString stringWithFormat:@"Do you want to play with %@",singleUser];
                   
                   dispatch_sync(dispatch_get_main_queue(), ^{
                       
                       UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"Bingo Request"
                                                                        message:alertMessage
                                                                       delegate:self
                                                              cancelButtonTitle:@"Cancel"
                                                              otherButtonTitles:@"Play", nil] autorelease];
                       
                       [alert show];
                   });

                   
               }
               NSMutableArray* tableData  = [[NSUserDefaults standardUserDefaults]objectForKey:@"receivedMessages"];
               
               NSMutableArray *receivedMessages = [[NSMutableArray alloc]initWithArray:tableData];
               
               
               [receivedMessages insertObject:message atIndex:0];
               
               //           ((GroupMessagesViewController *)_home).tableData = receivedMessages;
               //           [((GroupMessagesViewController *)_home).tableView reloadData];
               
               [[NSUserDefaults standardUserDefaults]setObject:receivedMessages forKey:@"receivedMessages"];
               NSMutableArray *a = [[NSMutableArray alloc]initWithObjects:message, nil];
               
               NSArray *key =[[NSArray alloc]initWithObjects:@"mesage", nil];
               NSDictionary *dic = [[NSDictionary alloc]initWithObjects:a forKeys:key];
               [[NSNotificationCenter defaultCenter]
                postNotificationName:@"receivedMessage"
                object:self userInfo:dic];
               
               NSString *soundPath = [[NSBundle mainBundle] pathForResource:@"sms-received" ofType:@"wav"];
               SystemSoundID soundID;
               AudioServicesCreateSystemSoundID((CFURLRef)[NSURL fileURLWithPath: soundPath], &soundID);
               AudioServicesPlaySystemSound (soundID);
               //[soundPath release];
               NSLog(@"soundpath retain count: %d", [soundPath retainCount]);
               
   
           }
           
                            
           
                   }
    
    
    if([presence length]>0){
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"Friend Request"
                                                             message:@"Do you want to add this contact to your friends list?"
                                                            delegate:self
                                                   cancelButtonTitle:@"Cancel"
                                                   otherButtonTitles:@"Add", nil] autorelease];
            
            [alert show];
        });
      
    }



    
    if(registered_int){
        
        NSString* urlString = [NSString stringWithFormat:@"http://idlecampus.com/groups/get_group_name?group_code=%@",groupToRegisterWith];
        
        NSURL *group_name_url = [NSURL URLWithString:urlString];        // Create the request.
        
        NSData* data = [NSData dataWithContentsOfURL:
                        group_name_url];
        
              
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            
            [MBProgressHUD hideHUDForView:_home.view animated:YES];
            
            
            
            [self getGroupFollowing:data];

        });
        
                
        

    }
    
    
    if(login1_int){
        
        if(login1_int == 1){
            dispatch_sync(dispatch_get_main_queue(), ^{
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                
                
                
                [defaults setObject:@"" forKey:kXMPPmyJID];
                [defaults setObject:@"" forKey:kXMPPmyPassword];
                
                
                [defaults synchronize];
                [MBProgressHUD hideHUDForView:_home.view animated:YES];
                
                [[[[UIAlertView alloc]
                   initWithTitle:@"Error"
                   message:@"Username and Password do not match"
                   delegate:self
                   cancelButtonTitle:@"Ok"
                   otherButtonTitles: nil] autorelease] show];
            });
           

           

            
        }else{
        
        [self gotGroups];
        
        }
        
        
          

    }






    
}

- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0){
       
          }else if (buttonIndex == 1){
        //friend request accepted
        
              
              NSMutableArray *a = [[NSMutableArray alloc]initWithObjects:fromUser, nil];
              
              NSArray *key =[[NSArray alloc]initWithObjects:@"fromUser", nil];
              NSDictionary *dic = [[NSDictionary alloc]initWithObjects:a forKeys:key];
              [[NSNotificationCenter defaultCenter]
               postNotificationName:@"newBingoGame"
               object:self userInfo:dic];
              

    }
}

-(void)getTimetable:(NSString *)group{
    
    NSMutableDictionary *timetableDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"timetable"];
    if(timetableDict){
        return;
    }else{
        Timetable *timetable = [[Timetable alloc]init];
        NSString *surl = [NSString stringWithFormat:@"http://idlecampus.com/groups/%@/timetable.json",group];
        [timetable notification:surl];
    }
   
    
    
}

-(void)showGroup:(NSString *)group{
    MyGroupViewController *myGroupViewController = [[MyGroupViewController alloc]
                                                    initWithNibName:@"MyGroupViewController"
                                                    bundle:nil];
    myGroupViewController.group = group;
    
//    [self gotRoster];
    
    [_home.navigationController pushViewController:myGroupViewController animated:NO];
    
    _home = myGroupViewController;
    
}

-(void)showMyGroup:(NSString *)group{
    SentMessagesViewController *sentMessagesViewController = [[SentMessagesViewController alloc]
                                                    initWithNibName:@"SentMessagesViewController"
                                                    bundle:nil];
    
    sentMessagesViewController.group = group;
    
    
    
    [_home.navigationController pushViewController:sentMessagesViewController animated:NO]; //
                                     
    
}

- (void)getGroupFollowing:(NSData *)responseData {
    //parse out the json data
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData //1
                          
                          options:kNilOptions
                          error:&error];
    
    NSString* group_name = [json objectForKey:@"group_name"]; //2
    NSString* group_code = [json objectForKey:@"group_code"]; //2
    
    
    //NSMutableDictionary myDictionary = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *myDictionary = [[NSMutableDictionary alloc] init];
    
    
    NSNumber *code = [NSNumber numberWithInt:2];
    [myDictionary setObject:code  forKey:@"code"];
    [myDictionary setObject:group_code  forKey:@"group_code"];
    [myDictionary setObject:group_name  forKey:@"name"];
    
    [[NSUserDefaults standardUserDefaults] setObject:myDictionary forKey:@"group"];
    
      
    NSLog(@"group_name: %@", group_name); //3
    [self getTimetable:group_code];
    if (![status isEqualToString:@"resume"]) {
        [self performSelectorOnMainThread:@selector(showGroup:)
                               withObject:group_name waitUntilDone:YES];
    }

    
}
- (void)getMyGroup:(NSData *)responseData {
    //parse out the json data
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData //1
                          
                          options:kNilOptions
                          error:&error];
    
    NSString* group_name = [json objectForKey:@"group_name"]; //2
    NSString* group_code = [json objectForKey:@"group_code"]; //2
 
    
    //NSMutableDictionary myDictionary = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *myDictionary = [[NSMutableDictionary alloc] init];
    
   
        NSNumber *code = [NSNumber numberWithInt:1];
        [myDictionary setObject:code  forKey:@"code"];
    [myDictionary setObject:group_code  forKey:@"group_code"];
        [myDictionary setObject:group_name  forKey:@"name"];
       
   [[NSUserDefaults standardUserDefaults] setObject:myDictionary forKey:@"group"];//    
//    [self getTimetable:group_code];
    
    if (![status isEqualToString:@"resume"]) {
        [self performSelectorOnMainThread:@selector(showMyGroup:)
                               withObject:group_name waitUntilDone:YES];
    }

}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // This method is called when the server has determined that it
    // has enough information to create the NSURLResponse.
    
    // It can be called multiple times, for example in the case of a
    // redirect, so each time we reset the data.
    
    // receivedData is an instance variable declared elsewhere.
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the new data to receivedData.
    // receivedData is an instance variable declared elsewhere.
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
    // release the connection, and the data object
    [connection release];
    // receivedData is declared as a method instance elsewhere
    [receivedData release];
    
    // inform the user
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // do something with the data
    // receivedData is declared as a method instance elsewhere
    NSLog(@"Succeeded! Received %d bytes of data",[receivedData length]);
    
    // release the connection, and the data object
    [connection release];
    [receivedData release];
}

- (void)disconnect{
//    CDVPluginResult* pluginResult = nil;
//    [commands removeAllObjects];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
        [bridge disconnect];
           
//    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)removeAllObjects{
   
    [commands removeAllObjects];
     [[NSNotificationCenter defaultCenter] removeObserver:self];
    
  
    
//    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)send:(NSString *)message :(NSString *)to{
   
  
   
        [bridge send:message to:to];
       
}

- (void)sendBingo:(NSString *)message :(NSString *)to{
    
    
    
    [bridge sendBingo:message to:to];
    
}




-(void)publish_message:(NSString *)message :(NSString *)to{
  
   
        [bridge publishMessage:message to:to];
      
   
}
-(void)gotRoster{
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"TestNotification" object:nil];
   
            
    
           [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(receiveTestNotification:)
                                                     name:@"TestNotification"
                                                   object:nil];
        
           

}

-(void)gotGroups{
    
    
  
   
     [bridge getGroups];
    
    
    
}
-(void)subscribeTo:(NSString *)user{
   
  
    
    
        [bridge subscribe:user];
       
}

-(void)on_presence{
}

#pragma mark Singleton Methods
+ (id)sharedManager {
     static XMPPNative *sharedMyManager = nil;
    @synchronized(self) {
        if(sharedMyManager == nil)
            sharedMyManager = [[super allocWithZone:NULL] init];
    }
    return sharedMyManager;
}
+ (id)allocWithZone:(NSZone *)zone {
    return [[self sharedManager] retain];
}
- (id)copyWithZone:(NSZone *)zone {
    return self;
}
- (id)retain {
    return self;
}
- (unsigned)retainCount {
    return UINT_MAX; //denotes an object that cannot be released
}
- (oneway void)release {
    // never release
}
- (id)autorelease {
    return self;
}
- (id)init {
    if (self = [super init]) {
//        someProperty = [[NSString alloc] initWithString:@"Default Property Value"];
    }
    return self;
}
- (void)dealloc {
    // Should never be called, but just here for clarity really.
//    [someProperty release];
    [super dealloc];
}
@end

