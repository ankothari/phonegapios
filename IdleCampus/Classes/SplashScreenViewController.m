//
//  SplashScreenViewController.m
//  IdleCampus
//
//  Created by Ankur Kothari on 6/7/13.
//
//

#import "SplashScreenViewController.h"
#import "XMPPNative.h"
#import "HomeViewController.h"
#import "Reachability.h"
#import "MyGroupViewController.h"
#import "SentMessagesViewController.h"
#import "RosterViewController.h"
@interface SplashScreenViewController ()

@end



@implementation SplashScreenViewController
@synthesize xmpp;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
     [self.navigationController setNavigationBarHidden:YES animated:animated];
    NSString *sUsername = [[NSUserDefaults standardUserDefaults] stringForKey:kXMPPmyJID];
    
    NSString *sPassword = [[NSUserDefaults standardUserDefaults] stringForKey:kXMPPmyPassword];
    
    if([sUsername length] == 0 && [sPassword length] == 0){
        HomeViewController *homeViewController = [[HomeViewController alloc]
                                                                      initWithNibName:@"HomeViewController"
                                                                      bundle:nil];
        
        [self.navigationController pushViewController:homeViewController animated:YES];
        
//        [self.view addSubview:homeViewController.view];
    }
    else{
        xmpp = [XMPPNative sharedManager];
        xmpp.home = self;
//                xmpp.status = @"resume"; //commnt this out remove rosterVC
        [xmpp gotRoster];
        
//        RosterViewController *rosterViewController = [[RosterViewController alloc]
//                                                      initWithNibName:@"RosterViewController"
//                                                      bundle:nil];
//        
//        [self.navigationController pushViewController:rosterViewController animated:YES];
        
        
        
        
        
//        [rosterViewController release];

     NSDictionary *group = [[NSUserDefaults standardUserDefaults] objectForKey:@"group"];
        NSString *scode = [group objectForKey:@"code"];
        int code = [scode intValue];
        NSString *group_name = [group objectForKey:@"name"];
        NSString *group_code = [group objectForKey:@"group_code"];
        if(code == 1){
            xmpp.status = @"resume";
            [self showMyGroup:group_name];
        }else if(code == 2){
            xmpp.status = @"resume";
            [self showGroup:group_name];
        }
        
    
    [xmpp login:sUsername password:sPassword ];
    }
    
    [super viewWillAppear:animated];

   
}

-(void)showGroup:(NSString *)group{
    MyGroupViewController *myGroupViewController = [[MyGroupViewController alloc]
                                                    initWithNibName:@"MyGroupViewController"
                                                    bundle:nil];
    myGroupViewController.group = group;
    
    //    [self gotRoster];
    
    [self.navigationController pushViewController:myGroupViewController animated:NO];
    
   
    
}

-(void)showMyGroup:(NSString *)group{
    SentMessagesViewController *sentMessagesViewController = [[SentMessagesViewController alloc]
                                                              initWithNibName:@"SentMessagesViewController"
                                                              bundle:nil];
    
    sentMessagesViewController.group = group;
    
    
    
    [self.navigationController pushViewController:sentMessagesViewController animated:NO]; //
    
    
}


- (void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}
- (void)viewDidLoad
{
    

    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
