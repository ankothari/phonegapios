var isAuthenticated = function (b) {
    localStorage.setItem("auth", b);
};
var timetable = {
notification: function (url) {
    console.log(url);
 
    $.get(url, function (data) {
          
          console.log(data);
          
          // Put the object into storage
          localStorage.setItem('data', JSON.stringify(data));
          
          // Retrieve the object from storage
          $('ul#timetableul li').not(".divider").remove();
          $('#Sunday').html("");
          $('#Monday').html("");
          $('#Tuesday').html("");
          $('#Wednesday').html("");
          $('#Thursday').html("");
          $('#Friday').html("");
          $('#Saturday').html("");
          var entries = data.timetable.entries;
          if(entries.length == 0){
            $("#timetablestatus").text("Not Available");
          }
          var weekdays = data.timetable.weekdays;
          for(var c=0;c < weekdays.length;c++){
          if(weekdays[c] == "Monday"){
          $("#timetableul").append("<li><a href='#"+weekdays[c]+"Page'>"+weekdays[c]+"</a></li>");
          }
          if(weekdays[c] == "Tuesday"){
           $("#timetableul").append("<li><a href='#"+weekdays[c]+"Page'>"+weekdays[c]+"</a></li>");
          }
          if(weekdays[c] == "Wednesday"){
           $("#timetableul").append("<li><a href='#"+weekdays[c]+"Page'>"+weekdays[c]+"</a></li>");
          }
          if(weekdays[c] == "Thursday"){
          $("#timetableul").append("<li><a href='#"+weekdays[c]+"Page'>"+weekdays[c]+"</a></li>");
          }
          if(weekdays[c] == "Friday"){
           $("#timetableul").append("<li><a href='#"+weekdays[c]+"Page'>"+weekdays[c]+"</a></li>");
          }
          if(weekdays[c] == "Saturday"){
           $("#timetableul").append("<li><a href='#"+weekdays[c]+"Page'>"+weekdays[c]+"</a></li>");
          }
          if(weekdays[c] == "Sunday"){
           $("#timetableul").append("<li><a href='#"+weekdays[c]+"Page'>"+weekdays[c]+"</a></li>");
          }
          }
           $('ul#timetableul').listview("refresh");
          for (var a = 0; a < entries.length; a++) {
          
          for (var b = 0; b < entries[a].length; b++) {
          
          for (var c = 0; c < entries[a][b].length; c++) {
          console.log(entries[a][b][c]);
          obj = entries[a][b][c];
          var str = '';
          str = str + timetable.getDisplayTime(entries[a]) + '\n';
          for (var key in obj) {
          if (obj.hasOwnProperty(key) && obj[key] != null) {
          if (key != "from_hours" && key != "from_minutes" && key != "to_minutes" && key != "to_hours" && key != "weekday")
            str += key + ':' + obj[key] + '\n';
          }
          }
          
          st = "<li>" + str + "</li>";
          
          
          if (entries[a][b][c].weekday == "Monday") {
          console.log("Monday");
//          $('#limonday').show();
          $('#Monday').append(st);
          }
          if (entries[a][b][c].weekday == "Saturday") {
          console.log("Saturday");
//          $('#lisaturday').show();
          $('#Saturday').append(st);
          }
          if (entries[a][b][c].weekday == "Tuesday") {
          console.log("Tuesday");
//          $('#lituesday').show();
          $('#Tuesday').append(st);
          }
          
          if (entries[a][b][c].weekday == "Wednesday") {
          console.log("Wednesday");
//          $('#liwednesday').show();
          $('#Wednesday').append(st);
          }
          if (entries[a][b][c].weekday == "Thursday") {
          console.log("Thursday");
//          $('#lithursday').show();
          $('#Thursday').append(st);
          }
          if (entries[a][b][c].weekday == "Friday") {
          console.log("Friday");
//          $('#lifriday').show();
          $('#Friday').append(st);
          }
          if (entries[a][b][c].weekday == "Sunday") {
          console.log("Sunday");
//          $('#lisaturday').show();
          $('#Sunday').append(st);
          }
          
          
          }
          }
          }
          
//          var status = $("#timetablestatus").text();
//          
//          if (status == "Update Available") {
//          
//          
//          } else {
//          $.mobile.changePage('#timetable');
//          }
          
          
          });
    
},
getDisplayTime: function (time1) {
    var display_from, display_to, from_date_hour, from_date_minute, to_date_hour, to_date_minute;
    console.log("TTTTTTTTT");
    console.log(time1);
    console.log(time1[0]);
    console.log(time1[0][0]);
    if (time1.length > 0) {
        from_date_hour = time1[0][0].from_hours;
        from_date_minute = time1[0][0].from_minutes;
        to_date_minute = time1[0][0].to_minutes;
        to_date_hour = time1[0][0].to_hours;
        display_from = void 0;
        if (from_date_hour == 12 && from_date_minute == 0) {
            
            display_from = "12 NOON";
        } else if (from_date_hour >= 12) {
            display_from = from_date_hour - 12 + ":";
            display_from = display_from + from_date_minute;
            display_from = display_from + "PM";
        } else {
            display_from = from_date_hour + ":";
            display_from = display_from + from_date_minute;
            display_from = display_from + "AM";
        }
        display_to = void 0;
        if (to_date_hour == 12 && to_date_minute == 0) {
            
            display_to = "12 NOON"
        } else if (to_date_hour >= 12) {
            display_to = to_date_hour - 12 + ":";
            display_to = display_to + to_date_minute;
            display_to = display_to + "PM";
        } else {
            display_to = to_date_hour + ":";
            display_to = display_to + to_date_minute;
            display_to = display_to + "AM";
        }
        console.log(display_from + "-" + display_to);
        return display_from + "-" + display_to;
    }
},
show: function () {
    //get the stored timetable
    //    timetable.getDisplayTime("o");
    var data = localStorage.getItem('data');
    
    data = JSON.parse(data);
    console.log(data);
    $('ul#timetableul li').not(".divider").remove();
    $('#Sunday').html("");
    $('#Monday').html("");
    $('#Tuesday').html("");
    $('#Wednesday').html("");
    $('#Thursday').html("");
    $('#Friday').html("");
    $('#Saturday').html("");
    var entries = data.timetable.entries;
    console.log(entries);
    var weekdays = data.timetable.weekdays;
    for(var c=0;c < weekdays.length;c++){
        if(weekdays[c] == "Monday"){
            $("#timetableul").append("<li><a href='#"+weekdays[c]+"Page'>"+weekdays[c]+"</a></li>");
        }
        if(weekdays[c] == "Tuesday"){
             $("#timetableul").append("<li><a href='#"+weekdays[c]+"Page'>"+weekdays[c]+"</a></li>");
        }
        if(weekdays[c] == "Wednesday"){
             $("#timetableul").append("<li><a href='#"+weekdays[c]+"Page'>"+weekdays[c]+"</a></li>");
        }
        if(weekdays[c] == "Thursday"){
             $("#timetableul").append("<li><a href='#"+weekdays[c]+"Page'>"+weekdays[c]+"</a></li>");
        }
        if(weekdays[c] == "Friday"){
             $("#timetableul").append("<li><a href='#"+weekdays[c]+"Page'>"+weekdays[c]+"</a></li>");
        }
        if(weekdays[c] == "Saturday"){
             $("#timetableul").append("<li><a href='#"+weekdays[c]+"Page'>"+weekdays[c]+"</a></li>");
        }
        if(weekdays[c] == "Sunday"){
             $("#timetableul").append("<li><a href='#"+weekdays[c]+"Page'>"+weekdays[c]+"</a></li>");
        }
    }
    $('ul#timetableul').listview("refresh");


    for (var a = 0; a < entries.length; a++) {
        
        for (var b = 0; b < entries[a].length; b++) {
            for (var c = 0; c < entries[a][b].length; c++) {
                console.log(entries[a][b][c]);
                obj = entries[a][b][c]
                var str = '';
                str = str + timetable.getDisplayTime(entries[a]) + '\n';
                for (var key in obj) {
                    
                    if (obj.hasOwnProperty(key) && obj[key] != null) {
                        if (key != "from_hours" && key != "from_minutes" && key != "to_minutes" && key != "to_hours" && key != "weekday") str += key + ':' + obj[key] + '\n';
                    }
                }
                
                st = "<li>" + str + "</li>";
                console.log(st);
                               console.log(entries[a][b][c].weekday);
                if (entries[a][b][c].weekday == "Monday") {
                    console.log("Monday");
//                    $('#limonday').show();
                    $('#Monday').append(st);
                }
                if (entries[a][b][c].weekday == "Saturday") {
                    console.log("Saturday");
                    $('#lisaturday').show();
                    $('#Saturday').append(st);
                }
                if (entries[a][b][c].weekday == "Tuesday") {
                    console.log("Tuesday");
//                    $('#lituesday').show();
                    $('#Tuesday').append(st);
                }
                
                if (entries[a][b][c].weekday == "Wednesday") {
                    console.log("Wednesday");
//                    $('#liwednesday').show();
                    $('#Wednesday').append(st);
                }
                if (entries[a][b][c].weekday == "Thursday") {
                    console.log("Thursday");
//                    $('#lithursday').show();
                    $('#Thursday').append(st);
                }
                if (entries[a][b][c].weekday == "Friday") {
                    console.log("Friday");
//                    $('#lifriday').show();
                    $('#Friday').append(st);
                }
                if (entries[a][b][c].weekday == "Sunday") {
                    console.log("Sunday");
//                    $('#lisaturday').show();
                    $('#Sunday').append(st);
                }

                
                
            }
        }
    }
    
    
//    $.mobile.changePage('#timetable');
    
    
}
    
};

function onOnline() {
    console.log("online");
    var name = localStorage.getItem("name");
    var password = localStorage.getItem("password");
    
    $("#loginname").val(name);
    $("#loginpassword").val(password);
    
        
    
    login();
}

function onPause() {
    var name = localStorage.getItem("name");
    var password = localStorage.getItem("password");
    //    alert("onpaise "+name+" "+password);
    console.log(name + "!" + password + "!");
    console.log(name !== null);
    var isPaused = true;
    if (name && password) {
        xmpp.disconnect();
    }
    else {
        xmpp.removeAllObjects();
    }
}

function onResume() {
    
    var name = localStorage.getItem("name");
    var password = localStorage.getItem("password");
    //   alert("onreumse "+name+" "+password);
    
    startup();
    
    if (name && password) {
        
        xmpp.login(name, password, function (result_local) {
                   
                   //               alert(result_local);
                   
                   });
    }
    
    
}

function getGroupsJoined() {
    xmpp.getGroupsJoined(function (groups) {
                         alert(groups);
                         });
}

function checkName(name) {
    
}

function login() {
    var name = localStorage.getItem("name");
    var password = localStorage.getItem("password");
    
    //    alert("login "+name+" "+password);
    if (name !== null && password !== null) {
        
        xmpp.login(name, password, function (result_local) {
                   //                   alert(result_local);
                   //                   xmpp.gotGroups(function (data) {
                   //                                  alert("got groups here");
                   //                                  console.log(data);
                   //                                  });
                   
                   
                   group = localStorage.getItem("group_name");
                  
                   
                   $(".groupname").text(group);
                   //                   alert(group);
                   if (group) {
                   
                   //                   console.log($("#group").text());
                   //                   $("#grouplist").refresh();
                   
                    code = localStorage.getItem("code");
                   if (code == 1) {
                   $.mobile.changePage("#sentmessages", {
                                       transition: "none"
                                       });
                   
                   
                   } else if(code == 2){
                   $.mobile.changePage("#group", {
                                       transition: "none"
                                       });
                   
                   
                   }

//                   $.mobile.changePage("#group", {
//                                       transition: "none"
//                                       });
                   
                   //
                   //                   var data = localStorage.getItem('data');
                   //                   if (data)
                   //                   timetable.show();
                   //                   else {
                   //
                   //                   timetable.notification("http://idlecampus.com/timetable/get_timetable_for_group?group=" + data);
                   //                   }
                   }
                   //                   else {
                   //
                   //
                   //                   $.mobile.changePage("#joingroup", {
                   //                                       transition: "none"
                   //                                       });
                   //
                   //                   }
                   
                   });
        
    } else {
        
        $.mobile.changePage("#signin", {
                            transition: "none"
                            });
        
    }
    
    
}

function makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    
    for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    
    return text;
}


function startup() {
    xmpp.gotRoster(function (roster) {
                   console.log("h");
                   //        var rosterArray = roster.toString().split(",");
                   //
                   //        for (var i = 0; i < rosterArray.length; i++) {
                   //            console.log(rosterArray[i]);
                   //
                   //            // transform jid into an id
                   //            var jid_id = xmpp.jid_to_id(rosterArray[i]);
                   //            var name = xmpp.getNodeFromJid(rosterArray[i]);
                   //            var lcontact = "<li id='" + jid_id + "'>"
                   //                + "<div class='roster-contact offline'>"
                   //                + "<a href=#game><img class='ui-li-icon  ui-li-thumb' alt='' src=''><div class='roster-name'>" + name
                   //                + "</div><div class='roster-jid'>" + roster["Debasis Mishra"]//rosterArray[i]
                   //                + "</div></a></div></li>";
                   //
                   //            var contact = $(lcontact);
                   //
                   //            xmpp.insert_contact(contact);
                   //        }
                   
                   //                                      $.mobile.changePage("#friends", {
                   //                                                          transition : "none"
                   //                                                          });
                   //
                   
                   });
    
    
    xmpp.receiveMessage(function (msg) {
                        //                        console.log(msg);
                        //                                                alert(msg);
                        if ($.mobile.activePage.attr("id") == "messages") {
                        $("#count").text("0");
                        } else {
                        var c = $("#count").text();
                        
                        var count = parseInt(c);
                        
                        c++;
                        
                        $("#count").text(c);
                        }
                        
                        localStorage.setItem("count",c);
                        var n = msg.indexOf("#");
                        //
                        var full_jid = msg.substring(n + 1);
                        //
                        var jid = xmpp.getBareJidFromJid(full_jid);
                        //
                        var jid_id = xmpp.jid_to_id(jid);
                        //
                        var body = msg;//msg.substring(0, n);
                        //
                        var group = "<li>" + msg + "</li>";
                        var tempmsgs = localStorage.getItem("messages");
                        if (tempmsgs) {
                        console.log("TTTTTTT");
                        messages = JSON.parse(tempmsgs);
                        console.log(messages);
                        messages.push(msg);
                        console.log(JSON.stringify(messages));
                        localStorage.setItem("messages", JSON.stringify(messages));
                        } else {
                        var messages = []
                        messages.push(msg);
                        console.log(messages);
                        console.log(JSON.stringify(messages));
                        localStorage.setItem("messages", JSON.stringify(messages));
                        }
                        //                        messages = JSON.parse(localStorage.getItem("messages"));
                        //                        console.log(messages);
                        //                        messages.push(msg);
                        //                        localStorage["messages"] = JSON.stringify(messages);
                        
                        
                        $('ul.messageslist').append($(group));
                        
                        
                        //                                                $.mobile.changePage("#messages", {
                        //                                            transition: "none"
                        //                                            });
                        $('ul.messageslist').listview("refresh");
                        
                        
                        });
    
    
}


function checkConnection() {
    var networkState = navigator.connection.type;
    
    var states = {};
    states[Connection.UNKNOWN]  = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI]     = 'WiFi connection';
    states[Connection.CELL_2G]  = 'Cell 2G connection';
    states[Connection.CELL_3G]  = 'Cell 3G connection';
    states[Connection.CELL_4G]  = 'Cell 4G connection';
    states[Connection.NONE]     = 'No network connection';
    
//    alert('Connection type: ' + states[networkState]);
    return networkState;
    }

function onDeviceReady() {
    
//    checkConnection();
    navigator.splashscreen.hide();
    count = localStorage.getItem("count");
    if(count)
    $("#count").text(count);
    status = localStorage.getItem("status");
   
    if (status == "Update Available") {
       
        $("#timetablestatus").text("Update Available");
    }
    if (status == "New") {
        
        $("#timetablestatus").text("New");
    }
     else{
        $("#timetablestatus").text("");
    }
//    $("#registername").val(makeid);
    document.addEventListener("pause", onPause, false);
    document.addEventListener("resume", onResume, false);
    document.addEventListener("online", onOnline, false);
    startup();
    //login();
    
    //if there is a problem then remove the below code and replace it with the code in the js file. :)
    //load all the external javascript files here based on the name of the apps.
    
    //    $.getScript("js/bingo.js", function (data, textStatus, jqxhr) {
    //        console.log(data); //data returned
    //        console.log(textStatus); //success
    //        console.log(jqxhr.status); //200
    //        console.log('bingo was performed.');
    //    });
    //    $.getScript("js/timetable.js", function (data, textStatus, jqxhr) {
    //        console.log(data); //data returned
    //        console.log(textStatus); //success
    //        console.log(jqxhr.status); //200
    //        console.log('timetable was performed.');
    //    });
    //
    
    //    //console.log(device.uuid);
    //    $('#uldiv').delegate('li', 'tap', function () {
    //
    //        var from = $(this).find('#from').html();
    //        var subject = $(this).find('#subject').html();
    //        var body = $(this).find('#body').html();
    //        // console.log(from + " " + " "+subject+ " "+body);
    //        $('#emailfrom').html(from);
    //        $('#emailsubject').html(subject);
    //        //
    //        $('#emailbody').html(body);
    //
    //    });
    //    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
    //        xmlhttp = new XMLHttpRequest();
    //    }
    //    else {// code for IE6, IE5
    //        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    //    }
    //
    //
    //    //send my name to the database and retrieve the names of all the apps that i have installed. and not all the available apps.
    //    xmlhttp.open("GET", "http://192.168.0.22:8888/idlecampus-client/php/find.php", true);
    //    xmlhttp.send();
    //    xmlhttp.onreadystatechange = function () {
    //        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
    //            appsArrayAsAStringSeparatedByCommas = xmlhttp.responseText;
    //
    //            var a = appsArrayAsAStringSeparatedByCommas.substring(1);
    //            var appsArray = a.split(",");
    //            for (var i = 0; i < appsArray.length; i++) {
    //                var content = "<li id='" + appsArray[i] + "'>"
    //                    + "<a rel=external href=#game>" + appsArray[i] + "</a></li>";
    //                var app = $(content);
    //
    //                $('ul#ulapps').append(app);
    //            }
    //
    //
    //        }
    //    }
    
    
    //    xmpp.on_presence(function (request) {
    //        $('#approve-jid').text(xmpp.getBareJidFromJid(request));
    //        $.mobile.changePage("#approve_dialog", {
    //            transition: "none"
    //        });
    //
    //
    //    });
    
    $('.clear').click(function () {
                      localStorage.setItem("messages", "");
                      $('ul.messageslist').empty();
                      $('ul.messageslist').listview("refresh");
                      });
    
    $('#timetablebutton').click(function () {
                                
                                var data = localStorage.getItem('data');
                               
                                var status = $("#timetablestatus").text();
                                console.log(status);
                                if (status == "Update Available") {
                                var group = localStorage.getItem('group_code');
//                                alert(group);
                                timetable.notification("http://idlecampus.com/timetable/get_timetable_for_group?group=" + group);
                                $("#timetablestatus").text("");
                                 localStorage.setItem("status","a");
                                }
                                if (data){
                                $("#timetablestatus").text("");
                                localStorage.setItem("status","a");
                                    timetable.show();
                                }
                                
                                
                                
                                });
    
    $("#messagesbutton").click(function () {
                               $("#count").text("0");
                               localStorage.setItem("count","0");
                               $('ul#messageslist').html('');
                               var tempmsgs = localStorage.getItem("messages");
                               if (tempmsgs) {
                               var messages = JSON.parse(tempmsgs);
                               for (var i = 0; i < messages.length; i++) {
                               var group = "<li>" + messages[i] + "</li>";
                               $('ul#messageslist').append($(group));
                               
                               }
                               
                               }
                               
                               
                               $.mobile.changePage("#messages", {
                                                   transition: "none"
                                                   });
                               $('ul#messageslist').listview("refresh");
                               
                               });
    
    
    //
    //        var n = msg.indexOf("#");
    //        var full_jid = msg.substring(n + 1);
    //
    //        var jid = xmpp.getBareJidFromJid(full_jid);
    //
    //        var jid_id = xmpp.jid_to_id(jid);
    //
    //        var body = msg.substring(0, n);
    //
    //        var chat_full = $("#chat-area").val();
    //        chat_full = chat_full + "\n" + jid + ": " + body;
    //
    //        $("#chat-area").val(chat_full);
    //        $('#chat-area').scrollTop($('#chat-area')[0].scrollHeight);
    //
    //        var element = document.getElementById("chat-area");
    //
    //        element.scrollTop = element.scrollHeight; // doesn't work!
    //
    //
    //        // if ($('#chat-' + jid_id).length === 0) {
    //        //     $('#chat-area').tabs('add', '#chat-' + jid_id, jid);
    //        //     $('#chat-' + jid_id).append(
    //        //         "<div class='chat-messages'></div>" +
    //        //         "<input type='text' class='chat-input'>");
    //        // }
    //
    //        $("#" + jid_id).find('a').html(
    //            "<h3 class='ui-li-heading'>" + jid + "</h3>" + "<p class='ui-li-desc'>" + body.text() + "</p>");
    //        // $('#' + jid_id1).data('jid', jid_id);
    //
    //        // $('#chat-area').tabs('select', '#chat-' + jid_id);
    //        // $('#chat-' + jid_id + ' input').focus();
    //
    //        // var composing = $(message).find('composing');
    //        // if (composing.length > 0) {
    //        //     $('#chat-' + jid_id + ' .chat-messages').append(
    //        //         "<div class='chat-event'>" +
    //        //         Strophe.getNodeFromJid(jid) +
    //        //         " is typing...</div>");
    //
    //        //     XMPP.scroll_chat(jid_id);
    //        // }
    //
    //        // var body = $(message).find("html > body");
    //
    //        // if (body.length === 0) {
    //        //     body = $(message).find('body');
    //        //     if (body.length > 0) {
    //        //         body = body.text()
    //        //     } else {
    //        //         body = null;
    //        //     }
    //        // } else {
    //        //     body = body.contents();
    //
    //        //     var span = $("<span></span>");
    //        //     body.each(function () {
    //        //         if (document.importNode) {
    //        //             $(document.importNode(this, true)).appendTo(span);
    //        //         } else {
    //        //             // IE workaround
    //        //             span.append(this.xml);
    //        //         }
    //        //     });
    //
    //        //     body = span;
    //        // }
    //
    //        // if (body) {
    //        //     // remove notifications since user is now active
    //        //     $('#chat-' + jid_id + ' .chat-event').remove();
    //
    //        //     // add the new message
    //        //     $('#chat-' + jid_id + ' .chat-messages').append(
    //        //         "<div class='chat-message'>" +
    //        //         "&lt;<span class='chat-name'>" +
    //        //         Strophe.getNodeFromJid(jid) +
    //        //         "</span>&gt;<span class='chat-text'>" +
    //        //         "</span></div>");
    //
    //        //     $('#chat-' + jid_id + ' .chat-message:last .chat-text')
    //        //         .append(body);
    //
    //        //     XMPP.scroll_chat(jid_id);
    //        // }
    //
    //    });
    
    // console.log(name +" "+password);
    
    //    page = localStorage.getItem("page");
    ////    console.log(page);
    //    if(page){
    //                        hashid = "#"+localStorage.getItem("page");
    //                console.log(hashid);
    //                $.mobile.changePage(hashid, {
    //                    transition: "none"
    //                });
    //    }
    //    else{
    //        $.mobile.changePage("#zero", {
    //            transition: "none"
    //        });
    //    }
    
    //    login();
//    var name = localStorage.getItem("name");
//    var password = localStorage.getItem("password");
//    group = localStorage.getItem("group_name");
//    if (name !== null && password !== null) {
//        
//        if(group){
//            
//            xmpp.login(name, password, function (result_local) {
//                       
//                       
//                       group = localStorage.getItem("group_name");
//                       //                   console.log(group);
//                       $(".groupname").text(group);
//                       //                                      alert(group);
//                      
//                       
//                       code = localStorage.getItem("code");
//                       //                   alert(code);
//                       if (code == 1) {
//                       $.mobile.changePage("#sentmessages", {
//                                           transition: "none"
//                                           });
//                       
//                       
//                       } else if(code == 2){
//                       $.mobile.changePage("#group", {
//                                           transition: "none"
//                                           });
//                       
//                       
//                       }
//                    
//                       
//                       })
//
//            
//            
//        }
//        else{
//            $("#registername").val(localStorage.getItem("name"));
//           $("#registerpassword").val(localStorage.getItem("password"));
//            $.mobile.changePage("#joingroup", {
//                                transition: "none"
//                                });
//        }
    
        
    }
    
    
    else {
        
        $.mobile.changePage("#signin", {
                            transition: "none"
                            });
        
    }
    
    
};

function onbodyload() {
    
    document.addEventListener('deviceready', onDeviceReady, false);
};



$(document).delegate("#bregister1", "click", function (event, ui) {
                     setTimeout(function(){
                                window.scrollTo(0, 0);
                                }, 0);
                     var networkState = checkConnection();
                     if (networkState == Connection.NONE){
                     alert('no internet ');
                     }
                     else{
                     
                     

                     console.log("Initializing...");
                     var name = $("#registername").val().toLowerCase();
                     
                     
                     var password = $("#registerpassword").val();
                     var email = $("#registeremail").val().toLowerCase();
                     var atpos=email.indexOf("@");
                     var dotpos=email.lastIndexOf(".");
                     var cpassword = password;
                     if (password != cpassword) {
                     console.log("Password and Confirm Password do no match");
                     }
                     else if (!name) {
                     alert("Username cannot be empty");
                     }
                     else if (!password) {
                     alert("Password cannot be empty");
                     }
                     else if (!email) {
                     alert("Email cannot be empty");
                     }
                     else if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)
                     {
                     alert("Not a valid e-mail address");
                     
                     }
                     
                     else {
                     console.log("Name " + name);
                     console.log("Namelowercase " + name.toLowerCase())
                     setTimeout(function () {
                                $.mobile.loading('show', {
                                                 theme: "a",
                                                 text: "Checking Availability...",
                                                 textonly: true,
                                                 textVisible: true
                                                 });
                                }, 0);
                     localStorage.setItem("name", name);
                     localStorage.setItem("password", password);
                     $.get("http://www.idlecampus.com/users/checkName", {
                           name: name.toLowerCase()
                           
                           })
                     .done(function (data) {
                           console.log("Data Loaded: " + data);
                           
                           if (parseInt(data) == 0) {
                           
                           $.get("http://www.idlecampus.com/users/checkEmail", {
                                 email: email.toLowerCase()
                                 
                                 })
                           .done(function (data) {
                                 console.log("Data Loaded: " + data);
                                 
                                 if (parseInt(data) == 0) {
                                 
                                 $.mobile.changePage("#joingroup", {
                                                     transition: "none"
                                                     });
                                 
                                 
                                 }
                                 
                                 if (parseInt(data) !== 0) {
                                 setTimeout(function () {
                                            $.mobile.loading('hide');
                                            }, 300);
                                 
                                 
                                 alert("Email already taken");
                                 
                                 
                                 
                                 
                                 }
                                 });
                           
                           
                          
                           
                           
                           }
                           
                           if (parseInt(data) !== 0) {
                           setTimeout(function () {
                                      $.mobile.loading('hide');
                                      }, 300);
                           
                           
                           alert("Name already in use");
                           
                           
                           
                                                    
                           
                           }
                           });
                     
                     
                     }
                     }
                     });




$(document).delegate("#blogin", "click", function (event, ui) {
                     
                     setTimeout(function(){
                                window.scrollTo(0, 0);
                                }, 0);
                
                     var networkState = checkConnection();
                     if (networkState == Connection.NONE){
                     alert('no internet ');
                     }
                     else{
                     
                     
                     
                     console.log("Checking...");
                     var name = $("#loginname").val().toLowerCase();
                     
                     
                     var password = $("#loginpassword").val();
                     var cpassword = password;
                     if (password != cpassword) {
                     console.log("Password and Confirm Password do no match");
                     }
                     
                     else if (!password) {
                     alert("Password cannot be empty");
                     }
                     
                     else {
                     console.log("Name " + name);
                     console.log("Namelowercase " + name.toLowerCase())
                     
                     localStorage.setItem("name", name);
                     localStorage.setItem("password", password);
                     $.get("http://www.idlecampus.com/users/checkName", {
                           name: name.toLowerCase()
                           })
                     .done(function (data) {
                           console.log("Data Loaded: " + data);
                           
                           if (parseInt(data) == 0) {
                           localStorage.setItem("name", "");
                           localStorage.setItem("password", "");
                                                      setTimeout(function () {
                                                                 $.mobile.loading('hide');
                                                                 }, 300);
                           alert("User not found");
                           
                           
                           }
                           
                           if (parseInt(data) !== 0) {
                           setTimeout(function () {
                                      $.mobile.loading('show', {
                                                       theme: "a",
                                                       text: "Initializing...",
                                                       textonly: true,
                                                       textVisible: true
                                                       });
                                      }, 0);
                                                     
                           
                           var name = localStorage.getItem("name");
                           var password = localStorage.getItem("password");
                           
                           
                           if (name && password) {
                           
                           xmpp.login1(name, password, function (result_local) {
                                       setTimeout(function () {
                                                  $.mobile.loading('hide');
                                                  }, 300);
                                       if (parseInt(result_local) == 1) {
                                       localStorage.setItem("name", "");
                                       localStorage.setItem("password", "");
                                       alert("Invalid Credentials");
                                       
                                       
                                       }
                                       else if (parseInt(result_local) == 2) {
                                       
                                       
                                       xmpp.gotGroups(function (group) {
                                                      console.log(group);
                                                      if (group.code == 1) {
                                                      localStorage.setItem("code",group.code);
                                                      localStorage.setItem("group_code",group.group_code);
                                                      localStorage.setItem("group_name", group.name);
                                                      $.mobile.changePage("#sentmessages", {
                                                                          transition: "none"
                                                                          });
                                                      
                                                      
                                                      } else if(group.code == 2){
                                                      localStorage.setItem("code",group.code);
                                                      localStorage.setItem("group_code",group.group_code);
                                                      localStorage.setItem("group_name", group.name);
                                                      $(".groupname").text(group.name);
                                                      
                                                      var group = localStorage.getItem('group_code');
                                                      console.log(group);
                                                      timetable.notification("http://idlecampus.com/timetable/get_timetable_for_group?group=" + group);
                                                      $("#timetablestatus").text("New");
                                                      localStorage.setItem("status","New");
                                                      $.mobile.changePage("#group", {
                                                                          transition: "none"
                                                                          });
                                                      
                                                      
                                                      }
                                                      
                                                      })
                                       
                                       
                                       }
                                       
                                       })
                           
                           
                           }
                           
                           
                           }
                           });
                     
                     
                     }
                     }
                     });



$( document ).delegate("#sendmessage", "click",  function(event, ui) {
                       
//                       var msg = $('#newmessagefield').val();
//                       
//                       var chat_full = $("#chat-area").val();
//                       chat_full = chat_full + "\n"
//                       + localStorage.getItem("name") + ": "
//                       + $('#message').val();
//                       $("#chat-area").val(chat_full);
//                       $('#chat-area').scrollTop(
//                                                 $('#chat-area')[0].scrollHeight);
                       xmpp.send(msg,name,function(result_local) {
                                 
                                 });
                       ($('#message').val(""));
                       });



$(document).delegate("#bregister", "click", function (event, ui) {
                     setTimeout(function(){
                                window.scrollTo(0, 0);
                                }, 0);
                     var networkState = checkConnection();
                     if (networkState == Connection.NONE){
                     alert('no internet ');
                     }
                     else{
                     

                     console.log("Initializing...");
                     var name = $("#registername").val().toLowerCase();
                     
                     
                     var group = $("#joingroupinput").attr("value").toUpperCase();
                      var email = $("#registeremail").val().toLowerCase();
                     var password = $("#registerpassword").val();
                     var cpassword = password;
                     if (password != cpassword) {
                     console.log("Password and Confirm Password do no match");
                     } else {
                     console.log("Name " + name);
                     console.log("Namelowercase " + name.toLowerCase())
                     setTimeout(function () {
                                $.mobile.loading('show', {
                                                 theme: "a",
                                                 text: "Checking Availability...",
                                                 textonly: true,
                                                 textVisible: true
                                                 });
                                }, 0);
                     localStorage.setItem("name", name);
                     localStorage.setItem("password", password);
                     $.get("http://www.idlecampus.com/users/checkName", {
                           name: name.toLowerCase()
                           })
                     .done(function (data) {
                           console.log("Data Loaded: " + data);
                           
                           if (parseInt(data) == 0) {
//                           setTimeout(function () {
//                                      $.mobile.loading('hide');
//                                      }, 300);
                           
                           setTimeout(function () {
                                      $.mobile.loading('show', {
                                                       theme: "a",
                                                       text: "Initializing...",
                                                       textonly: true,
                                                       textVisible: true
                                                       });
                                      }, 0);


                           
                           
                           $.post("http://idlecampus.com/groups/get_group_name", {
                                  group_code: group
                                  }).done(function (data) {
                                          //                                          alert(data);
                                          if (data == "group not found") {
                                          alert(data);
                                          } else {
                                          localStorage.setItem("code",2);
                                          localStorage.setItem("group_code",data.group_code);
                                          localStorage.setItem("group_name",data.group_name);
//                                          alert(data.group_name);
                                          $(".groupname").text(data.group_name);
                                          var name = localStorage.getItem("name");
                                          var password = localStorage.getItem("password");
                                          xmpp.register(name, password, group,email, function (result_local) {
                                                                                                               
                                                        //                                                        alert(result_local);
                                                        if (parseInt(result_local) == 1) {
                                                                                                               
                                                                                                              
                                                        var group = localStorage.getItem('group_code');
                                                        console.log(group);
                                                        timetable.notification("http://idlecampus.com/timetable/get_timetable_for_group?group=" + group);
                                                        $("#timetablestatus").text("New");
                                                        localStorage.setItem("status","New");
                                                        $.mobile.changePage("#group", {
                                                                            transition: "none"
                                                                            });
                                                        
                                                        }
                                                        else {
                                                        alert("Invalid Credentials");
                                                        }
                                                        //                                                        xmpp.gotGroups(function(data){
                                                        //                                                                       alert(data);
                                                        //                                                                       });
                                                        
                                                        
                                                        });
                                          
                                          
                                          }
                                          ;
                                          });
                           }
                           
                           
                           });
                     
                     
                     }
                     }
                     });


//$(document).delegate("#sub-accept", "click", function (event, ui) {
//    approve_jid = $('#approve-jid').html();
//
//    xmpp.auth_user(approve_jid, function () {
//
//    });
//
//});
//$(document).delegate("#sub-reject", "click", function (event, ui) {
//
//});


//$(document).delegate("#creategroupbutton", "click", function (event, ui) {
//    //create a new node here.
//
//    var node = $("#creategroupinput").val();
//
//    xmpp.create_group(node, function () {
//
//    });
//
//
//});

$(document).delegate("#joingroupbutton", "click", function (event, ui) {
                     //delete a new node here.
                     var node = $("#joingroupinput").val();
                     xmpp.echo("hi", function (data) {
                               alert(data);
                               });
                     //                     $(".groupname").text(data.group_name);
                     //                     xmpp.join_group(node, function (data) {
                     //
                     //                                     $.mobile.changePage("#group", {transition: "none"});
                     //
                     //
                     //                                     });
                     
                     
                     $.post("http://idlecampus.com/groups/get_group_name", {
                            group_code: node
                            }).done(function (data) {
                                    console.log(data);
                                    if (data == "group not found") {
                                    alert(data);
                                    } else {
                                    localStorage.setItem("code",1);
                                    localStorage.setItem("group_code",data.group_code);
                                    localStorage.setItem("group_name",data.group_name);                                    $(".groupname").text(data.group_name);
                                    xmpp.join_group(data.group_code, function (data) {
                                                    
                                                    $.mobile.changePage("#group", {
                                                                        transition: "none"
                                                                        });
                                                    
                                                    
                                                    });
                                    
                                    
                                    }
                                    });
                     
                     
                     //                                     $.mobile.changePage("#group", {
                     //                                                         transition: "none"
                     //                                                         });
                     //                                     timetable.notification("http://idlecampus.com/timetable/get_timetable_for_group?group=" + node);
                     
                     });


$(document).delegate("#publishmessage", "click", function (event, ui) {
    //delete a new node here.

    var msg = $("#messagetext").val();
    var node = localStorage.getItem("group_code");
                         xmpp.publish_message(node, msg, function () {

    });
                     
                    $("#messagetext").val("");
                     
                     var group = "<li>" + msg + "</li>";
                     var tempmsgs = localStorage.getItem("sentmessages");
                     if (tempmsgs) {
                     console.log("TTTTTTT");
                     var sentmessages = JSON.parse(tempmsgs);
                     console.log(messages);
                     sentmessages.push(msg);
                     console.log(JSON.stringify(messages));
                     localStorage.setItem("sentmessages", JSON.stringify(messages));
                     } else {
                     var messages = []
                     messages.push(msg);
                     console.log(messages);
                     console.log(JSON.stringify(messages));
                     localStorage.setItem("messages", JSON.stringify(messages));
                     }
                     
                     var tempmsgs = localStorage.getItem("sentmessages");
                     if (tempmsgs) {
                     var messages = JSON.parse(tempmsgs);
                     for (var i = 0; i < sentmessages.length; i++) {
                     var group = "<li>" + sentmessages[i] + "</li>";
                     $('ul#sentmessageslist').append($(group));
                     
                     }
                     
                     }
                     
                     
                     
                     $('ul#sentmessageslist').listview("refresh");
                     




});

//$(document).delegate("#send", "click", function (event, ui) {
//
//    var msg = $('#message').val();
//
//    var chat_full = $("#chat-area").val();
//    chat_full = chat_full + "\n"
//        + localStorage.getItem("name") + ": "
//        + $('#message').val();
//    $("#chat-area").val(chat_full);
//    $('#chat-area').scrollTop(
//        $('#chat-area')[0].scrollHeight);
//    xmpp.send(msg, name, function (result_local) {
//
//    });
//    ($('#message').val(""));
//});
//
//$(document).delegate("#addf", "click", function (event, ui) {
//
//    xmpp.subscribeTo($("#addfriend").val(), function (result_local) {
//
//    });
//
//
//});
//
//$(document).delegate("#installapp", "click", function (event, ui) {
//    //do an ajax and add the application to the users list of installed app.
//});


$(document).delegate("#logout", "click", function (event, ui) {
                     
                     window.localStorage.setItem("name", "");
                     window.localStorage.setItem("password", "");
                     $("#loginname").val("");
                     $("#loginpassword").val("");
                     $.mobile.changePage("#zero", {
                                         transition: "none"
                                         });
                     XMPP.connection.disconnect("bored");
                     });

//$(document).delegate("#accept", "click", function (event, ui) {
//    XMPP.connection.send($pres({
//        to: XMPP.pending_subscriber,
//        "type": "subscribed"
//    }));
//
//    XMPP.connection.send($pres({
//        to: XMPP.pending_subscriber,
//        "type": "subscribe"
//    }));
//
//    XMPP.pending_subscriber = null;
//});
//
//$(document).delegate("#reject", "click", function (event, ui) {
//    XMPP.connection.send($pres({
//        to: XMPP.pending_subscriber,
//        "type": "unsubscribed"
//    }));
//    XMPP.pending_subscriber = null;
//});
//
//
//$(document).delegate("#myfriends li", "click", function (event, ui) {
//    name = $(this).find('a div.roster-jid').html();
//    $("#friendname").html(name);
//
//});


$(document).bind(
                 'connected',
                 
                 function () {
                 // on connection get all the apps and games that have been installed
                 // by me and display.
                 
                 // 	$.mobile.changePage("#mypage", {
                 // 	transition : "none"
                 // });
                 
                 
                 var iq = $iq({
                              type: 'get'
                              }).c('query', {
                                   xmlns: 'jabber:iq:roster'
                                   });
                 XMPP.connection.sendIQ(iq, XMPP.on_roster);
                 
                 XMPP.connection.addHandler(XMPP.on_roster_changed,
                                            "jabber:iq:roster", "iq", "set");
                 
                 XMPP.connection.addHandler(XMPP.on_message, null, "message", "chat");
                 });

$(document).bind('disconnected', function () {
                 XMPP.connection = null;
                 XMPP.pending_subscriber = null;
                 
                 $('#roster-area ul').empty();
                 $('#chat-area ul').empty();
                 $('#chat-area div').remove();
                 
                 $('#login_dialog').dialog('open');
                 
                 });

//$(document).bind('contact_added', function (ev, data) {
//    var iq = $iq({
//        type: "set"
//    }).c("query", {
//            xmlns: "jabber:iq:roster"
//        }).c("item", data);
//    XMPP.connection.sendIQ(iq);
//
//    var subscribe = $pres({
//        to: data.jid,
//        "type": "subscribe"
//    });
//    XMPP.connection.send(subscribe);
//});