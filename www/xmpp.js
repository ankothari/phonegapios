var xmpp = {
    
open_app:function(){
    $.mobile.changePage("#apps", { transition: "slideup"} );
},
    
    insert_feed : function(elem) {
        
        $('ul#feedul').append(elem);
        
    },
    notification : function(msg){
        
        // alert(msg);
        //  based on the value of the message do an ajax and get the message from the server.
        if (window.XMLHttpRequest)
        {// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp=new XMLHttpRequest();
        }
        else
        {// code for IE6, IE5
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        
        
        xmlhttp.open("GET",msg,true);
        xmlhttp.send();
        xmlhttp.onreadystatechange=function()
        {
            if (xmlhttp.readyState==4 && xmlhttp.status==200)
            {
                
                var response  = xmlhttp.responseText;
                var message_as_json = jQuery.parseJSON(response);
                //console.log( $('#emailsubject').html());
                
                
                var lfeed = "<li><a href=\"#per_email\"><h3 id = \"from\">"+message_as_json.from+"</h3> <p><strong id = \"subject\">"+message_as_json.subject+"</strong></p><p id = \"body\">"+message_as_json.body+"</p><p class=\"ui-li-aside\"><strong>6:24</strong>PM</p></a></li>"
                
                var feed = $(lfeed);
                xmpp.insert_feed(feed)
                $('#emailsubject').html(message_as_json.subject);
                //
                $('#emailbody').html(message_as_json.body);
                $.mobile.changePage("#per_email", { transition: "slideup"} );
            }
        }
        
    },
    
    //    get_messages = function(){
    //        if (window.XMLHttpRequest)
    //        {// code for IE7+, Firefox, Chrome, Opera, Safari
    //            xmlhttp=new XMLHttpRequest();
    //        }
    //        else
    //        {// code for IE6, IE5
    //            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    //        }
    //
    //
    //        xmlhttp.open("GET","http://192.168.0.22:3000/messages.json",true);
    //        xmlhttp.send();
    //        xmlhttp.onreadystatechange=function()
    //        {
    //            if (xmlhttp.readyState==4 && xmlhttp.status==200)
    //            {
    //
    //                var message_as_json_in_an_array = xmlhttp.responseText;
    //                //loop through the json for all the messages.
    //
    //                for (var i = 0; i < message_as_json_in_an_array.length; i++) {
    //                    var message = message_as_json_in_an_array[i];
    //                    // get the subject, from, body from the message.
    //                    var subject = message.subject;
    //                    var from = message.from
    //                    var body = message.body;
    //                    var url = "http://192.168.0.22:3000/message/"+message.id+".json";
    //                    console.log(url);
    //                }
    //
    //
    //            }
    //        }
    //
    //    },
    jid_to_id : function(jid) {
        return xmpp.getBareJidFromJid(jid).replace(/@/g, "-").replace(/\./g,"-");
    },
getBareJidFromJid: function (jid)
    {
        return jid.split("/")[0];
    },
    
getDomainFromJid: function (jid)
    {
        var bare = xmpp.getBareJidFromJid(jid);
        if (bare.indexOf("@") < 0) {
            return bare;
        } else {
            var parts = bare.split("@");
            parts.splice(0, 1);
            return parts.join('@');
        }
    },
getNodeFromJid: function (jid)
    {
        if (jid.indexOf("@") < 0) { return null; }
        return jid.split("@")[0];
    },
    insert_contact : function(elem) {
        
        var jid = elem.find('.roster-jid').text();
        var pres = xmpp.presence_value(elem.find('.roster-contact'));
        
        var contacts = $('ul#myfriends li');
        
        if (contacts.length > 0) {
            var inserted = false;
            contacts.each(function() {
                          var cmp_pres = xmpp.presence_value($(this).find(
                                                                          '.roster-contact'));
                          var cmp_jid = $(this).find('.roster-jid').text();
                          
                          if (pres > cmp_pres) {
                          $(this).before(elem);
                          inserted = true;
                          return false;
                          } else if (pres === cmp_pres) {
                          if (jid < cmp_jid) {
                          $(this).before(elem);
                          inserted = true;
                          return false;
                          }
                          }
                          });
            
            if (!inserted) {
                
                $('ul#myfriends').append(elem);
            }
        } else {
            
            $('ul#myfriends').append(elem);
        }
    },
    
    presence_value : function(elem) {
        if (elem.hasClass('online')) {
            return 2;
        } else if (elem.hasClass('away')) {
            return 1;
        }
        
        return 0;
    }
};
xmpp.echo = function(str, callback) {
    
    cordova.exec(callback, function(err) {
                 
                 callback('Nothing to echo.');
                 }, "XMPP", "echo", [str]);
};

xmpp.login = function(username,password, callback) {
    
    cordova.exec(callback, function(err) {
                 
                 callback(false);
                 }, "XMPP", "login", [username,password]);
};
xmpp.register = function(username,password,group,email, callback) {
    
    cordova.exec(callback, function(err) {
                 
                 callback(err);
                 }, "XMPP", "registerUser", [username,password,group,email]);
};

xmpp.login1 = function(username,password, callback) {
    
    cordova.exec(callback, function(err) {
                 
                 callback(err);
                 }, "XMPP", "login1", [username,password]);
};

xmpp.send = function(message,jid, callback) {
    
    cordova.exec(callback, function(err) {
                 
                 callback(false);
                 }, "XMPP", "send", [message,jid]);
};
xmpp.receiveMessage = function(callback) {
    
    cordova.exec(callback, function(err) {
                 
                 callback(false);
                 }, "XMPP", "receiveMessage", []);
};

xmpp.gotRoster = function(callback){
    cordova.exec(callback, function(err) {
                 
                 callback(false);
                 }, "XMPP", "gotRoster", []);
}

xmpp.disconnect = function(callback){
    cordova.exec(callback, function(err) {
                 
                 callback(false);
                 }, "XMPP", "disconnect", []);
}

xmpp.get_group_name = function(group, callback) {
    alert("hi");
    cordova.exec(callback, function(err) {
                 
                 callback(false);
                 }, "XMPP", "get_group_name", [group]);
};

xmpp.removeAllObjects = function(callback){
    cordova.exec(callback, function(err) {
                 
                 callback(false);
                 }, "XMPP", "removeAllObjects", []);
}

xmpp.on_presence = function(callback) {
    cordova.exec(callback, function(err) {
                 
                 callback('Nothing to echo.');
                 }, "XMPP", "on_presence",[]);
},

xmpp.subscribeTo = function(user, callback) {
    
    cordova.exec(callback, function(err) {
                 
                 callback(false);
                 }, "XMPP", "subscribeTo", [user]);
};
xmpp.auth_user = function(user,callback) {
    
    cordova.exec(callback, function(err) {
                 
                 callback(false);
                 }, "XMPP", "auth_user", ["true",user]);
};
xmpp.create_group = function(group,callback) {
    
    cordova.exec(callback, function(err) {
                 
                 callback(false);
                 }, "XMPP", "create_group", [group]);
};
xmpp.join_group = function(group,callback) {
    
    cordova.exec(callback, function(err) {
                 
                 callback(err);
                 }, "XMPP", "join_group", [group]);
};
xmpp.publish_message = function(node,message,callback) {
    
    cordova.exec(callback, function(err) {
                 
                 callback(false);
                 }, "XMPP", "publish_message", [node,message]);
};

xmpp.gotGroups = function(callback) {
    
    cordova.exec(callback, function(err) {
                 
                 callback(false);
                 }, "XMPP", "gotGroups", []);
};
xmpp.getGroupsJoined = function(callback) {
    
    cordova.exec(callback, function(err) {
                 
                 callback(false);
                 }, "XMPP", "getGroupsJoined", []);
};

